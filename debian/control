Source: gnumed-client
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Andreas Tille <tille@debian.org>,
           Sebastian Hilbert <sebastian.hilbert@gmx.net>
Section: misc
Priority: optional
Build-Depends: debhelper-compat (= 13),
               po-debconf,
               python3,
               dh-sequence-python3,
               dh-linktree,
               libjs-jquery,
               libjs-jquery-livequery,
               yui-compressor,
               bash-completion
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/med-team/gnumed-client
Vcs-Git: https://salsa.debian.org/med-team/gnumed-client.git
Homepage: https://www.gnumed.de
Rules-Requires-Root: no

Package: gnumed-client
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends},
         python3-wxgtk4.0,
         gnumed-common (= ${source:Version}),
         file,
         python3-enchant,
         hunspell | aspell | ispell | myspell-dictionary | hspell,
         python3-gnuplot,
         texlive-latex-base,
         imagemagick,
         python3-httplib2,
         python3-hl7,
         python3-pyudev,
         python3-psutil,
         python3-lxml
Recommends: gnumed-doc (= ${source:Version}),
            audiofile-tools,
            dcmtk,
            ginkgocadx | aeskulap | amide | dicomscope | imagej | xmedcon,
            libimage-exiftool-perl,
            xsane,
            extract,
            ntp | ntpdate | systemd-timesyncd,
            www-browser,
            libreoffice-writer,
            wgerman-medical,
            xdg-utils,
            gtklp,
            texlive-latex-recommended,
            texlive-latex-extra,
            pdftk-java,
            python3-vobject,
            python3-pyqrcode,
            python3-unidecode,
            python3-docutils,
            7zip,
            gpg,
            poppler-utils,
            qpdf,
            lacheck,
            chktex
Suggests: korganizer,
          libchipcard-tools,
          incron,
          gnumed-server,
          konsolekalendar,
          gimp | kolourpaint,
          shutdown-at-night,
          edfbrowser,
          autokey-qt | autokey-gtk,
          wakeonlan | etherwake | gwakeonlan,
          nvram-wakeup,
          entangle,
          python3-uno,
          qrisk2
Provides: ${python3:Provides}
Description: medical practice management - Client
 This is the GNUmed Electronic Medical Record. Its purpose is
 to enable doctors to keep a medically sound record on their
 patients' health. It does not currently provide functionality
 for stock keeping. Clinical features are well-tested by real
 doctors in the field.
 .
 While the GNUmed team has taken the utmost care to make sure
 the medical records are safe at all times you still need to
 make sure you are taking appropriate steps to backup the
 medical data to a safe place at appropriate intervals. Do
 not forget to test your recovery procedures, too !
 .
 Protect your data! GNUmed itself comes without
 any warranty whatsoever. You have been warned.
 .
 This package contains the wxpython client.

Package: gnumed-client-de
Architecture: all
Depends: gnumed-client (= ${source:Version}),
         ${misc:Depends},
         adduser
Recommends: wgerman-medical,
            hunspell-de-med,
            dmtx-utils | iec16022,
            libchipcard-tools,
Suggests: libctapimkt1
Description: medical practice management - Client for German users
 Just install this package if you want to use the GNUmed
 client in Germany and you have to use German chipcard
 systems.  The package installs the GNUmed client and
 cares for proper libchipcard support.

Package: gnumed-common
Architecture: all
Depends: ${python3:Depends},
         ${misc:Depends},
         python3-psycopg2
Suggests: dmtx-utils | iec16022
Provides: ${python3:Provides}
Description: medical practice management - common files
 This is the GNUmed Electronic Medical Record. Its purpose is
 to enable doctors to keep a medically sound record on their
 patients' health. Currently it is not fully featured. The
 features provided are, however, tested, in use, and
 considered stable. This package does NOT yet provide
 functionality for billing and stock keeping.
 .
 While the GNUmed team has taken the utmost care to make sure
 the medical records are safe at all times you still need to
 make sure you are taking appropriate steps to backup the
 medical data to a safe place at appropriate intervals. Do
 test your backup and disaster recovery procedures, too !
 .
 Protect your data! GNUmed itself comes without
 any warranty whatsoever. You have been warned.
 .
 This package contains the files which are common to client
 and server.

Package: gnumed-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
         libjs-jquery-livequery
Suggests: gnumed-client,
          dwww
Enhances: gnumed-client
Description: medical practice management - Documentation
 This is the GNUmed Electronic Medical Record. Its purpose is
 to enable doctors to keep a medically sound record on their
 patients' health. Currently it is not fully featured. The
 features provided are, however, tested, in use, and
 considered stable. This package does NOT yet provide
 functionality for billing and stock keeping.
 .
 While the GNUmed team has taken the utmost care to make sure
 the medical records are safe at all times you still need to
 make sure you are taking appropriate steps to backup the
 medical data to a safe place at appropriate intervals. Do
 test your backup and disaster recovery procedures, too !
 .
 Protect your data! GNUmed itself comes without
 any warranty whatsoever. You have been warned.
 .
 This package contains the documentation for users.
