
import sys
import datetime

sys.path.insert(0, '../../')

from Gnumed.business import gmBilling

scan2pay = gmBilling.generate_scan2pay_string (
	IBAN = 'DE70500105170634401730',
	beneficiary = 'Karsten Hilbert',
	BIC = 'INGDDEFFXXX',
	amount = sys.argv[1],
	invoice_id = 'KH-%s' % datetime.datetime.now().strftime('%Y%m%d-%H%M%S'),
	comment = sys.argv[2]
)
print(scan2pay)
print(gmBilling.generate_scan2pay_qrcode(data = scan2pay, create_svg = True))
