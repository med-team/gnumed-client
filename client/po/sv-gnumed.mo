��    3     �  E  L3      `D  9   aD  5   �D  �   �D  �   |E  �   bF  L   CG  .   �G  #  �G  �   �H  V   �I  �   �I  i   �J  m   �J  
   XK  #   cK     �K     �K     �K     �K     �K     �K  
   L     L     ,L     KL  !   ^L     �L  /   �L     �L     �L     �L     	M      &M     GM  .   XM     �M     �M     �M     �M     �M     �M     N      N     =N     [N     xN     �N     �N     �N     �N     �N     �N     O  5   O     TO     nO     �O     �O     �O     �O  D   �O     0P     9P     AP     TP  	   nP     xP     �P     �P  
   �P     �P     �P     �P     �P     �P     �P  '   �P  	   !Q  
   +Q     6Q     <Q     DQ     LQ  	   YQ     cQ     ~Q     �Q     �Q     �Q     �Q     �Q     �Q  	   �Q  
   �Q     �Q  ;   �Q  1   3R     eR     R     �R     �R     �R     �R     �R     
S      S     6S     8S     AS  9  MS  '   �T  `   �T     U     U  #   U     BU     HU     dU     lU     yU  
   �U     �U     �U     �U     �U     �U     �U     �U     �U     �U     V      V     0V     @V  X   NV  y   �V     !W  .   %W  +   TW     �W  ,   �W  )   �W  )   �W     X     X     1X     AX     PX  
   _X     jX     sX     |X     �X     �X  -   �X     �X     �X     �X     �X  /   Y  4   5Y     jY     �Y     �Y     �Y     �Y     �Y  '   �Y  2   'Z  5   ZZ  0   �Z  *   �Z  1   �Z  )   [  -   H[     v[  
   �[     �[  "   �[  0   �[  	   �[     �[     \     \  	   \     (\     ;\     M\  T   \\  Z   �\  N   ]     []  0   g]     �]  -   �]  F   �]  B   (^  >   k^     �^  ?   �^  <   �^     -_  =   3_     q_     x_     �_     �_     �_     �_     �_     �_     �_  A   �_  
   `  '   (`  Q   P`  S   �`  H   �`     ?a  C   Ya     �a     �a     �a     �a     �a     �a     b     b  .   (b     Wb     _b  -   wb     �b  2   �b  5   �b     c     c     #c     <c     Kc      Yc     zc     �c     �c  .   �c     �c     �c  	   �c  &   �c     d  2   1d      dd  Z   �d  �   �d  
   be     me  
   {e     �e     �e  d   �e     f     f     f     +f     ;f     Hf  !   Qf     sf     xf     �f     �f     �f     �f  
   �f     �f  
   �f     �f     g     g     g     -g  0   Jg  
   {g  )   �g     �g  !   �g     �g  	   �g     �g     h     h  
   #h     .h     =h     Wh     [h  
   oh  
   zh     �h     �h  U   �h  |   �h  t   |i  z   �i     lj     tj  "   �j     �j     �j     �j  *   �j  -   �j  0    k  (   Qk     zk  <   �k  B   �k  6   l  7   Ql     �l     �l     �l     �l  '   �l  =   �l  1   /m     am     fm     wm     �m     �m     �m     �m     �m     �m     �m      n     n     'n     0n     4n     =n     Qn     Vn     pn     �n     �n  *   �n     �n     �n     �n  
   o     o  	   o     %o     .o     7o     Co     Io     Qo     do     vo  
   �o     �o     �o  
   �o     �o     �o     �o     �o     �o     �o     �o     p     #p     2p     >p     Kp  
   Wp  
   bp     mp     }p     �p     �p  	   �p     �p     �p     �p     �p     q     q     q  _   8q  	   �q  <   �q  	   �q     �q     �q     �q     r  0   r  
   @r  
   Kr     Vr     dr  	   ur  �   r     [s     gs     vs     |s     �s     �s     �s     �s     �s     �s     �s     �s     �s  
   �s  	   �s  
   �s     t     t     +t  �   0t     �t     �t  )   �t     u     u     &u     2u     9u     Gu     au     ~u     �u  !   �u     �u     �u     �u     �u     v     v  
   *v  B   5v  !   xv     �v     �v     �v     �v     �v     �v     �v     �v     w     w     w  	   "w  (   ,w  #   Uw     yw  !   }w     �w     �w     �w  !   �w  *   �w     x     (x     Cx     Wx  A   gx  Y   �x  	   y     y     #y     +y     7y     ;y     Hy     Vy  $   py     �y  )   �y  i  �y     A{     W{     ]{     }{  !   �{     �{     �{     �{     �{     �{     �{     |  �  
|     �}     �}     �}     �}     �}     �}     �}  >   �}     =~     F~     R~     a~  (   q~     �~     �~     �~     �~     �~     �~     �~  	             $     )     7  @   U     �  B   �  	   �  
   �     �  .   �  -   B�  W   p�  G   Ȁ     �     ,�  L   3�  2   ��      ��     ԁ     ہ     �     �     �     /�     B�     `�     t�     ��     ��     ��     ��     ؂     �     ��     �     1�  2   >�     q�     ��  -   ��  *   ؃     �     #�  
   4�     ?�     H�  "   L�  +   o�  '   ��     Ä     �     �  	   �     �  	   �  b   $�     ��  �   ��     6�  7   <�  p   t�  7   �     �     1�     H�  /   Y�     ��     ��     ��  	   ��     ��     ��     ��     Ї     �     �     ��      �     �     �     �  "   �  /  A�  �   q�  ]   >�  B   ��  �   ߊ  C   ��  0   ��  2   -�  &   `�  7   ��  )   ��  ,   �  3   �  H   J�  �   ��  4    �  `   U�  E   ��  A   ��  K  >�     ��  *   ��  �   ϐ  !   y�     ��  �   ��  	   d�     n�     r�     {�  >   ��  >   ɒ     �     �     �  	   �  M   '�  <   u�     ��     ��     ��     Ó  $   ��  {   �     ��     ��     ��  "   ٔ  '   ��  3   $�  ;   X�     ��  s   ��  #   '�     K�  )   j�     ��     ��     ��     ��     Ӗ     �  $   ��     �  	   "�     ,�     4�     T�     k�     |�     ��     ��     ��  8   ��  %   �     
�     �     /�     @�  "   R�     u�     ��     ��     ��     ��     ��  	   ̘     ֘     ޘ  @   �  �   3�  �   3�    +�  �   :�  *   ߜ     
�  ;   �     L�  -   f�  ;   ��     Н     ՝  E   ٝ  7   �  �   W�  .   �     �     0�     5�  	   >�  9   H�     ��     ��     ��     ��  	   ��     ǟ     ˟     џ     �     �     ��     �     �     $�     (�     H�     M�     d�  '   u�     ��     ��     Ơ  5   ڠ     �     �     �  
   &�  	   1�     ;�     Q�     f�     ~�  +   ��     ��     ��     š     ˡ     ݡ     �      �     �     &�     8�     J�     S�     g�     ��  \   ��      �     �  	   �     �      �     '�     /�     G�     V�     [�     o�     ��     ��     ��     ��      ѣ     �      �     &�  *   =�  $   h�     ��     ��     ��     ��     Ѥ     �     �     ��  *   	�  )   4�  $   ^�  	   ��     ��     ��     ��     ��  
   ��     ��  
   å     Υ     ܥ     �     ��     �  '   �     @�  '   Y�     ��     ��     ��     ��     ��     ��  l  ��  :   %�  ;   `�  �   ��    l�  �   t�  f   h�  /   ϫ    ��  �   �  U   ��  �   �  j   ��  �   	�  
   ��      į     �     ��     �     +�     @�     S�     k�     y�  $   ��     ��  G   װ  %   �  /   E�     u�     ��     ��     ��  '   ױ     ��  2   �     I�  #   ^�     ��     ��     ��     ϲ  $   �  "   
�  (   -�      V�  &   w�  %   ��     ĳ     ڳ     �     �     �     3�  5   H�      ~�     ��  "   ��     �     ��  "   �  J   2�     }�     ��     ��     ��     ǵ      е  	   �     ��     �     '�  
   8�     C�     J�     R�     p�  *   ��  	   ��     ��     ƶ  	   ϶     ٶ     �  
   ��     ��     �     %�     +�     D�     T�     l�     s�  	   |�  
   ��     ��  O   ��  4   ��  #   "�  "   F�  (   i�     ��  #   ��  #   ʸ  "   �  %   �  %   7�     ]�     _�  
   k�  h  v�  0   ߺ  _   �     p�     x�  !   �     ��  .   ��     ӻ     ٻ     ��     �  &   �     ;�     I�  !   W�     y�     ��     ��     ��     ��     ̼     Լ      �     �  {   �  �   ��     0�  4   4�  :   i�     ��  '   ¾  (   �  &   �     :�     >�     M�     _�     p�     ��     ��     ��  
   ��     ��  	   ��  0   ƿ     ��     �     !�     :�  6   B�  J   y�  *   ��  *   ��  (   �  +   C�  $   o�     ��  (   ��  :   ��  <   ��  5   <�  3   r�  ;   ��  0   ��  3   �     G�  
   d�     o�  #   x�  2   ��  	   ��     ��     ��     ��     �     �  "   +�     N�  h   c�  S   ��  n    �  	   ��  +   ��     ��  @   ��  Y   �  T   y�  6   ��     �  B   �  A   O�     ��  P   ��     ��     ��     �     �  	   �     �     '�  ;   :�     v�  U   �     ��  <   ��  \   �  ]   {�  b   ��     <�  ]   Y�     ��     ��     ��     ��     ��     �     "�     2�  .   G�     v�  ,   �  ;   ��     ��  2   ��  4   (�     ]�     a�  )   e�     ��     ��  3   ��     ��     ��     ��  I   �  
   R�     ]�     r�  -   z�  #   ��  8   ��  $   �  V   *�  �   ��  	   "�     ,�     9�     L�     `�  y   �     ��     �     	�     �     /�     <�  &   H�     o�     x�     ��     ��     ��     ��     ��     ��     ��     �     #�     ;�     K�  "   _�  6   ��     ��     ��     ��  4   ��     -�     A�     O�     a�     z�     ��     ��     ��     ��     ��     ��     ��  
   �  '   �  f   :�  �   ��  �   '�  �   ��     <�     C�  '   Y�     ��     ��     ��  ;   ��  9   ��  6   �  ;   J�  6   ��  a   ��  S   �  E   s�  X   ��  !   �     4�     N�  
   f�  F   q�  U   ��  @   �     O�     W�     j�  	   ��  	   ��     ��     ��     ��     ��     ��     �     �  
   /�     :�     >�     L�     f�     l�  1   ��     ��     ��  2   ��  &   ��     &�     9�     E�     W�     i�     x�     ��  	   ��     ��     ��     ��     ��     ��  	   ��     ��     ��     ��     �     �     2�     B�     [�     l�     r�     ��     ��     ��     ��     ��     ��     ��     ��      �     )�  )   6�     `�  
   l�     w�     ��     ��     ��     ��  ,   ��  n   ��     h�  T   ~�  
   ��     ��     ��     ��  	   ��  ?   	�  	   I�     S�     `�     u�     ��  �   ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �      �     $�     (�     ,�     4�     B�     Q�     g�     }�  �   ��     ,�     K�  -   _�     ��     ��     ��     ��     ��     ��      ��     ��     �  +   �     E�     I�     e�     ��     ��     ��  	   ��  b   ��  *   .�     Y�     f�     y�     ~�     ��     ��     ��     ��     ��     ��     ��     ��  0   �  (   F�     o�  /   r�  (   ��     ��     ��  $   ��  :   ��     5�  /   K�     {�     ��  J   ��  e   ��  	   Z�     d�  	   x�     ��     ��     ��     ��     ��  "   ��  #   ��  .   �  �  K�     ��     �  /   
�     :�  !   L�     n�  
   ��  
   ��  	   ��     ��     ��     ��  �  ��     ��     ��     ��     ��     ��     ��     �  L    �     m�     v�     ��     ��  6   ��     ��     ��     �     �     %�     6�     ?�     L�     Y�     r�     z�     ��  A   ��     ��  @   �  	   M�     W�     d�  B   w�  /   ��  j   ��  Y   U�  !   ��     ��  V   ��  D   /�  J   t�     ��     ��     ��     ��     
�     �  +   .�     Z�     o�     ��     ��  
   ��     ��     ��     ��     �     �     ;�  :   K�     ��     ��  3   ��  *   ��  !   �     =�     P�     \�     j�  &   n�  :   ��  +   ��  (   ��     %�     3�     F�     U�     b�  f   k�     ��  ,   ��     �  9   �  �   Y�  B   ��     ,�     F�     c�  8   s�     ��     ��     ��     ��     ��     ��     ��     ��     �     �     �     )�     /�     E�     J�  )   P�  �  z�  �   2�  �   ��  <   ��    ��  9   ��  (   �  9   7�  #   q�  V   ��  1   ��  .   �  >   M�  V   ��  �   ��  E   {�  �   ��  C   O  K   �  R  �  %   2 3   X �   �     7 "   X �   {    +    : 	   >    H O   W L   �    �    �         Q   2 Z   �    �    �    � -   � .   ( �   W    �    �      "   ? *   b J   � G   � )     �   J 2   	 %   5	 /   [	    �	 	   �	    �	    �	 $   �	    �	 4   
    G
    N
    ]
 6   d
 $   �
    �
    �
    �
    �
    �
 H    '   J    r    �    �    � %   �     �        "    *    .    4 	   G 
   Q    \ >   u w  � q  , >  � �   � 5   �    � Q   �     ,   & E   S    �    � ^   � :   � �   : 8   �    &    D 	   J 	   T F   ^    �    �    �    � 	   �    �    �    �            $    )    5    J $   N    s $   y 
   � 3   �    �    �    	 4       Q 
   X    c    l    u    �    �    �    � 0   � *       ?    E    L    ]    o    �    �    �    �    �    � :   � %    q   E    �    � 
   �    �    �    �    �                 2    H    \    q    �     �    �     �     � B    O   R    �    � 
   �     �    �            $ #   0 +   T :   �    �    �    �    �    � 	   �    � 	               "    ?    K ,   [ $   � 7   �    �    �    �         
   
        A               �  R   �  $  �      ]    r                �   �      :  �              >      �       5    �   �   �  *  �       �       Y       �  �  '  y  �          ;  �  M      t             �          l          J   �           �  .  �          �      w      �                �  f  �   t  y  �              s      {   �      �  &    L        �   Y  P       �   Q  P  �       �  �              n          E   �        !  �  �  2  �  =  �        �            �    �  �           P  �   �  �   �   �   %      �          �  �      �         �      �   7     S      k      b     �  �  m      �  D   j    4  �    \  #    �      �       �  �  $      	      W  1      �     �  �        �  T  �    �  �   k     6              �      �   �      �      u  �  �  �  |           �                      K                        �  �      d  �  �  �   �       r   �  :          �     e   �       
  �  �  �   e  �  .   �  �      �   �  +   S  �       i  �  9  �  "        d  h  .      /  �  �  -    �  `          "  �          �   �  J        ^  &  �  �  �  |  1   #       �  )   =   �       �      �   �               `      �  Z      �  c        �   m            �     �   �       �  N      �      ~  �   H  8   �   �  �   �   �   z    �       W     �       l  �  o         �  �  }   O                 �  �   �                  �            �   �  B   �  �   x   �   �         �   �      .  {  O        �  /      Z            (  _  N    1  �  -  �   �  �   �           #    B  n   q  M       �      q     2      +    f   [        G       _   �  �  �   �  ;  V  �  �  �              8  �  �             O       �  �  (  #      �  v      �  -  �   �       �   }             �  )  u   &  �   �  �   �  �    I   �   3   =  �           �    �  	  i   �  ]     p   �   �      �   �  �  �          �      �  �      �       �  �  �  a  �       �   k  �  �  N   @        Z  v  �  ~   �   �      �     3      s        �         �    }  ,       4  *  �  H   L       �  �  �  �    l  �  
  G      �   *   -   W  �       T  �  
           �    I  q  9  �  ^   �  "     +  �  �   �         �   %  �  �  L      �  �   4          �      �                 h  0  �          �   b  �  
  �  X        3  `   D    �  %             �       o  �  �   �  S   �  �   m           �   �   ,   \   t      \  c  J  7   �         e  Q          K     �  �  1  �      �  <   �  �  �  �   �  �   �  �      _  )  z      �   H    �            Y  �        g   :   �  �     �      �  �  w  h           '           x  �                 T   �      %   B         2       ~  5   �          �  r     9   �  �   �      �  K  �  <  )      z           ,      �  �   �           i  $      �    g  b       �          V       	      M  !   /           ;   �           �  2      �  [   �  �  C  �      �     �          U        �   �   �   �  �  "  *  �        �  6   �  [          �       �            �   �         $       F  �      E  j  �  g  0  �   �  �   �      �  �  �      �        A  �        �   �    y   �       7      �      �             >   �  �  �                �      p  �  �   �   x  �  �              >      �  ^          �  �                   C     o   �   f      �  �    �  6  �   �   �   F   �   R  �  G  �  (  �  �  �   �   �  !  �  D  �   �   �  �       �  ]  �  <                 d   R  �  /  E  �    |  �         n      '  �  @       F  (   �  �          �       �   a    �  '  �         X  �  �  �   V  �   5  �     ?         �  !  �      I  �   �   0  �      �  X   p      ,    �  v       �  �          8    �  �   0           A   �  �  3  C  �  �  �  a       @  �       U  j   �  �   �  s   &   {  �     u      �  w       �  �       U     	      �  c   Q   �   ?  �      �   +      ?  �         �  �   �       

Please consult the error log for all the gory details ! 
A new encounter was started for the active patient.
 
Categories are used to group events. An event can only belong to one category. All events that belong to the same category are displayed with the same background color.
 
Categories can be managed in the *Edit Categories* dialog (*Timeline* > *Edit Categories*). To edit an existing category, double click on it.

The visibility of categories can also be edited in the sidebar (*View* > *Sidebar*).
 
If you have more questions about Timeline, or if you want to get in contact with users and developers of Timeline, send an email to the user mailing list: <thetimelineproj-user@lists.sourceforge.net>. (Please use English.)
 
No. The events will still be there but they will not belong to a category.
 
Select the encounter type you want to edit !
 
The *Create Event* dialog can be opened in the following ways:

- Select *Timeline* - *Create Event* from the menu.
- Double click with the *left* mouse button on the timeline.
- Press the *Ctrl* key, thereafter hold *left* mouse button down on the timeline, drag the mouse and release it.
 
The date data object used does not support week numbers for weeks that start on Sunday at present.  We plan on using a different date object that will support this in future versions.
 
There is no save button. Timeline will automatically save your data whenever needed.
 
Timeline is developed and translated by volunteers. If you would like to contribute translations you are very much welcome to contact us.
 
To delete an event, select it and press the *Del* key. Multiple events can be deleted at the same time.
 
To select an event, click on it. To select multiple events, hold down the *Ctrl* key while clicking events.
   risk: %s   your time: %s - %s  (@%s = %s%s)
  %s active medications
  %s documents
  %s encounters from %s to %s
  %s known problems
  %s test results
  (last confirmed %s)  (ongoing)  ***** CONFIDENTIAL *****  - bug fixes and new features
  - bug fixes only
  - database fixups may be needed
  - database upgrade required
  Created during encounter: %s (%s - %s)   [#%s]  Most recent: %s - %s  New version: "%s"  Noted at age: %s  Your current version: "%s"
  contributed to death of patient  option [%s]: %s %d Events not duplicated due to missing dates. %d day(s) ago: %s %d events will be imported. %d hour(s) ago: %s %d month(s) ago: %s %d week(s) ago: %s %d year(s) ago: %s %d. of %s (last month) - a %s %d. of %s (last month): a %s %d. of %s (next month) - a %s %d. of %s (next month): a %s %d. of %s (this month) - a %s %d. of %s (this month): a %s %s (%s last year) %s (%s next year) %s (%s this year) %s Bytes %s encounter(s) (%s - %s): %s events hidden %s other episodes touched upon during this encounter: %s-%s-%s: a %s last month %s-%s-%s: a %s next month %s-%s-%s: a %s this month %s: %s by %.8s (v%s)
%s %s: encounter (%s) %sAllergy: %s, %s (noted %s)
 %sEpisode "%s" [%s]
%sEncounters: %s (%s - %s)
%sLast worked on: %s
 &Allergy &Append &Balloons on hover &Compress timeline Events &Definite &Duplicate Selected Event... &Edit &Edit Selected Event... &Encounter &Encounters &Enlarge &File &Help &Hide Events done &Legend &Measure Distance between two Events... &Navigate &Read Only &Redo &Reduce &Remove &Sensitivity &Timeline &Tolerates others in class &Undo &View ** DOB unknown ** *does* have allergies ... encounter level 1 Byte 1-period 10-period 100-period 1000-period 1st and (up to 3) most recent (of %s) encounters (%s - %s): <%s>: unknown diagnostic certainty classification <channel> cannot be empty <current allergy state> <issuer> cannot be empty <last confirmed> <name> cannot be empty <number> cannot be empty <street> cannot be empty <urb> cannot be empty <zip> cannot be empty ? ?ongoing ?short-term A GNUmed slave client has been started because
no running client could be found. You will now
have to enter your user name and password into
the login dialog as usual.

Switch to the GNUmed login dialog now and proceed
with the login procedure. Once GNUmed has started
up successfully switch back to this window.
 A new version of GNUmed is available.

 A short alias identifying the GNUmed user. It is used in the clinical record among other places. A: Sign Abort Abort and do NOT connect to GNUmed. About Accept negative Julian days Account Add Category Add Container Add an Era Add more events after this one Add new Add... Adding new encounter type Admin password Advice Alert Alias Allergies and Intolerances
 Allergy Allergy Manager Allergy details Allergy state An error occurred while Demographic record export
Please check the log file for details. An error occurred while retrieving a text
dump of the EMR for the active patient.

Please check the log file for details. Apr Are you sure you want to delete category '%s'? Are you sure you want to delete this event? Assessment of Encounter At least one Category Field must be selected At least one Event Field must be selected At least one Export Item must be selected Aug B: Cluster of signs Back One &Month Back One &Week Back One &Year Background Backward Battery: Body: Born   : %s, age: %s

 Both Both Modifier and Shortcut key must be given! Bottom-Left Bottom-Right C: Syndromic diagnosis CSV File Can multiple users work with the same timeline? Can't connect to database without login information! Can't move locked event Can't scroll more to the left Can't scroll more to the right Can't zoom deeper than 1 minute Can't zoom deeper than 5 Cancel Cancel editing the allergy/intolerance. Cancel this dialog, do not enlist new GNUmed user. Cannot connect to database without login information! Cannot delete encounter type [%s]. It is in use. Cannot list encounters. No active patient. Cannot merge active patient into another patient. Cannot read version information from:

%s Cannot retrieve version information from:

%s Cannot run [arriba] ! Categories Category Category name '%s' already in use. Category name '%s' not valid. Must be non-empty. Category: Center Center Event texts Check Check All Check all children Check all parents Check children Check if this reaction applies to this drug/generic only, not the entire drug class. Check this if this allergy/intolerance is known to exist for sure as opposed to suspected. Check this if you want GNUmed to remember your decision and not ask you again. Choose file Choose what type of timeline you want to create. Chronological EMR Journal
 Clear all fields or reset to database values. Click on it.

Hold down Ctrl while clicking events to select multiple. Clinical data generated during encounters under this health issue: Clinical data generated during encounters within this episode: Close Codes relevant to the Assessment of Encounter
separated by ";". Codes relevant to the Reason for Encounter
separated by ";". Color Color used when the Event has no category associated with it. Color: Colorize weekends Colour: Colours Comment Concepts Connect to GNUmed Connecting to backend Contact Container Subevent 1
Click on the event to get the resize handles Container: Continue the existing recent encounter. Copy and paste this email into your favorite email client and send it from there. Could not find iCalendar Python package. It is required for working with ICS files. Could not find markdown Python package. It is needed by the help system. Could not find page '%s'. Could not find pysvg Python package. It is needed to export to SVG. Create &Event... Create &Milestone... Create Event Create Milestone Create Timeline Create a new timeline Create event Create new timeline Created during encounter: %s (%s - %s)   [#%s] Current D: Scientific diagnosis Data of current patient to be displayed here. Date && Time Date and time when the current (!) encounter ends. Date and time when the current (!) encounter started. Day Dec Default Event box drawer Default client Default color Default type for new encounters. Delete Delete event Description Details are found on <http://wiki.gnumed.de>.
 Device(%s): Dialog moved Directory Display checkmark when events are done Displayed period must be > 0. Displays drug classe(s) along with their ATC code. Distance between selected events Do you want GNUmed to show the encounter
details editor when changing the active patient ? Document signs and symptoms. If reaction is to a drug also document time of onset after drug administration (<24h, 24-72h, >72h). Documents: Documents: %s Done Color Done Color: Double click on an event. Double click somewhere on the timeline.

Hold down Ctrl while dragging the mouse to select a period. Drag me Drug Drug Classes Duplicate Event Duplicate... Duration ERROR: unknown allergy state [%s] Edit Edit &Categories Edit Allergy/Intolerance Edit Categories Edit Category Edit Container Edit Era's Edit Era's... Edit Event Edit Milestone Edit Shortcuts Edit an Era Edit categories Edit corresponding encounter Edit encounter details before change of patient. Edit event Edit the details for the encounter below: Edit... Editing local encounter type name Email Feedback Encounter Encounter ... Encounter Actions: Encounter type Encounter: Encounters ... Encounters: %s (%s - %s): End End must be > Start Ends Today Ends today Enlist Enlist person as GNUmed user Enlist this person as a GNUmed user and associate it with the given database account. Enlisting GNUmed users is a priviledged operation.
You must enter the password for the database administrator "gm-dbo" here. Enter the Encounter Summary here. This is your final assessment of the total encounter across all relevant episodes. Enter the Reason For Encounter here. This is the patient's initial request or purpose of visit which led to the encounter. Episode Episode %s%s%s   [#%s] Episodes: %s (most recent: %s%s%s) Era Era Properties Error Error retrieving encounters for episode
%s Error retrieving encounters for this episode. Error retrieving episodes for this health issue. Error running gnuplot. Cannot plot data. Error running hook [%s] script. Error running pdflatex. Cannot turn LaTeX template into PDF. Error running pdftk. Cannot extract fields from PDF form template. Error running pdftk. Cannot fill in PDF form template. Error running pdftk. Cannot flatten filled in PDF form. Event Editor Tab Order Event Properties Event appearance Events Events are overlapping or distance is 0 Events belonging to '%s' will no longer belong to a category. Events belonging to '%s' will now belong to '%s'. Exit Exit the program Experimental Features Export Export Timeline Export Timeline to File... Export to Image Export to Listbox... Export to SVG Exported: %s
 Extend Container height Family History Features Feb Feedback Feedback On Feature File File '%s' does not exist. File '%s' exists. Overwrite? File does not exist. Find First select me and then drag the handles. First time using Timeline? Fit Century Fit Day Fit Decade Fit Millennium Fit Month Fit Week Fit Year Font Color: Fonts Forward Forward One Mont&h Forward One Wee&k Forward One Yea&r Frequency: Fri Fuzzy Fuzzy icon Generics Getting started tutorial Give &Feedback... Give Feedback Give feedback Gmail Go back one page Go forward one page Go to &Date... Go to &Time Go to &Today Go to &Zero Go to Date Go to Time Go to home page Go to previous time period Goto URL Gradient Event box drawer Gregorian Happened Has allergies Health Issue %s%s%s%s   [#%s] Health issue Help Help contents Help requested
-------------- Hold down Ctrl while scrolling the mouse wheel.

Hold down Shift while dragging with the mouse. Hover me! Hovering events with a triangle shows the event description. Hyperlink Hyperlink icon Icon Icons Image files Images will be scaled to fit inside a %ix%i box. Impedance: Implanted: Import events Import events... Import... In this area GNUmed will place the notes of the
previous encounter as well as notes by other
staff for the current encounter.

Note that this may change depending on which
active problem is selected in the editor below. Information Injection site Intro Invalid Data Invalid date Invalid end time Invalid start time Is Container Is Subevent Jan Jul Jun Lab Lab result Last Date Last given Last worked on: %s
 Latest vaccinations Left Left click somewhere on the timeline and start dragging.

You can also use the mouse wheel.

You can also middle click with the mouse to center around that point. Legend Position Legends: List all encounters including empty ones. Location Locked Locked icon Lot \# Major Strips: Major strip divider line: Managing encounter types ... Mar Mark Event as Done Maximum duration of an encounter. May Measurements and Results: Measurements and Results: %s Medication history Medication list Milestone Properties Milestones Minimum age of an empty encounter before considering for deletion. Minimum duration of an encounter. Minor Strips: Minor strip divider line: Mon Monday Month Move encounters Move event down Move event up Move event vertically Name Name: Narrative Never show period Events as point Events Never use time precision for events New New notelets in current encounter New notes in current encounter New... No No [arriba] result found in [%s]. No encounters found for this health issue. No known allergies No test results to format. No timeline opened. No timeline set No, never show the encounter editor even if it would seem useful. Note, however, that this version may not yet
be available *pre-packaged* for your system. Now line: Number of duplicates: Numeric Occupations Oct Open &Recent Open Timeline Open an existing timeline Open most recent timeline on startup Open to encounter level Opening timeline instead of creating new. Optional: A system-wide description for this encounter type. If you leave this empty the local name will be used.

It is useful to choose an English term but that is not mandatory. One advantage to using a system-wide type description is that different people can have the system description translated into their language and still use the same encounter type. Organization: %s (%s) Other Other Gradient Event box drawer Other email client PDF output file cannot be opened. Page not found Parent Parent: Password Password, again Patient: %s (%s), No: %s
 Period Please select your database language from the list below.

Your current setting is [%s].

This setting will not affect the language the user interface
is displayed in but rather that of the metadata returned
from the database such as encounter types, document types,
and EMR formatting.

To switch back to the default English language unselect all
pre-selected languages from the list below. Point event alignment Preferences Prescribed For Previous Encounters Print Problem Procedures performed: %s Proceed and try to connect to the newly started GNUmed client. Progress Progress %: Progress Color Progress Color: Progress notes in most recent encounter: Put caption here. Put question here. Python version: Quantity Questions and answers Reaction Reaction Type Read Only Reason for Encounter Redo Related pages Remember and don't ask again. Required: A name for this encounter type in your local language. Resize and move me! Right-click for context menu where the hyperlinks can be accessed. SVG files Save As... Save Timeline As Save encounter details and all progress notes. Save the allergy/intolerance in the database. Save the currently displayed note into an encounter selected from a list of encounters. Save the currently displayed progress note under the current encounter. Save the encounter details. Saving Script must be readable by the calling user only (permissions "0600"): [%s]. Script must be readable by the calling user: [%s]. Script must not be a link: [%s]. Scroll Search results for '%s' Select All Events Select Category Fields Select Category... Select Date format Select Event Editor Tab Order Select Event Fields Select Export File Type Select Fields to Export Select Fields... Select Icon Select Items to export Select Tab Order: Select Text Encoding Select a Category: Select corresponding encounter Select event Select events to be deleted and press the Del key. Select region to zoom into Select the type of encounter. Select this if the reaction is a sensitivity. Select this if the reaction is an allergy. Select timeline to import from: Selecting events Send With: Sensing: Sep Set Category on Selected Events... Set Category on events &without category... Set Category on events without category Set Category on selected events Shortcut Key: Shortcut is saved Shortcuts Shortcuts... Show time Since the directory of the Timeline file is not writable,
the timeline is opened in read-only mode Skip s in decade text Someoneelse has changed the Timeline.
You have two choices!
  1. Set Timeline in Read-Only mode.
  2. Synchronize Timeline.

Do you want to Synchronize? Start Start a new encounter for the active patient right now. Start a new encounter. If there are any changes to the current encounter you will be asked whether to save them. Start a new encounter. The existing one will be closed. Start new encounter Start of new encounter Start slide show Started a new encounter for the active patient. Status Sticky Balloon Strength Substance Sun Sunday System Information System information System version: Target Tasks Te&st Test results Text Text: The currently selected patient is: The database account for this user.

The account will be created in the database with proper access rights. Privacy restrictions are currently hardcoded to membership in the PostgreSQL group "gm-doctors".

You can use the name of an existing account but it must not be used by any other GNUmed user yet. The database currently holds no translations for
language [%s]. However, you can add translations
for things like document or encounter types yourself.

Do you want to force the language setting to [%s] ? The database password must be typed again to enable double-checking to protect against typos. The lab request already exists but belongs to a different patient. The lockfile used to protect the timeline from concurrent updates is opened by another program or process.
This lockfile must be removed in order be able to continue editing the timeline!
The lockfile is found at: %s The password for the new database account. Input will not be shown. The selected timeline has a different time type. The shortcut %s is already bound to function '%s'! The specified timeline already exists. The wxPython GUI framework hasn't been initialized yet! There are no encounters for this episode. There are no episodes for this health issue. There is no version information available from:

%s This creates a timeline that has numbers on the x-axis instead of dates. This creates a timeline using the fictuous Bosparanian calendar from the German pen-and-paper RPG "The Dark Eye" ("Das schwarze Auge", DSA). This creates a timeline using the standard calendar. This creates a timeline where the modification date of files in a directory are shown as events. This dialog has been removed. Edit categories in the sidebar instead. This displays the current allergy state as saved in the database. This documents why the encounter takes place.

It may be due to a patient request or it may be prompted by other reasons. Often initially collected at the front desk and put into a waiting list comment. May turn out to just be a proxy request for why the patient really is here.

Also known as the Reason For Encounter/Visit (RFE). This event has hyperlinks This shows the list of previous encounters This summarizes the outcome/assessment of the consultation from the doctors point of view. Note that this summary spans all the problems discussed during this encounter. This timeline is not being saved. This timeline is read-only. This timeline is stored in memory and modifications to it will not be persisted between sessions.

Choose File/New/File Timeline to create a timeline that is saved on disk. Threshold Thu Timeline Timeline files To edit this timeline, save it to a new file: File -> Save As. To save this timeline, save it to a new file: File -> Save As. To: Toolbar Top-Left Top-Right Total number of vaccinations recorded for the corresponding target condition. Translate this or i18n into <en_EN> will not work properly ! Trigger Tue URL: Unable to copy to clipboard. Unable to delete backup dbfile '%s'. Unable to display the file:

 [%s]

Your system does not seem to have a (working)
viewer registered for the file type
 [%s] Unable to load events: %s. Unable to open timeline '%s'. Unable to read from file '%s'. Unable to read from filename '%s'. Unable to read timeline data from '%s'. Unable to rename temporary dbfile '%s' to original. Unable to save timeline data to '%s'. File left unmodified. Unable to take backup to '%s'. Unable to take lock on %s
This means you can't edit the timeline.
Check if you have write access to this directory. Unable to write configuration file. Unable to write timeline data. Unable to write to temporary dbfile '%s'. Unattributed episodes Uncheck Uncheck All Uncheck all children Uncheck all parents Uncheck children Uncheck time checkbox for new events Undo Undo/Redo Unknown Use extended Container strategy Use inertial scrolling User parameters: Vaccination Vaccinations Vaccinations: Vaccine Verify which patient this lab request really belongs to. Version information loaded from:

 %s Vertical Zoom &In Vertical Zoom &Out Vertical Zoom In Vertical Zoom Out Vertical space between Events (px) View Categories Individually View Container demo video Warning Wed Week Week start on: Weekends: Welcome Welcome to Timeline What is the patient here for. Could be the Reason for Encounter. When a patient is activated GNUmed checks the
age of the most recent encounter.

If that encounter is older than this age
GNUmed will always start a new encounter.

(If it is very recent the existing encounter
 is continued, or else GNUmed will ask you.)
 When a patient is activated GNUmed checks the
age of the most recent encounter.

If that encounter is younger than this age
the existing encounter will be continued.

(If it is really old a new encounter is
 started, or else GNUmed will ask you.)
 When a patient is activated GNUmed checks the
chart for encounters lacking any entries.

Any such encounters older than what you set
here will be removed from the medical record.

To effectively disable removal of such encounters
set this option to an improbable value.
 When activating a patient, do you want GNUmed to
auto-open editors for all active problems that were
touched upon during the current and the most recent
encounter ? When was the allergy state last confirmed. When: Where do the week numbers go if I start my weeks on Sunday? Where is the save button? Why is Timeline not available in my language? Will associated events be deleted when I delete a category? Year Yes Yes, auto-open editors for all problems of the most recent encounter. Yes, show the encounter editor if it seems appropriate. You are trying to open an old file with a new version of timeline. Please install version 0.21.1 of timeline to convert it to the new format. You can't change time when the Event is locked You must select a category! Zoom Zoom &In Zoom &Out [%s:%s]: row not updated (nothing returned), row in use ? [%s] is not a readable file active already reported animal bilateral boy child clinically relevant closed corrected result data date of birth date of birth/death day daylight savings time in effect days edit encounter details episode     : %s error retrieving unreviewed lab results error with placeholder [%s] export files external patient ID external patient source (name, gender, date of birth) female final finished first name full name generic error message generic info message generic warning message girl gm_ctl_client: starting slave GNUmed client health issue: %s hour hours in %d day(s) - %s in %d day(s): %s in %d hour(s): %s in %d month(s): %s in %d week(s) - %s in %d week(s): %s in %d year(s): %s inactive internal patient ID invalid age: DOB after death invalid age: DOB in the future lab [%s], request ID [%s], expected link with patient [%s], currently linked to patient [%s] last check: left long-term male minute minutes missing, reported later modified entry name name, date of birth name, gender, date of birth name: first-last name: last, first name: last-first names: first last names: first-last, date of birth names: last, first names: last-first, date of birth names: last-first, dob need lab request when inserting lab result no hook specified, please report bug no known allergies not clinically relevant ongoing ordered by brand original entry partial per target condition preliminary programmer forgot to specify error message programmer forgot to specify info message programmer forgot to specify warning read-only right right now (%s, %s) second seconds short-term to today (%s) tomorrow (%s) units unknown allergy state unknown gender unknown reaction unknown test results output format [%s] unknown test status [%s] unknown vaccinations output format [%s] week weeks wxPython version: year years yesterday (%s) Project-Id-Version: gnumed
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-04-07 10:15+0000
Last-Translator: Rikard Edgren <Unknown>
Language-Team: Swedish <kde-i18n-doc@kde.org>
Language: sv
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2012-06-09 20:37+0000
X-Generator: Launchpad (build 15376)
 

Var vänlig konsultera felloggen för vidare information 
En ny undersökning initierades till den valda patienten.
 
Kategorier används för att gruppera händelser. En händelse kan tillhöra en och endast en kategori. Alla händelser som tillhör samma kategori visas med den bakgrundsfärg som man valt för kategorin.
 
Kategorier kan redigerasi dialogen "Redigera Kategorier" (*Timeline* > * Redigera Kategorier*). För att ändra data för en kategori så dubbelklickar du på den.

Om befintliga kategorier ska visas  i sidopanelen eller inte är valfritt (*Visa* > *Sidopanel*)
 
Om du har fler frågor om Timeline eller om du vill komma i kontakt med användare och utvecklare av Timeline, skicka ett email till mailinglistan för användare: <thetimelineproj-user@lists.sourceforge.net>. (Vänligen skriv på engelska.)
 
Nej. Händelserna kommer fortfarande att vara kvar men de kommer inte att tillhöra någon kategori.
 
Välj den undersökningstyp du vill redigera!
 
Dialogen *Skapa Händelse* kan öppnas på följande sätt:

- Välj *Tidslinje* - *Skapa Händelse* från menyn
- Dubbelklicka med den *vänstra* musknappen på tidslinjen
- Håll *Ctrl* tangenten, håll *vänster* musknapp nedtryckt, dra den över tidslinjen och släpp den.
 
Datumobjektet som används stöder inte veckonummer för veckor som börjar på en Söndag. I framtiden planerar att använda olika datumobjekt som stödjer detta.
 
Det finns ingen sparaknapp. Timeline sparar automatiskt ditt data när det behövs.
 
Timeline utvecklas och översätts av volontärer. Om du vill hjälpa till att översätta så är du mycket välkommen att kontakta oss.
 
För att ta bort en händelse, markera den och tryck på *Del*. Flera händelser kan tas bort samtidigt.
 
För att markera en händelse så klickar man på den. För att markera flera händelser samtidigt håller man ner *Ctrl* knappen samtidigt som man klickar på händelserna.
   risk: %s   er tid: %s - %s  (@%s = %s%s)
  %s aktiv medicinering
  %s dokument
  %s undersökningar %s till %s
  %s kända problem.
  %s test resultat
  (senast bekräftad %s)  (pågående)  ***** KONFIDENTIELLT *****  - korrigeringar och nya egenskaper
  - bug korrigeringar endast
  - det kan vara nödvändigt att underhålla eller korrigera databasen
  - uppgradering av databasen behövs
  Skapad vid undersökning: %s (%s - %s)   [#%s]  De allra senaste: %s - %s  Ny version: "%s"  Observerat vid ålder: %s  Nuvarande version: "%s"
  bidragande faktor till patientens död  inställning [%s]: %s %d händelser duplicerades inte pga ogiltiga datum %d dag(ar) sedan: %s %d händelser kommer att importeras för %d timmar sedan: %s för %d månader sedan: %s för %d veckor sedan: %s för %d år sedan. %s %d. av %s (senaste månaden) - en %s %d. av %s (förra månaden): en %s %d. av %s (nästkommande månad) - en %s %d. av %s (nästa månad): en %s %d. av %s (innevarande månad) - en %s %d. av %s (innevarande månad): en %s %s (%s senaste året) %s(%s nästkommande år) %s (%s innevarande år) %s Byte %s undersökning(ar) (%s - %s): %s gömda händelser %s andra under denna undersökning berörda episoder: %s-%s-%s: en %s senaste månaden %s-%s-%s: en %s nästa månad %s-%s-%s: en %s innevarande månad %s: %s by %.8s (v%s)
%s %s: undersökning (%s) %sAllergi: %s, %s (nedtecknat %s)
 %sEpisod "%s" [%s]
%sUndersökningar: %s (%s - %s)
%sSenast bearbetad: %s
 &Allergi &Lägg till &Ballonger vid hovring Komprimera timelinehändelser &Definit &Duplicera Markerad Händelse... &Redigera &Editera valda Händelser... &Undersökning &Undersökningar &Förstora &Arkiv &Hjälp Visa inte färdiga händelser &Teckenförklaring &Mät avståndet mellan två händelser... &Navigera Sk&rivskyddad &Gör om &Reducera Ta bo&rt &Överkänslighet &Tidslinje &Tolererar övriga i kategori &Ångra &Visa ** Födelsedag okänd ** *har* allergier ... undersökningsnivå 1 Byte 1-period 10-period 100-period 1000-period Första och de (som mest tre) senaste (av totalt %s) undersökningar (%s - %s): <%s>: okänd diagnostisk felsäkerhetsklassificering fältet <kanal> kan ej lämnas tomt <nuvarande allerigiskt tillstånd> fältet <utfärdare> kan ej lämnas tomt <senast verifierad> fältet <namn> får ej lämnas tomt fältet <antal> kan ej lämnas tomt fältet <gata> kan ej lämnas tomt fältet <postort> kan ej lämnas tomt fältet <postkod> kan ej lämnas tomt ? ?pågående ?kort sikt En GNUmed-klient i slavläge har startats eftersom
ingen exekverande sådan kunde hittas. Ni kommer nu
att behöva mata in ert användarnamn och lösenord med\ hjälp av inloggningsdialogen som vanligt.

Växla nu till GNUmeds inloggningsdialog och fortsätt
med inloggningsproceduren. När GNUmed väl har startat
 kan ni växla tillbaks till detta fönster.
 En nyare version av GNUmed finns tillgänglig.

 Kort alias som identifierar GNUmed-användaren. Det används bland annat i medicinska register. A: Fynd Avbryt Avbryt och anslut EJ till GNUmed. Om Acceptera negativa värden på Julianska dagar Konto Lägg till Kategori Lägg til Behållare Lägg till en Era Lägg till fler händelser efter denna Lägg till ny Lägg till... Lägger till ny undersökningstyp Administrators lösenord Rekommendation Larm Alias Allergier och överkänslighet
 Allergi Allergihanterare Detaljer om allergiskt tillstån Allergiskt tillstånd Ett fel inträffade under utmatning av Demografiska akter.
Var vänlig konsultera loggfilen för mer ingående information. Ett fel inträffade vid inhämtande av en text-
utmatning gällande EMR för vald patient.

Var vänlig kolla loggfilen för mer ingående information. Apr Är du säker på att du vill radera kategorin '%s'? Är du säker på att du vill ta bort den här händelsen? Utvärdering an Undersökning Minst ett kategorifält måste väljas. Minst ett händelsefält måste väljas. Minst ett exportobjekt måste väljas. Aug B: Fyndkluster Bakåt en &Månad Bakåt en &Vecka Bakåt ett &År Bakgrund Bakåt Batteri: Innehåll: Född: %s, ålder: %s

 Både och Både modifierare och snabbtangent måste anges! Nederst till vänster Nederst till höger C: Övergripande diagnos CSV-Fil Kan fler personer arbeta samtidigt med samma timeline? Kan inte upprätta förbindelse med databasen utan inloggningsuppgifterna! Det går inte att flytta låsta händelser Det går inte att scrolla mer åt vänster Det går inte att scrolla mer åt höger Det går inte att zooma djupare än 1 minut Det går inte att zooma in mer än 5 Avbryt Avbryt redigering av Allergi/Intolerans. Avbryt denna procedur. Registrera ej ny GNUmed-användare. Kan inte ansluta till databasen utan inloggningsuppgifterna! Kan inte radera undersökningstyp [%s]. Den används. Kan inte lista undersökningar. Ingen patient vald. Kan ej införliva vald patients akt med annan patients akt. Kan inte läsa information om version från:

%s Kan inte inhämta information om version från:

%s Kan inte exekvera [arriba] ! Kategorier Kategori Kategorinamnet '%s' används redan. Ogiltigt kategorinamn '%s'. Får inte vara blankt. Kategori: Centrera Centrera händelsens text Markera Markera alla Markera alla undernivåer Markera alla överliggande nivåer Markera undernivåer Undersök om denna reaktion gäller endast denna medicin/generika och inte en hel kategori av medicinen. Bocka i här om denna allergi/intolerans är fastställd i kontrast till förmodad. Bocka i detta val om du vill att GNUmed skall registrera ditt val och inte ånyo fråga dig i fortsättningen. Välj fil Välj den typ av timeline som du vill skapa Kronologisk EMR Journal
 Nollställ alla fält eller återställ till databasens värden. Klicka på den.

Håll ner Ctrl-knappen då du klickar för att markera flera händelser. Klinisk data som genererats vidundersökning och som gäller detta hälsotillstånd: Klinisk data från undersökningar under denna episod: Stäng Koder som berör Utvärdering av Undersökning
separerade med ";". Koder som berör Anledning till Undersökning
separerade med ";". Färg Färg som används för händelser som inte är associerade med någon kategori. Färg: Färgmarkera veckoslut Färg: Färger Kommentar Begrepp Anslut till GNUmed Ansluter till databasen via databasens direkta gränssnitt. Kontakta Händels i container
Klicka på händelsen för att få handtag för storleksändring Behållare: Fortsätt den redan initierade föregående undersökningen. Kopiera och klistra in detta email i den epostklient du föredrar och skicka det därifrån. Kunde inte hitta Pythonmodulen iCalender. Denna modul krävs för att kunna hantera ICS-filer Kunde inte hitta python-modulen markdown. Den är nödvändig för att hjälpsystemet ska fungera. Kunde inte hitta sidan '%s'. Kunde inte hitta Pythonmodulen pysvg. Modulen behövs för att kunna exportera till SVG-filer Skapa &Händelse... Skapa &Milstolpe Skapa Händelse Skapa Milstolpe Skapa Tidslinje Skapa en ny tidslinje Skapa händelse Skapa en ny timeline Skapad vid undersökning: %s (%s - %s)   [#%s] Aktuellt D: Vetenskaplig(a) beteckning(ar) av diagnos Information angående aktuell patient som skall visas här. Datum && tid Datum och klockslag då vald undersökning slutar. Datum och klockslag då vald undersökning startade. Dag Dec Standardritare för Händelsers rektangel Förvald klient Förvald färg Initial tilldelning av typ på en ny undersökning. Ta bort Ta bort händelse Beskrivning Mer ingående information finns att tillgå på <http://wiki.gnumed.de>.
 Enhet(%s): Dialogen är flyttad Katalog Visa check-markering för färdiga händelser Perioden måste vara större än 0. Visar medicinkategorier tillsammans med deras ATC-koder. Avstånd mellan markerade händelser Vill ni att GNUmed skall visa undersökningsredigerare 
när ni växlar aktiv patient? Dokumentera tecken och symptom. Om reaktionen utlöses av en medicin, dokumentera även hur länge efter medicinintaget, anfallet ägt rum (<24h, 24-72h, >72h). Dokument: Dokument: %s Färg för Färdig Färg för färdig: Dubbelklicka på en händelse. Dubbelklicka vid önskad punkt på tidslinjen.

Håll nere Ctrl-knappen då du navigerar och väljer en period med musen. Dra mig Medicin Medicinkategorier Duplicera händelse Duplicera... Varaktighet FEL: Okänt allergiskt tillstånd [%s] Redigera Redigera &Kategorier Redigera Allergi/Intolerans Redigera Kategorier Redigera Kategori Redigera Behållare Redigera Eror Redigera Eror Redigera Händelse Ändra Milstolpe Redigera snabbtangenter Redigera en Era Redigera kategorier Redigera motsvarande undersökning Redigera undersökningsdetaljer innan byte av patient. Redigera händelse Redigera undersökningen nedan: Redigera... Redigerar det lokala namnet för undersökningstypen Återkoppling Email Undersökning Undersökning ... Undesökningsåtgärder: Typ av undersökning Undersökning: Undersökningar ... Undersökningar: %s (%s - %s): Slut Slut måste vara > Start Slutar idag Slutar idag Registrera Registrera person som GNUmed-användare Registrera denna person som GNUmed-användare och associera dennes konto till det givna databaskontot. Att registrera GNUmed användare är en privilegierad operation.Du måste mata in lösenord för databasadministrator  "gm-dbo" här. Ange Undersökningens Sammanfattning här. Detta är den slutgiltiga värderingen av den sammanlagda undersökning för alla relevanta episoder. Ange Orsak Till Undersökning här. Patientens första begäran eller anledning till besöket. Vad som föranledde undersökningen. Episod Episod %s%s%s   [#%s] Episoder: %s (de allra senaste: %s%s%s) Era Egenskaper för Era Fel Fel vid inhämtande av undersökningar för denna episod
%s Fel vid inhämtning av undersökningar för denna episod. Fel vid mottagande av episoder för detta vårdärende Felmeddelande från programmet gnuplot. Kan ej plotta data. Fel då hulling (eng. hook) i skript [%s] exekverades. Fel från programmet pdflatex. Kan inte transformera formatet LaTeX till PDF för detta dokument. Fel från programmet pdftk. Kan inte extrahera fält från mall för PDF-formulär. Fel från programmet pdftk. Kan inte fylla i mall för PDF-formulär. Fel från programmet pdftk. Kan inte formulera ifyllt PDF-formulär på slutgiltig form. Händelseredigerarens Tab-ordning Egenskaper för Händelse Utseende på händelser Händelser Antingen överlappar händelserna varandra eller så är  avståndet 0 Händelser knutna till '%s' kommer inte längre att vara knutna till någon kategori. Händelser knutna till '%s' kommer nu att vara knutna till '%s'. Avsluta Avsluta programmet Experimentella funktioner Exportera Exportera Exportera till en fil... Exportera till Bild Exportera till en listbox... Exportera till SVG Exporterat: %s
 Utöka Behållarens höjd Familjehistoria Funktioner Feb Återkoppling Återkoppling av funktion Arkiv Filen '%s' finns inte. Filen '%s' finns redan. Vill du skriva över den? Filen finns inte. Sök Markera mig först och dra sedan i något handtag. Första gången du använder Timeline? Anpass Århundrade Anpassa Dag Anpassa Decennium Anpassa Millenium Anpassa Månad Anpassa Vecka Anpassa År Textfärg Teckensnitt Framåt Framåt en Må&nad Framåt en Vec&ka Framåt ett Å&r Frekvens: Fre Luddig Ikon för obestämd period Generika Kom igång övning Ge &Feedback... Tyck till/Lämna respons Ge återkoppling Gmail Gå bakåt en sida Gå framåt en sida Gå till &Datum... Gå till &Tid Gå till &Idag Gå till &Noll Gå till Datum Gå till tid Gå till startsidan Gå till föregående tidsperiod Gå till URL Gradientritare för Händelsers rektangel Gregoriansk Inträffat Har allergier Hälsotillstånd %s%s%s%s [#%s] Vårdbeteckning Hjälp Hjälpinnehåll Hjälp har begärts
------------------------ Håll nere Ctrl-knappen medan du rullar med mushjulet.

Håll nere Shift-knappen medan du navigerar med musen. Rör musen över mig! Om man rör musen över en händelse med triangel, så visas händelsens beskrivning Hyperlänk Ikon för hyperlänk Ikon Ikoner Bildfiler Bilder kommer skalas ner så att de inte är större än %ix%i. Impedans: Implanterad: Importera händelser Importera händelser... Importera... I detta utrymme kommer GNUmed placera anteckningarna från
tidigare undersökningar och anteckningar från annan personal
som berör denna undersökning.

Notera att detta kan ändra sig beroende på vilka 
aktiva problem valts i redigeraren nedan. Information Behandlingslokal Intro Ogiltigt Data Ogiltigt datum Ogiltig Sluttid Ogiltig Starttid Är en Behållare Är en Underhändelse Jan Jul Jun Lab Labsvar Senaste datum Senast angiven Senast bearbetad: %s
 Senaste vaccineringar Vänster Klicka på tidslinjen och dra för att navigera.

Du kan även använda mushjulet.

Du kan även klicka med mittenknappen för att centrera vid den önskade punkten. Position på teckenförklaring Teckenförklaringar Lista alla undersökningar, tomma inkluderat. Position Låst Ikon för låst händelse Part\# Huvudskalor: Huvudskalans linje Hanterar undersökningstyper ... Mar Klarmarkera Händelsen Maximal tidsåtgång för en undersökning. Maj Provtagningar och resultat: Provtagningar och Resultat: %s Medicineringshistorik Medicinlista Egenskaper för milstolpe Milstolpe Minsta tid från då en tom undersökning initierats tills dess att den kan beaktas för radering. Minsta tidsåtgång för en undersökning. Underskalor: Underskalans linje Mån Måndag Månad Flytta undersökningar Flytta händelser nedåt Flytta händelser uppåt Flytta händelser vertikalt Namn Namn: Patientens redogörelse Visa aldrig periodhändelser som punkthändelser Använd aldrig klockslag för händelser Ny Nya korta anteckningar för denna undersökning Nya anteckningar i aktuell undersökning Ny… Nej Inga [arriba] resultat funna i [%s]. Inga undersökningar hittades för detta hälsotillstånd. Inga kända allergier Inga provtagnings-resultat för formatsättning Ingen tidslinje öppnad. Ingen tidslinje vald Nej, visa aldrig undersökningsredigerare oavsett om det verkar lämpligt. Observera, att denna version inte säkert ännu
 finns tillgänglig *för-paketerad* för ert system. Nu-linjen Antal dupliceringar Numeriskt Sysselsättning Okt Öppna s&enaste Öppna Tidslinje Öppna en befintlig tidslinje Öppna senaste tidslinje vid start Öppen upp till undersökningsnivå Öppnar tidslinje istället för att skapa ny. Valfritt: En systemövergripande beskrivning för denna typ av undersökning. Om detta utelämnas kommer den inhemska benämningen att anges.

Det är meningsfullt att välja en engelsk benämning men inte obligatoriskt. En fördel med att använda en systemövergripande typbeskrivning är att olika mäniskor kan få denna beskrivning översatt till sitt språk och fortfarande använda samma typ av undersökning. Organisation: %s (%s) Övriga Annan gradientritare för Händelsers rektangel Annan epostklient PDF-resultatfil kan inte öppnas. Sidan hittades inte Förälder Förälder Lösenord Lösenord, igen Patient: %s (%s), Nummer: %s
 Period Var vänlig välj språk för databasen från listan nedan.

Nuvarande inställningar är [%s].

Denna inställning kommer inte att påverka vilket språk som används i 
användargränssnittet utan snarare det som används för den metadata som
returneras från databasen. Exempelvis undersökningstyper, dokumenttyper,
och EMR-formattering.

För att växla tillbaks till förvalt Engelskt språk, välj bort alla förvalda språk från
listan nedan. Justering av punkt-händelser Inställningar Utskriven för Föregående Undersökningar Skriv ut Problem Utförda behandlingar: %s Fortsätt och försök ansluta till den nyligen uppstartade GNUmed-klienten. Framsteg Framsteg %: Färg för Progress Färg för framsteg: Anteckningar om förbättring i senaste undersökning: Skriv sammanfattning här. Skriv frågan här. Pythonversion Antal Frågor och svar Reaktion Reaktionstyp Skrivskyddad Orsak till Undersökning Gör om Relaterade sidor Kom ihåg och fråga inte igen. Obligatoriskt: Ett inhemskt namn för denna typ av undersökning. Ändra storlek och flytta mig! Höger-klicka för att öppna menyn där hyperlänk kan väljas. SVG-filer Spara som… Spara Timeline som Spara undersökningens innehåll och alla utvecklingsanteckningar. Spara Allergi/Intolerans-uppgifter i databasen. Spara den för närvarande visade anteckningen till en undersökning vald från listan av undersökningar. Spara den för närvarande visade utvecklingsanteckningen till den valda undersökningen. Spara undersökningens innehåll. Sparar Skript (makro) måste vara läsbart enbart för användaren (tillstånd "0600"): [%s]. Skript (makro) måste vara läsbart för anropande användare: [%s]. Skript (makro) måste vara en riktig fil, inte en länk till en fil: [%s]. Scrolla Sökresultat för '%s' Markera alla Händelser Välj kategorifält Välj kategori... Välj datumformat Välj Tab-ordning för händelseredigeraren Välj händelsefält Välj filtyp att exportera till Välj fält som ska exporteras Välj Fält... Välj Ikon Välj objekt att exportera Välj Tab-ordning Välj Text-kodning Välj en kategori: Välj motsvarande undersökning Välj händelse Välj händelser som skall tas bort och tryck Del-knappen. Välj område att zooma in Välj typ av undersökning Bocka i här om reaktionen är en överkänslighet. Bocka i här om reaktionen är en allergi. Välj timeline som ska importeras Markera händelser Skicka med: Förnimmelse: Sep Sätt Kategori på valda Händelser... Välj kategori för alla händelser som saknar kategori... Sätt Kategori på Händelser utan Kategori Välj kategori för markerade händelser Snabbtangent: Genväg är sparad Snabbkommandon Genvägar... Visa tid Eftersom katalogen där Timelinefilen finns, inte är skrivbar, så öppnas Timelinefilen i Läs-läge Inget s i decenniumtext Timeline har blivit ändrad av någon annan. Början Initiera en ny undersökning för den valda patienten nu. Påbörja en ny undersökning. Om det sker några ändringar på vald undersökning kommer du att få frågan om dessa skall sparas eller inte. Initiera en ny undersökning. Den innevarande kommer att stängas. Initiera ny undersökning Initiera en ny undersökning Starta bildspel Initierade en ny undersökning för den valda patienten. Status Fäst ballong Styrka Substans Sön Söndag Systeminformation Systeminformation Systemversion Målvariabel Aktiviteter Te&st Provtagnings-resultat Text Text: Den för närvarande valda patienten är: Databaskontot för denna användare.

Kontot kommer att upprättas med tillbörliga åtkomsträttigheter. Databasens restriktioner gällande privat integritet är för närvarande fullkomligt definierade av användarens registrering i PostgreSQL-grupp "gm-doctors" och definieras endast i den PostgreSQL-gruppen.

Du kan använda ett namn på ett befintligt konto men detta skall då ej vara registrerat till annan GNUmed-användare ännu. Databasen har för närvarande inte stöd för det
språket [%s]. Dock kan ni lägga till översättningar
för dokument och undersökningstyper manuellt.

Vill ni tvinga språkinställning [%s]? Lösenordet för databaskontot måste ånyo matas in för att avvärja stavfel eller andra misstag. Kontrollera att ni inte har versal-lås-knappen aktiverad. Laborationsförfrågan existerar men tillhör annan patient. Låsfilen som används för att skydda timeline mot samtidiga uppdateringar från två användare, är öppnad av ett annat program eller process.
Låsfilen måste tas bort för att du ska kunna fortsätta ändra i din timeline.
Låsfilen hittar du här: %s Lösenord för det nya databaskontot. Inmatning visas ej. Den valda timelinen har en annan tidstyp Snabbtangenten %s är redan kopplad till funktionen '%s'! Den angivna tidslinjen finns redan. Ramverk wxPython för grafiska användargränssnittet,  har inte initialiserats ännu! Det finns inga undersökningar för denna episod. Det finn inga episoder för detta vårdärende Det finns ingen information om version tillgänglig från:

%s Detta alternativ skapar en timeline som har nummer på tidsaxeln i stället för datum Detta alternativ skapar en timeline med den påhittade Bosoporiska kalendern från den Tyska pen-and-paper RPG "The Dark Eye" (Das schwarze Auge", DSA) Detta alternativ skapar en timeline som använder datum på tidsaxeln Detta alternativ skapar en timeline som visar filer i en katalog som händelser. Händelsernas tidpunkt är lika med filernas ändringsdatum. Dialogen har tagits bort. Ändra kategorier i sidebaren i stället. Här visas det allergiska tillstånd som sparats som nuvarande i databasen. Dokumentation om varför undersökningen äger rum.

Om den initierats av patient eller skett av andra anledningar. Ofta noterat i kassan och registrerat som kommentar i väntelistan. Kan visa sig vara en ställeföreträdares begäran om undersökning.

Kallas Anledning till Undersökning/Besök (ATU) (Reason For Encounter/Visit (RFE)) Den här händelsen har en hyperlänk Här återfinns listan på tidigare undersökningar Summering av utfall/värdering av konsultationen från läkares perspektiv. Observera att sammanfattning gäller alla problem som diskuterats under denna undersökningen. Denna timeline blir inte sparad. Denna timeline är enbart läsbar. Denna Timeline är bara lagrad i internminnet och ändringar som görs kommer inte att permanentas.

Välj Arkiv/Ny/Timelinefil för att skapa en Timeline som sparas på disk. Tröskelvärde Tor Tidslinje Tidslinjefiler För att ändra i denna timeline, spara den till en ny fil: Arkiv -> Spara Som. För att spara denna timeline, spara den till en ny fil: Arkiv -> Spara Som. Till: Verktygsfält Överst till vänster Överst till höger Total antal vaccinationer som registrerats för detta mål av sjukdomstillstånd. Översätt detta eller så kommer inte i18n till <en_EN> konversionen att fungera riktigt! Utlösande faktor Tis URL: Misslyckades med att kopiera till klippbordet Kan inte radera backupen av databasfilen '%s'. Oförmögen att visa file:

 [%s]

Ert system verkar inte att ha ett (fungerande)
visningsprogram för den registrerade filtypen
 [%s] Kan inte läs in händelser: %s Kan inte öppna tidslinje '%s'. Kan inte läsa från filen '%s'. Det går inte att läsa filen '%s' Kunde inte läsa tidslinjedata från '%s'. Kan inte namnändra den temporära databasfilen '%s' till origininalnamnet Det gick inte att spara Timelinedata till '%s'. Filen är oförändrad. Det gick inte att göra backup till '%s'. Det lyckades inte att ta ett lås för %s
Det betyder att det inte går att ändra i din timeline.
Kontrollera om du har skrivrättigheter till katalogen där din timelinefile ligger. Det gick inte att skriva till konfigurationsfilen. Det går inte att skriva Timelinedata Kan inte skriva till temporär databasfil '%s'. Ej beaktade episoder: Avmarkera Avmarkera alla Avmarkera alla undernivåer Avmarkera alla överliggande nivåer Avmarkera undernivåer Avmarkera checkboxen 'Visa tid'  för nya händelser Ångra Ångra/Gör om Okänd Använd Behållarstrategin för utökad funktionalitet Använd scrollning med fördröjning Användarparametrar: Vaccinering Vaccinering Vaccinering: Vaccin Verifiera vilken patient denna laborationsförfrågan faktiskt tillhör. Versionsinformation hämtad från:

 %s Zooma in vertikalt Zooma ut vertikalt Zooma in vertikalt Zomma ut vertikalt Vertikalt mellanrum mellan händelser Visa enbart markerade Kategorier Se video  med Container demo Varning Ons Vecka Veckan börjar på Veckoslut Välkommen Välkommen till Timeline Varför är patienten här. Kan vara Orsak till Undersökning. När en patient aktiveras kommer GNUmed kontrollera
hur länge sedan den senaste undersökningen skett.

Om undersökning skett för längre sedan än vad som 
anges här kommer GNUmed alltid att starta en ny
undersökning.

(Om undersökningen skett väldigt nyligen kommer  
undersökning att fortsättas, eller så kommer GNUmed 
fråga er hur det skall behandla detta? )
 När en patient aktiveras kommer GNUmed att 
kontrollera tiden sedan den senaste undersökningen.

Om undersökningen skett senare än vad som anges här
kommer den innevarande undersökningen att fortsättas.

(Om undersökningen skett för mycket länge sedan kommer 
en ny undersökning att inledas, eller så kommer GNUmed fråga
er hur det skall behandla detta? )
 När ni växlar aktiv patient kontrollerar GNUmed den
samling undersökningar som saknar fält.

Vilken som helst sådan undersökning äldre än det
du anger här kommer att raderas från det medicinska registret.

För att garanterat avaktivera borttagning av sådana 
undersökningar ge denna ett otolkbart värde.
 När ni aktiverar en patient, vill ni då att GNUmed skall
automatiskt öppna redigerare för alla valda problem som
berörts denna och den senaste undersökningen? När bekräftades senast det allergiska tillståndet. När: Hur blir det med veckonummer om jag väljer att veckan ska börja på en Söndag? Var är sparaknappen? Varför finns inte Timeline på mitt språk? Kommer associerade händelser tas bort när jag tar bort en kategori? År Ja Ja, automatiskt öppna redigerare för alla problem som hör till den senaste undersökningen. Ja, visa undersökningsredigerare om det verkar lämpligt. Du försöker öppna en fil med ett gammalt format med en ny version av Timeline. Du måste installera version 0.21.1 av Timeline för att konvertera filen till det nya formatet. Det går inte att ändra tid när en händelse är låst Du måste välja en kategori! Zooma Zooma &in Zooma &ut [%s:%s]: rad har ej uppdaterats (inget har returnerats), rad upptagen? [%s] är inte en läsbar fil aktiv redan rapporterat djur bilateral pojke barn kliniskt relevant avslutat korrigerade resultat data födelsedag födelsedag/dödsdag dag Effektiv besparing av tid i dagsljus dagar Redigera undersökningens innehåll. Episod: %s fel vid inhämtande ej granskat laborationsresultat Fel för platshållare [%s] exportera filer externt patient-ID extern källa för patient (namn,kön,födelsedatum) kvinna slutgiltig avklarad Förnamn fullständigt namn Allmänt felmeddelanden Allmänt informationsmeddelande Allmänt varningsmeddelande flicka gm_ctl_client: startar GNUmed-klient i slavläge Kategori av medicinsk frågeställning: %s timme timmar om %d dagar - %s om %d dag(ar): %s om %d timmar: %s om %d månader: %s om %d veckor - %s om %d veckor: %s om %d månader: %s inaktiv internt patient-ID felaktig ålder: födelsedag efter dag då patient avlidit felaktig ålder: framtida födelsedag lab [%s], förfrågans ID [%s], förväntad koppling till patient [%s], förnärvarande kopplad till patient [%s] senaste granskning: vänster lång sikt man minut minuter fattas, rapporteras senare Ändrat fält. namn namn, födelsedag namn,kön,födelsedag namn: första-sista namn: sista, första namn: sista-första namn: först sist namn: första-sista, födelsedag namn: sista, första namn: sista-första, födelsedag namn: sista-första, födelsedag behöver laborationsförfrågan för att spara laborationsresultat ingen hulling (eng. hook) har specificerats, var vänlig rapportera denna bugg. Inga känd allergier inte kliniskt relevant pågående sorterad med avseende på märke Ursprungligt fält. delvis per målvilkor preliminär Felmeddelandet har ej specifierats. Informationsmeddelande har ej specifierats. Programmerare har glömt att specifiera Varningsmeddelande skrivskyddad höger Alldeles nu (%s, %s) sekund sekunder kort sikt till idag (%s) imorgon (%s) enheter Okänt allergiskt tillstånd okänt kön okänd reaktion okänt format på provtagnings-resultat [%s] Okända status för provtagning [%s] okänt format på resultat gällande vaccinationer [%s] vecka veckor wxPythonversion år år igår (%s) 