��    5      �  G   l      �     �     �     �  /   �                 3     F     O     g  
   s     ~     �     �     �     �     �     �     �     �     �  8        ;     B     I     M     S     g     n          �     �     �  
   �  	   �     �     �     �     �               %     *     /     C     _     p     �     �     �     �     �  -  �     �     	     0	  0   G	     x	     �	     �	     �	     �	  
   �	     �	     �	     
     
     4
     P
     S
     V
     ^
     e
     s
  D   �
     �
     �
     �
     �
     �
     �
     �
               $     7     >  
   F     Q     j     o     �     �     �     �     �     �     �     �     �          &     <     C     L         (                 
       '                       "      )   .                     /          -                 0              1   !      $      	   ,      &             5   *       +                      %   2         #   3                4         %s encounters from %s to %s
  %s known problems
  (last confirmed %s)  Created during encounter: %s (%s - %s)   [#%s]  Noted at age: %s  contributed to death of patient %s: encounter (%s) Battery: D: Scientific diagnosis Device(%s): Documents: Documents: %s Family History Measurements and Results: Measurements and Results: %s Mr Ok Problem Status Vaccinations Vaccinations: Verify which patient this lab request really belongs to. active animal boy child clinically relevant closed corrected result date of birth episode     : %s external patient ID female first name full name generic warning message girl health issue: %s inactive internal patient ID last check: male name name, date of birth name, gender, date of birth name: last-first names: first last no known allergies not clinically relevant partial relevant unknown reaction Project-Id-Version: GNUmed
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
  %s tilfælde fra %s til %s
  %s kendte problemer
  (sidst bekræftet %s)  Oprettet under konsultation: %s (%s - %s) [#%s]  Bemærket ved alderen: %s  bidrog til patientens død %s: tilfælde (%s) Batteri: D: Videnskabelig diagnose Enhed(%s): Dokumenter: Dokumenter: %s Familiehistorik Målinger og resultater: Målinger og resultater: %s Hr Ok Problem Status Vaccinationer Vaccinationer: Kontroller hvilken patient denne laboratorie forespørgsel tilhører aktiv Dyr Dreng barn klinisk relevant lukket Korrekt resultat Fødselsdag hændelse     : %s Ekstern patient ID kvinde fornavn fulde navn generisk advarselsbesked Pige Helbredsproblem %s inaktiv Internt patient ID sidste tjek: mand navn Navn, fødselsdag Navn, køn, fødselsdag Navn: efternavn-fornavn Navne: fornavn efternavn Ingen kendte allergier ikke klinisk relevant delvis Relevant Ukendt reaktion 