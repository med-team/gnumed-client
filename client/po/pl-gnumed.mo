��    �     d  9  �      �#  9   �#  �   �#     v$  L   �$  #  �$  �   �%  V   �&  �   '  i   �'  m   �'     k(     �(     �(     �(     �(     �(     �(     )     $)     7)  /   U)     �)     �)      �)     �)     �)     �)     �)     *     *     *     *  	    *  	   **     4*     :*     P*     b*     i*  >   k*  0   �*  '   �*  '   +  W   ++     �+     �+  #   �+     �+     �+  ,   �+     �+  !   �+  '   ,  
   G,     R,     q,     �,     �,     �,     �,     �,  X   �,     -  6   #-  ;   Z-  
   �-     �-  ,   �-  )   �-     .     .     &.     /.  -   F.     t.     �.  /   �.  4   �.     �.  %    /  *   &/  ,   Q/  B   ~/  +   �/  5   �/     #0  ,   ?0  D   l0  *   �0  1   �0     1  
   $1  "   /1  0   R1  	   �1     �1  M   �1  E   �1  ,   (2  -   U2  N   �2  �   �2     �3  -   �3     �3  F   �3     4     04  7   C4     {4     �4     �4     �4     �4     �4     �4     �4     �4  -   5     45     T5     a5     e5  `   l5  =   �5     6     6     26     >6     J6     h6  
   v6     �6     �6  d   �6     7     7     7      7     47     @7  9   P7     �7     �7  $   �7  B   �7  b    8  W   �8  3   �8  (   9     89     @9  "   L9     o9  -   u9  0   �9     �9  �   �9     �:     �:     �:  �   �:     f;     o;     s;     �;     �;     �;     �;     �;  
   �;  *   �;     <     )<     5<  
   =<  	   H<     R<     [<     _<     x<     �<     �<  
   �<     �<     �<     �<     �<     =     =  _   =  	   z=  <   �=     �=     �=     �=  0   �=  
   >  
   >  	   !>     +>     :>     @>     M>     T>     X>     \>     `>  
   d>  
   o>     z>  �   �>     1?  D   Q?     �?  #   �?     �?     �?     �?     �?      @     @  9   3@  #   m@     �@     �@     �@     �@     �@     �@  !   �@  *   �@  4   �@  -   ,A     ZA     nA     ~A     �A     �A  	   �A     �A     �A     �A     �A     �A  $   �A  
   �A  )   	B     3B     BB     RB     lB     sB     xB     �B     �B     �B     �B     �B     �B     �B  	   �B     C     
C     C     1C  ,   EC     rC  
   wC     �C     �C     �C     �C  2   �C  7   �C  5   D  2   JD      }D     �D     �D     �D  '   �D     �D  	   �D  8   E  	   ;E     EE  �   �E  -   ^F     �F     �F     �F     �F  
   �F  	   �F     �F     �F     �F     �F     �F     �F     G     G  "   G  ]   1G  F   �G  (   �G  J   �G  9   JH  8   �H  B   �H  :    I  2   ;I  C   nI  I   �I  2   �I  &   /J  C   VJ     �J  :   �J  7   �J  )   -K  ,   WK  (   �K  �   �K  	   ZL     dL     hL     qL  <   �L     �L     �L     �L     �L     M  '    M  ;   HM     �M  #   �M     �M     �M     �M     N     N     N     (N     0N  8   6N     oN     �N     �N     �N     �N     �N     �N  v   �N     =O  ;   CO     O  w   �O  -   P  ;   ?P     {P  .   P     �P  7   �P     Q     Q     Q     Q     (Q     /Q  	   =Q     GQ     KQ     QQ     eQ     �Q     �Q     �Q     �Q     �Q  '   �Q     �Q  5   R     >R     ER  
   NR  	   YR     cR     yR     �R     �R     �R     �R     �R     �R     �R     �R     �R     S     S     S     /S     KS     WS     jS     �S     �S     �S     �S  *   �S  )   �S  $   T  	   ,T     6T     ?T     ET  
   HT     ST     aT     wT     �T     �T  h  �T  <   V  �   LV     �V  >   W  $  KW  �   pX  p   �X  l   lY  �   �Y  \   eZ     �Z     �Z     �Z     [     [     -[     I[  ,   ][     �[  %   �[  7   �[     \     \  &   0\  
   W\     b\     u\     �\     �\     �\     �\     �\  
   �\     �\     �\     �\     �\     �\     ]  ;   ]  #   ?]  $   c]  *   �]  O   �]  
   ^     ^  !   ^     9^     E^  4   K^     �^  '   �^  -   �^     �^     �^     _     &_     ,_     B_     H_     ]_  Z   e_     �_  9   �_  E   �_     D`     L`  5   a`  6   �`     �`     �`     �`     �`  /   a     <a     Na  B   Wa  ;   �a     �a     �a  *   �a     $b  B   3b  @   vb  ;   �b  "   �b  >   c  K   Uc  2   �c  :   �c     d  	   /d  &   9d  @   `d  
   �d  
   �d  X   �d  7   e  1   He  F   ze  R   �e  �   f  	   g  J   g     eg  P   g     �g     �g  E   �g     :h     Ah     Mh     bh     zh     �h     �h     �h     �h  1   �h  *   �h     *i     6i     :i  l   @i  @   �i     �i     �i     j     "j  *   2j     ]j  
   kj     vj      �j  [   �j     k     k     k     k     6k     Ck  @   \k     �k  ;   �k  1   �k  B   &l  i   il  K   �l  &   m  %   Fm     lm  
   sm  "   ~m     �m  .   �m  A   �m  
   n    $n     'o     .o     Do  m   ]o     �o     �o     �o     �o      �o  :   p     Vp     bp     ip  ?   wp  &   �p     �p     �p     �p     q     (q     5q  	   8q     Bq     Wq     vq     �q     �q     �q  &   �q     �q     r     !r  f   'r     �r  @   �r     �r     �r     �r  O   s  
   [s     fs  	   ss     }s     �s     �s     �s     �s     �s     �s     �s     �s     �s     �s  �   t     �t  ;   �t     u  .   u     Hu     Pu     hu     ku      yu     �u  5   �u  $   �u     v     v     v      v     'v     ,v  2   0v  2   cv  =   �v  .   �v     w     #w  
   ;w     Fw     Lw     Ow     ]w     dw     iw      lw  !   �w  =   �w  
   �w  =   �w     6x     Vx     px     �x  	   �x  "   �x     �x     �x     �x     �x     �x     y     y     &y     7y     >y  !   Fy      hy  .   �y     �y     �y     �y     �y     �y     �y  7   z  @   @z  4   �z  5   �z     �z     {  
   #{     .{  ,   2{     _{     p{  B   �{     �{  m   �{  �   E|  9   �|     }     2}     7}     T}     Z}  
   a}  	   l}     v}  	   y}     �}     �}     �}     �}     �}     �}  c   �}  6   7~  !   n~  T   �~  6   �~  9     H   V  1   �  2   �  H   �  /   M�  2   }�  "   ��  -   Ӏ  (   �  2   *�  K   ]�     ��  ,   Á  4   ��  �   %�     ̂     ҂     ւ     �  ?   ��     :�     =�  !   B�  !   d�     ��  "   ��  I   ƃ  %   �  (   6�  !   _�     ��     ��     ��     ��     ��     Ǆ  	   ӄ  F   ݄     $�     7�     F�     J�     S�     m�     s�  m   ��     �  ^   ��      W�  �   x�  4   	�  M   >�     ��  <   ��     ͇  G   �     0�     =�     K�     S�     c�  %   l�     ��  	   ��     ��     ��     ψ     �     ��     �     �     3�  8   ?�     x�  L   ��     �     �     ��     ��  +   �     8�     K�     `�     l�  
   ��     ��     ��     ��     Ċ      Њ     �      �     	�      "�     C�     O�     d�  	   z�     ��     ��     ��  *   ��  *   ؋  8   �     <�     M�     U�     [�     ^�  
   k�     v�     ��     ��     ��     �   "     �   ~  �   �       �   w      t               �  Q  M   q   h      =         �    _   2   �           Y     v       �           �  "  �   �       �   z  �   j  X       �       �   �   k       W  �      *     Z   �   (   �   $   �  �   ?   7  v  -  �       u  a         �       �   A       j       ;       E  �      `  %        �   y          S              Y          �   �      R     �   �          '  C  �   �     <           �   <                           �       %   �   �   m  �       �      )  {  K       O       L  ,   }  �   �   �   +  �       O      D  �   g          a           N      �   I  �        �   �     �   �   d  }          0  �   �         #   L   {        �   �  �          �   /       �   �   �   K      �   �  �   z     p       �   s  S     |   =   �   Q   �          �   0       �                 5   �   r       �   �      �   �   �   )   �   �         1       e   �   &  �  ?  �   l   ^  �   +       �           U  3  b   9      i  N               J           &       b  ]       c         �   �   ~   �   �   �       f  �   X  w           [       �   �   k  �   6   �       W     �   �  G   .   ;  �  A     1  �  l     �              �       .      f   @  �  �  �   �   �   �  �   �   @      #      [  �     P  C       �   �     �   8  ^   	       �       �   M  �   V  �   m     !     �   �   �  
    x       �  �   T   �  `   	          J          �    x  F   >       E   4      !      9   5  3           �       h       _            �          q  /  �  �  �           (    ,        G  �   4  F  �      c  �   \  �  i       7   �   �  �   �   �   �   �  �   >  �       ]  n      *          �   -           �          �   R   �       �     t      B   :   '      r  �  $  d   �       �   �     �   �   �   6         �      2  e           �   �   s       T          �       �       �         �   8     V   y   u   I         �       o   �   P   �   �     �  o  n      H   B      �          U      \       �       �       �       �          D              g             
   Z  �   �   :  p  H  �  �       |   

Please consult the error log for all the gory details ! 
Categories are used to group events. An event can only belong to one category. All events that belong to the same category are displayed with the same background color.
 
Command
(SQL) 
No. The events will still be there but they will not belong to a category.
 
The *Create Event* dialog can be opened in the following ways:

- Select *Timeline* - *Create Event* from the menu.
- Double click with the *left* mouse button on the timeline.
- Press the *Ctrl* key, thereafter hold *left* mouse button down on the timeline, drag the mouse and release it.
 
The date data object used does not support week numbers for weeks that start on Sunday at present.  We plan on using a different date object that will support this in future versions.
 
There is no save button. Timeline will automatically save your data whenever needed.
 
Timeline is developed and translated by volunteers. If you would like to contribute translations you are very much welcome to contact us.
 
To delete an event, select it and press the *Del* key. Multiple events can be deleted at the same time.
 
To select an event, click on it. To select multiple events, hold down the *Ctrl* key while clicking events.
  %s active medications
  %s documents
  %s encounters from %s to %s
  %s known problems
  %s test results
  (last confirmed %s)  ***** CONFIDENTIAL *****  - bug fixes and new features
  - bug fixes only
  - database upgrade required
  Created during encounter: %s (%s - %s)   [#%s]  Most recent: %s - %s  Noted at age: %s  contributed to death of patient %s Bytes %s: encounter (%s) &Balloons on hover &Close &Delete &Edit &Help &Legend &Navigate &Timeline &View *does* have allergies ... episode level 1 Byte ? A clinical assessment of the measurement.
Usually by a doctor. A long, descriptive name for this form template. A new version of GNUmed is available.

 A short, catchy name for this template. A technical comment on the result.
Usually by the entering Medical Technical Assistant. A: Sign Abort Abort and do NOT connect to GNUmed. About Account Act on the items selected in the above list. Add Category Add a new item to the list above. Add above input as a new document type. Add an Era Add more events after this one Adding a new episode Address Admin password Age: Allergies and Intolerances
 Allergy An error occurred while Demographic record export
Please check the log file for details. Apr Are you sure you want to delete this episode ?

 "%s"
 Are you sure you want to delete this health issue ?

 "%s"
 Assessment Associate to episode: At least one Category Field must be selected At least one Event Field must be selected Aug B: Cluster of signs Battery: Born   : %s, age: %s

 Both Modifier and Shortcut key must be given! C: Syndromic diagnosis CSV File Can multiple users work with the same timeline? Can't connect to database without login information! Cancel Cancel and do NOT delete the episode. Cancel and do NOT delete the health issue. Cancel editing the data and discard changes. Cancel moving the narrative entries and leave them where they are. Cannot add health issue. No active patient. Cannot connect to database without login information! Cannot create episode
 [%s] Cannot create new health issue:
 [%(issue)s] Cannot delete episode. There is still clinical data recorded for it. Cannot link the document to episode

 [%s] Cannot merge active patient into another patient. Cannot run [arriba] ! Categories Category name '%s' already in use. Category name '%s' not valid. Must be non-empty. Category: Center Change the type of all documents currently having the selected document type. Change translation of selected document type for your local language. Check if this result is clinically relevant. Check if this result is technically abnormal. Check this if you want GNUmed to remember your decision and not ask you again. Check this to mark the document as reviewed upon import. If checked you can (and must) decide on "technically abnormal" and "clinically relevant", too. The default can be set by an option. Clear Clear all fields or reset to database values. Clear all fields. Click on it.

Hold down Ctrl while clicking events to select multiple. Clinically relevant Close this dialog. Closed episodes older than %s days on health issue [%s] Color: Concepts Connect to GNUmed Connecting to backend Contact Could not find page '%s'. Country Create &Event... D: Scientific diagnosis Data of current patient to be displayed here. Database welcome message editor Date && Time Dec Delete Delete selected document type. Note that you can only delete document types that are not in use. Delete the episode if possible (it must be completely empty). Deleting episode Deleting health issue Description Device(%s): Displayed period must be > 0. Document type Documents: Documents: %s Double click on an event. Double click somewhere on the timeline.

Hold down Ctrl while dragging the mouse to select a period. Drag me Drug Edit Edit Document Types Edit an Era Edit staff list Edit the (first or only) item selected in the list above. Encounters: %s (%s - %s): End must be > Start Enter a comment on this external ID. Enter a comment relevant to this name (such as "before marriage"). Enter an indicator for the degree of abnormality.
Often +, -, !, ?, () or any combination thereof. Enter any additional notes and comments on this address which didn't fit anywhere else. Enter the actual ID (number, identifier, ...) here. Enter the house number for this address. Episode Episode ... Episodes: %s (most recent: %s%s%s) Error Error retrieving encounters for this episode. Error retrieving episodes for this health issue. Events Examplary filename. Mainly used for deriving a suitable file extension since that matters to some form engines. Most of the time this should already be set correctly when the template data is imported initially. Exit Exit the program Export to Image External version information such as the exact version/release/revision of a paper form onto which to print data with the help of this template. Features Feb Field '%s' can't be empty. File File '%s' does not exist. File '%s' exists. Overwrite? Filename Find First Date First select me and then drag the handles. First time using Timeline? Fit Century Fit Day Fit Decade Fit Month Fit Year Fri Getting started tutorial Give Feedback Go back one page Go forward one page Go to Date Go to home page Health Issue Health Issue %s%s%s%s   [#%s] Health issue selector Help Help contents Hold down Ctrl while scrolling the mouse wheel.

Hold down Shift while dragging with the mouse. Hover me! Hovering events with a triangle shows the event description. ID Type Icon Image files Images will be scaled to fit inside a %ix%i box. Impedance: Implanted: Import... Injection site Intro Invalid Data Issuer Jan Jul Jun Lab Lab result Last given Last worked on: %s
 Left click somewhere on the timeline and start dragging.

You can also use the mouse wheel.

You can also middle click with the mouse to center around that point. Load template data from a file. Mark this checkbox if you want this template to be active in GNUmed. May Measure Distance between two Events Measurements and Results: Measurements and Results: %s Mon Monday Move data to another episode Move event vertically Move the narrative from the source to the target episode. Moving narrative to another episode Mr Mrs Name Name: New No No [arriba] result found in [%s]. No encounters found for this health issue. No episodes recorded for the health issues selected. No narrative available for selected episodes. No timeline opened. No timeline set Not relevant Number OK Objective Occupations Oct Ok Open &Recent Open an existing timeline Open most recent timeline on startup OpenOffice Opening timeline instead of creating new. Page not found Password, again Patient: %s (%s), No: %s
 Period Plan Please select a health issue: Preferences Print Problem Procedures performed: %s Put question here. Quantity Questions and answers Read Only Redo Relevant Remember and don't ask again. Resize and move me! Run the query and present the results below. Save Save As... Saving Scroll Select Icon Select event Select events to be deleted and press the Del key. Select the episode under which to file the document ... Select the episode you want to move the narrative to. Select the health issues you are interested in ... Select the type of address here. Selecting health issue Sensing: Sep Set Category on events without category Shortcut Key: Shortcuts Show the database schema definition in your web browser. Show time Shows the episode associated with this document. Select another one or type in a new episode name to associate a different one. Someoneelse has changed the Timeline.
You have two choices!
  1. Set Timeline in Read-Only mode.
  2. Synchronize Timeline.

Do you want to Synchronize? Sort documents by the episode they belong to. Source episode Status Sticky Balloon Street Subjective Substance Suburb: Sun Sunday Target episode Tasks Template type Text Text: The currently selected patient is: The database password must be typed again to enable double-checking to protect against typos. The doctor in charge who will have to assess and sign off this result. The document type in the local language. The document type under which to store forms generated from this template. The episode the displayed narrative currently belongs to. The episode you want to move the displayed narrative to. The lab request already exists but belongs to a different patient. The lower bound of the range of technically normal values. The medical problem this test results pertains to. The password for the new database account. Input will not be shown. The result of the measurement. Numeric and alphanumeric input is allowed. The shortcut %s is already bound to function '%s'! The specified timeline already exists. The type of this template. The intended use case for this template. The units this result comes in. The upper bound of the range of technically normal values. The wxPython GUI framework hasn't been initialized yet! There are no encounters for this episode. There are no episodes for this health issue. This lists the available document types. This timeline is stored in memory and modifications to it will not be persisted between sessions.

Choose File/New/File Timeline to create a timeline that is saved on disk. Threshold Thu Timeline Timeline files Translate this or i18n into <en_EN> will not work properly ! Tue URL: Unable to copy to clipboard. Unable to open timeline '%s'. Unable to read from file '%s'. Unable to read timeline data from '%s'. Unable to save timeline data to '%s'. File left unmodified. Unable to take backup to '%s'. Unable to write configuration file. Unable to write timeline data. Unattributed episodes Undo Vaccination Vaccinations Vaccinations: Vaccine Value Verify which patient this lab request really belongs to. Vertical Zoom In Vertical Zoom Out Wed Week Week start on: Welcome Welcome to Timeline When was this result actually obtained. Usually the same or between the time for "sample taken" and "result reported". When: Where do the week numbers go if I start my weeks on Sunday? Where is the save button? Whether this document reports clinically relevant results. Note that both normal and abnormal resuslts can be relevant. Why is Timeline not available in my language? Will associated events be deleted when I delete a category? Yes You can't change time when the Event is locked You must select a category! You must select an episode to save this document under. Zip code Zoom active already reported animal any name part bilateral boy child clinically relevant close existing episode "%s" closed corrected result date of birth date of birth/death episode     : %s error retrieving unreviewed lab results external patient ID external patient source (name, gender, date of birth) female finished first name full name generic error message generic info message generic warning message girl health issue: %s inactive internal patient ID last check: left male missing, reported later modified entry name name, date of birth name, gender, date of birth new episode no known allergies not clinically relevant ongoing ordered by brand original entry partial programmer forgot to specify error message programmer forgot to specify info message programmer forgot to specify warning read-only relevant right to today (%s) tomorrow (%s) unknown allergy state unknown gender unknown reaction yesterday (%s) Project-Id-Version: gnumed
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2012-06-01 01:17+0000
Last-Translator: J.Luszawski <jl@leksoft.com.pl>
Language-Team: Polish <pl@li.org>
Language: pl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2012-06-09 20:36+0000
X-Generator: Launchpad (build 15376)
 

Proszę skonsultować log błędu by zobaczyć szczegóły 
Kategorie służą do grupowania zdarzeń. Zdarzeniu można przypisać tylko jedną kategorię. Zdarzenia z tej samej kategorii są wyświetlane z takim samym kolorem tła.
 
Polecenie
(SQL) 
Nie. Po prostu przestaną należeć do usuniętej kategorii.
 
Okienko *Utwórz wydarzenie* może być otwarte na kilka sposobów:
- Wybierz z menu *Linia czasu* - *Utwórz wydarzenie*.
- Kliknij dwukrotnie lewym przyciskiem myszy w dowolnym miejscu na linii czasu.
- Trzymając klawisz *Ctrl*, przeciągnij lewym przyciskiem myszy wybrany zakres czasu.
 
Obecnie nie wspieramy numerów tygodni przy ustawieniu niedzieli jako pierwszy dzień tygodnia. Planujemy dokonać tego w przyszłości.
 
Nie ma przycisku zapisywania. Timeline zapisuje twoje dane automatycznie, jeśli tylko zachodzi taka potrzeba.
 
Timeline jest rozwijany i tłumaczony przez ochotników. Jeśli chcesz nam pomóc, skontaktuj się z nami.
 
By usunąć wydarzenie, zaznacz je i naciśnij klawisz *Del*. Jeśli zaznaczysz kilka wydarzeń, zostaną usunięte wszystkie zaznaczone.
 
Kliknij na zdarzeniu by je wybrać. Przytrzymaj klawisz *Ctrl*, by wybrać wiele zdarzeń.
  %s aktywne leki
  %s dokumentów
  %s konsultacji od %s do %s
  %s znanych problemów
  %s Wyniki badań
  (potwierdzone ostatnio %s)  ***** POUFNE *****  - poprawiono błędy i dodano nowe funkcje
  - tylko poprawiono błędy
  - wymagana aktualizacja bazy danych
  Sporządzono podczas konsultacji: %s (%s - %s)   [#%s]  Ostatnio: %s - %s  Zauważony w wieku: %s  przyczynił się do śmierci pacjenta %s bajtów %s: spotkanie (%s) Dymki po najechaniu myszką &Zamknij &Usuń &Edycja Pomo&c &Legenda &Nawigacja &Linia czasu &Wygląd *ma* alergie ... poziom epizodu 1 bajt ? Kliniczna ocena wyniku.
Zazwyczaj dokonywana przez lekarza. Długa,opisowa nazwa tego szablonu. Dostępna jest nowa wersja GNUmed.

 Skrócona, łatwa nazwa dla tego szablonu. Komentarz techniczny do wyniku.
Zazwyczaj wprowadzany przez technika/laboranta. A: Symptom Przerwij Przerwij i NIE łącz z GNUmed-em O programie Konto Działaj na elementach wybranych z powyższej listy. Dodaj kategorię Dodaj nowy element do powyższej listy. Dodaj powyższy wpis jako nowy typ dokumentu. Dodaj Epokę Dodaj kolejne wydarzenie Dodawanie nowego epizodu Adres Hasło administratora Wiek: Alergie i uczulenia
 Alergia Podczas eksportu danych osobowych wystąpił błąd.
Szczegóły można sprawdzić w logu. kwi Czy jesteś pewny, że chcesz usunąć ten epizod?
 "%s"
 Czy jesteś pewny, że chcesz usunąć ten problem zdrowotny?

 "%s"
 Wnioski Powiąż z epizodem: Co najmniej jedno pole Kategorii musi być zaznaczone Co najmniej jedno pole Wydarzenia musi boć zaznaczone sie B: Zespół objawów Akumulator: Urodzony: %s, wiek: %s

 Musisz podać modyfikator oraz klawisz skrótu! Diagnoza objawowa Plik CSV Czy wielu użytkowników może pracować z tą samą linią czasu? Nie można się połączyć z bazą danych bez autoryzacji! Anuluj Anuluj i NIE USUWAJ epizodu Przerwij i NIE USUWAJ problemu zdrowotnego Anuluj zmiany. Anuluj przenoszenie danych opisowych i pozostaw je tam, gdzie są. Nie można dodać problemu zdrowotnego. Brak aktywnego pacjenta. Nie można się połączyć z bazą danych bez autoryzacji! Nie można utworzyć epizodu
 [%s] Nie można utworzyć nowego problemu zdrowotnego:
 [%(issue)s] Nie można usunąć epizodu. Nadal są w nim zarejestrowane dane kliniczne. Nie można połączyć dokumentu z epizodem

 [%s] Nie mogę połączyć aktywnego pacjenta z innym pacjentem Nie można uruchomić [arriba]! Kategorie Istnieje już kategoria o nazwie '%s'. Nieprawidłowa nazwa kategorii '%s'. Nazwa nie może być pusta. Kategoria: Wyśrodkuj Zmień typ wszystkich dokumentów, które obecnie mają ustawiony wybrany typ dokumentu. Zmień tłumaczenie typu dokumentu dla twojego języka. Zaznacz jeśli ten wynik jest istotny klinicznie. Zaznacz, jeśli ten wynik jest nieprawidłowy z powodów technicznych. Zaznacz to jeśli chcesz, by GNUmed pamiętał twój wybór i nie pytał ponownie. Zaznacz to żeby oznaczyć dokument przy imporcie jako zatwierdzony. Jeśli to jest zaznaczone możesz (i musisz) również określić czy jest on "technicznie nieprawidłowy" lub "klinicznie istotny". Istnieją opcje ustawiające wartości domyślne. Wyczyść Wyczyść wszystkie pola lub przywróć wartości zapisane w bazie danych. Wyczyść wszystkie pola. Kliknij na nim.

Przytrzymaj Ctrl podczas klikania, by wybrać więcej okresów. Istotny klinicznie Zamknij to okno. Zamknięto epizody starsze niż %s dni dla problemu zdrowotnego [%s]. Kolor: Założenia Połącz z GNUmed-em Trwa łączenie z bazą Kontakt Nie znaleziono strony '%s'. Kraj Stwórz wydarz&enie... Diagnoza kliniczna Tu będą wyświetlone dane bieżącego pacjenta. Edycja wiadomości powitalnej bazy danych. Data i czas gru Usuń Usuń wybrany typ dokumentu. Pamiętaj, że można usunąć tylko typy dokumentów, które nie są używane. Usuń epizod jeśli to możliwe (nie może on niczego zawierać) Usuwanie epizodu Usuwanie problemu zdrowotnego Opis Urządzenie(%s) Wyświetlony przedział czasu musi być >0 Typ dokumentu Dokumenty: Dokumenty: %s Kliknij podwójnie na zdarzeniu. Kliknij podwójnie na osi czasu.

Przytrzymaj Ctrl podczas przeciągania, by wybrać okres. Przeciągnij mnie Lek Edytuj Zmień typy dokumentów Eytuj Epokę Zmień listę personelu. Zmień pierwszy (lub jedyny) wybrany element z powyższej listy. Konsultacje: %s (%s - %s): Data końcowa nie może być wcześniejsza niż początkowa Wprowadź opis tego zewnętrznego identyfikatora. Wprowadź komentarz dla tego nazwiska (np. "przed małżeństwem") Wprowadź wskaźnik stopnia odchylenia od normy.
Najczęściej +, -, !, ?, () lub dowolna ich kombinacja. Wprowadź dodatkowe dane dla tego adresu, które nie pasują gdzie indziej. Tu wprowadź właściwy identyfikator. Wprowadź numer domu dla tego adresu. Epizod Episod ... Epizody: %s (najczęstsze: %s%s%s) Błąd Błąd podczas otrzymania wystąpienia epizodu Błąd podczas pobierania epizodów dla tego problemu zdrowotnego Wydarzenia Przykładowa nazwa pliku. Używana głównie do określenia odpowiedniego rozszerzenia, gdyż dla niektórych aplikacji obsługujących formularze ma to znaczenie. Zazwyczaj powinno to być ustawione odpowiednio podczas pierwotnego wczytania danych szablonu. Wyjdź Zakończenie programu Eksport do pliku obrazka Informacja o obcej wersji, takiej jak np. wersja/kod papierowego formularza do nadruku według tego szablonu. Cechy lut Pole '%s' nie może być puste. Plik Plik o nazwie '%s' nie istnieje. Plik o nazwie '%s' już istnieje. Czy chcesz go nadpisać? Nazwa pliku Szukaj Pierwsza data Najpierw mnie zaznacz, a potem chwyć za uchwyt i przeciągnij. Korzystasz z Timeline po raz pierwszy? Widok na wiek Widok na dzień Widok na dziesięciolecie Widok na miesiąc Widok na rok Pt Samouczek Podziel się opinią Przejdź do poprzedniej strony Przejdź do następnej strony Przejdź do daty Przejdź do strony głównej Problem zdrowotny Zagadnienie zdrowotne %s%s%s%s   [#%s] Wybieranie problemu zdrowotnego Pomoc Pomoc Przytrzymaj Ctrl podczas przewijania za pomocą rolki myszy.

Przytrzymaj Shift podczas przeciągania. Najedź na mnie! Najechanie na wydarzenia z trójkątem pokazuje opis wydarzenia. Typ identyfikatora Obrazek Pliki graficzne Obrazki zostaną zmniejszone, tak by zmieścić się w kwadracie %ix%i pikseli. Oporność Implantowany Import... Miejsce injekcji Wprowadzenie Nieprawidłowe dane Wystawca sty lip cze Laboratorium Wynik bad. lab. Ostatnio podany Ostatnio pracowano %s
 Kliknij lewym przyciskiem na oś czasu i zacznij przeciągać.

Możesz także użyć rolki myszy.

Możesz kliknąć środkowym przyciskiem, by wyśrodkować na danym punkcie. Załaduj dane szablonu z pliku. Zaznacz to pole jeśli chcesz, by ten szablon był aktywny. maj Zmierz odległość między dwoma Wydarzeniami Wyniki: Pomiary i Rezultaty: %s Pn Poniedziałek Przenieś dane do innego epizodu Przesuń zdarzenie w pionie Przenieś opis z epizodu źródłowego do docelowego. Przenoszenie opisu do innego epizodu Pan Pani Nazwa Nazwa: Nowa Nie Brak wyników dla zapytania „[arriba]” w [%s]. nie zanleziono klasyfikacji dla głównegj choroby Brak zapisanych epizodów dla wybranego problemu zdrowotnego. Brak danych opisowych dla wybranych epizodów. Nie otwarto żadnej linii czasu Nie wybrano linii czasu NIeistotny Numer OK Wynik badania Zawody paź OK Otwórz pop&rzednie linie  czasu Otwórz istniejącą linię czasu Otwórz podczas uruchamiania ostatnio używaną linię czasu. OpenOffice Otworzenie istniejącej linii czasu, zamiast tworzenia nowej. Nie znaleziono żądanej strony Ponownie wprowadź hasło Pacjent: %s (%s), nr %s
 Zakres Zalecenia Proszę wybrać problem zdrowotny: Opcje Drukuj Problem Przeprowadzone procedury: %s Tu wprowadź pytanie. Ilość Często zadawane pytania Tylko do odczytu Ponów Istotny Zapamiętaj i nie pytaj ponownie. Zmień rozmiar i przenieś mnie! Wykonaj zapytanie i przedstaw wyniki poniżej. Zapisz Zapisz jako... Zapisywanie Przewijanie Wybierz obrazek Wybierz zdarzenie Wybierz zdarzenia do usunięcia i wciśnij klawisz Del. Wybierz epizod, w ramach którego ma być zachowany dokument ... Wybierz epizod, do którego chcesz przenieść opis. Wybierz problem zdrowotny, który cię interesuje ... Tu wybierz typ adresu. Wybieranie problemu zdrowotnego Czułość wrz Ustaw Kategorię dla wydarzeń bez kategorii Klawisz skrótu: Skróty klawiszowe Pokaż strukturę bazy danych w twojej przeglądarce internetowej. Pokazuj godzinę Pokazuje epizod powiązany z tym dokumentem. Żeby powiązać z innym wybierz lub wpisz nową nazwę epizodu. Ktoś zmienił Linię czasu.
Masz dwie opcje!
  1. Ustaw Linię czasu w trybie Tylko do odczytu.
  2. Zsynchronizuj Linię czasu.

Czy chcesz zsynchronizować? Sortuj dokumenty według epizodów, do których należą. Epizod źródłowy Stan Przypięty dymek podpowiedzi Ulica Wywiad Substancje Dzielnica Nd Niedziela Epizod docelowy Zadania Typ szablonu Tekst Tekst: Aktualnie wybrany pacjent to: Hasło do bazy danych musi być wprowadzone ponownie aby zapobiec przypadkowym pomyłkom literowym. Doktor odpowiedzialny za ocenę i weryfikację wyniku. Typ dokumentu w języku lokalnym. Typ dokumentu, pod jakim zachowywane będą formularze generowane przez ten szablon. Epizod, do którego obecnie należy wyświetlany opis. Epizod, do którego chcesz przenieść wyświetlany opis. To zlecenie laboratoryjne już istnieje, ale należy do innego pacjenta. Dolna granica technicznie prawidłowych wyników. Problem zdrowotny, z którym związany jest wynik. Hasło dla nowego konta w bazie danych. Wprowadzone znaki będą ukryte. Wynik badania. Można wpisywać cyfry i litery. Skrót %s został już przypisany do funkcji '%s'! Wybrana linia czasu już istnieje. Typ tego szablonu. Przewidywane zastosowanie. Jednostki, w których podany jest wynik. Górna granica technicznie prawidłowych wyników. Nie zainicjalizowano jeszcze platformy wxPython dla interfejsu graficznego. Ten epizod nie wystąpił Brak epizodów dla tego problemu zdrowotnego To wyświetla listę dostępnych typów dokumentów. Ta Linia czasu jest przechowywana w pamięci i żadne modyfikacje nie zostaną zachowane.

Wybierz Plik/Nowy/Linia czasu by stworzyć Linię czasu zapisaną na dysku. Próg Czw Linia czasu Pliki programu Timeline Przetłumacz to, albo i18n nie będzie właściwie działało ! Wt URL: Nie można skopiować do schowka. Błąd otwarcia linii czasu '%s'. Błąd odczytu z pliku '%s'. Błąd odczytu linii czasu z '%s'. Błąd zapisu danych linii czasu do pliku '%s'. Plik pozostał bez zmian. Błąd zapisu kopii zapasowej w '%s'. Błąd zapisu do pliku konfiguracyjnego. Błąd zapisu danych linii czasu. Niesklasyfikowane epizody Cofnij Szczepienie Szczepienia Szczepienia: Szczepionka Wartość Sprawdź, do którego pacjenta to zlecenie lab. rzeczywiście należy. Przybliż w pionie Oddal w pionie Śr Tydzień Pierwszy dzień tygodnia: Witaj Witaj w Timeline Kiedy uzyskano ten wynik. Zazwyczaj w granicach między "czasem pobrania próbki" a "czasem wpisania wyniku". Data: Dlaczego nie widać numerów tygodni po przestawieniu niedzieli jako pierwszy dzień tygodnia? Gdzie jest przycisk zapisywania? Czy ten dokument opisuje wyniki istotne klinicznie. Zauważ, że zarówno prawidłowe jak i nieprawidłowe wyniki mogą być klinicznie istotne. Dlaczego Timeline nie jest dostępny w moim języku? Czy po usunięciu kategorii zostaną usunięte przypisane do niej wydarzenia? Tak Nie możesz zmienićczasu kiedy Wydarzenie jest zablokowane. Musisz wybrać kategorię! Musisz wybrać epizod, w ramach którego ten dokument ma być zapisany. Kod pocztowy Przybliżenie aktywny już zgłoszone zwierzę dowolna część imienia lub nazwiska obustronny
obustronnie chłopiec dziecko klinicznie istotne zamknij istniejący epizod "%s" zakończony poprawiony wynik data urodzenia data urodzenia/śmierci epizod : %s błąd przy pobieraniu niezatwierdzonych wyników badań zewn. identyfikator pacjenta zewnętrzne źródło danych osobowych pacjenta (nazwisko, płeć, data ur.) kobieta zakończono imię pełne nazwisko generyczna, ogólna wiadomość o błędzie ogólna informacja ogólne ostrzeżenie dziewczynka problem zdrowotny: %s nieaktywny wewn. identyfikator pacjenta ostatnio sprawdzany: lewy mężczyzna brakujący, zgłoszony później zmieniony wpis nazwisko nazwisko, data urodzenia nazwisko, płeć, data urodzenia nowy epizod Brak znanych alergii klinicznie nieistotne trwający według marki pierwotny wpis częściowy programista zapomniał sprecyzować błąd programista zapomniał sprecyzować błąd programista zapomniał sprecyzować treść ostrzeżenia tylko do odczytu istotne prawy do dzisiaj (%s) jutro (%s) Brak informacji o alergiach nieznana płeć nieznana reakcja wczoraj (%s) 