��    A     $
  �  ,      �  9   �  �   #  �   �     �  �   �  L   �  #  �  �     V   �  �   *   i   �   m    !     �!     �!     �!     �!     �!  /   �!     !"     /"     H"     ["     l"     z"     �"     �"     �"     �"     �"  9   �"  0    #     Q#     g#      �#  .   �#  '   �#     �#     $     *$     3$     P$     V$     \$     b$  '   j$  	   �$  	   �$     �$     �$     �$  	   �$  
   �$     �$     �$     �$     
%     %     %%     D%     L%     S%  
   Y%     d%     �%     �%     �%  .   �%  +   �%     	&     &     !&     *&     3&  #   ?&  -   c&  %   �&     �&  #   �&  (   �&  %   "'     H'     M'     d'     |'     �'     �'  
   �'  "   �'  0    (  	   1(     ;(  F   B(  B   �(  >   �(     )     )     )     $)     -)  
   5)     @)     Z)     k)     x)     �)     �)  .   �)     �)     �)     �)     *     *     *     *     '*  	   3*     =*      [*  
   |*     �*     �*  d   �*     +     $+  !   1+     S+     X+     i+     y+     �+  
   �+     �+  
   �+     �+     �+     �+  
   �+     �+  "   ,     7,  0   =,     n,     ,  '   �,  =   �,  1   �,     -     /-     ?-     M-     \-     o-     x-     |-     �-     �-  
   �-  *   �-     .     .     +.  
   3.     >.     B.     [.     l.  
   �.  
   �.     �.     �.     �.     �.     �.     �.  _   �.     M/  	   b/  <   l/  	   �/     �/     �/  0   �/  
   �/      0     0     #0     90     ?0     L0     P0     T0     X0  �   l0     1  0   1  F   G1  6   �1     �1     �1     �1     �1     �1     �1     �1  >   2     P2     U2  *   Y2     �2     �2     �2     �2     �2     �2     �2     �2     �2  	   3  $   3  )   63     `3     o3     v3     �3     �3     �3     �3     �3     �3     �3     �3  	   �3     �3     4     4     4     '4     34  2   @4     s4     �4     �4     �4  	   �4     �4     �4     �4     �4     �4  ?   �4     5     5     $5     )5  &   /5  �   V5     6     6     6     6     #6     (6     E6     c6  '   �6  ;   �6     �6  #   7     )7     H7     ^7  
   n7     y7     �7     �7  
   �7     �7     �7     �7     �7     �7  ;   �7     &8  -   @8  ;   n8     �8  .   �8     �8  	   �8     �8     9     9     9  -   9     ?9  M   P9     �9     �9     �9     �9     �9     �9     �9     �9     :     	:     :     +:     4:     B:     Z:     b:  *   q:  )   �:  $   �:  	   �:     �:     �:     ;     +;  -  <;  A   j<  �   �<    v=     �>  �   �>  E   �?    �?  �   �@  d   �A  �   (B  u   �B  �   7C     �C     �C     �C     D     #D  2   3D     fD     tD     �D     �D     �D     �D     �D     E     E     .E     =E  =   OE  0   �E     �E      �E     �E  .   F     BF     ]F     zF  
   �F      �F     �F     �F     �F     �F  .   �F  
   G     G     )G     8G  	   AG  
   KG     VG     bG     oG  "   |G     �G     �G  "   �G     �G  
   �G     �G     H     H     5H     NH     fH  0   jH  $   �H     �H     �H     �H     �H     �H  $    I  /   %I  '   UI  !   }I  "   �I  )   �I  %   �I     J     J  %   0J     VJ     pJ     �J  	   �J  '   �J  :   �J  
   K  	   #K  U   -K  X   �K  K   �K     (L     1L     CL     JL     SL  
   [L     fL     �L     �L     �L     �L     �L  .   �L     #M     :M     HM     LM     PM     WM     gM     mM     ~M  "   �M  "   �M  
   �M     �M     �M  h   �M     gN     {N  $   �N     �N     �N     �N     �N     �N     �N     O     "O  
   4O     ?O     ZO     wO     �O  !   �O     �O  >   �O     P  	   P  :   "P  D   ]P  8   �P     �P     �P     Q     Q     $Q  
   ;Q     FQ  "   KQ     nQ  $   �Q     �Q  ,   �Q  '   �Q     R     "R  
   )R     4R     8R     NR     hR     �R     �R     �R     �R  $   �R     �R  
   S     S  ^   !S     �S     �S  @   �S     �S     �S     �S  6   T     =T  #   LT  	   pT     zT     �T     �T     �T     �T     �T     �T  �   �T     �U  3   �U  G   �U  ;   V     LV     QV     VV     pV  	   sV     }V     �V  C   �V     �V     �V  <   �V  &   2W     YW     vW     zW     �W     �W     �W     �W  #   �W     �W  ,   �W  3   +X     _X     sX  
   {X     �X     �X     �X     �X     �X     �X     �X     �X     Y     Y     Y      Y     )Y     FY     SY  H   dY      �Y     �Y     �Y     �Y     �Y     Z     Z     Z     Z     Z  H    Z     iZ  
   pZ     {Z     �Z  %   �Z  �   �Z     n[     r[     [     �[     �[     �[  #   �[     �[  '   �[  H   #\      l\  &   �\     �\     �\     �\      ]  !   ]     2]     >]     K]     W]     Z]     a]     t]     }]  H   �]     �]  -   �]  <   )^     f^  5   j^     �^     �^     �^  
   �^     �^     �^  -   �^     _  L   #_  
   p_     {_     �_     �_     �_     �_     �_     �_     �_     �_     `     &`     7`     C`     ^`     m`  -   �`  1   �`  )   �`     
a     a      a     :a     Ua     Z   +  �      	    f   �       8    _   C   ]   �                    �   �   �   �   �   �   c       @  �   w   �   �   �   @       6      b   �           M       �   -   �   �   �   q         �   #   %   $              z   �       k   '  !       �           �   �   �   a       �   K   
      2  F       �   5   }       �           �   �                                  �       �     <                      9      �   �       �   o   l   W   %  0  X        -  3  B               �   �   [   �       �   �      �   �   �       I   �   V           �       �   s   �               �       p       <      �   �           �   �      �              "   �       �   6   �       *           O      ^   /   �   ;      �   �   :   t   �   �   �   x       Q       e        �   !      |       >   H          P           �          �           i   �   �       �   Y           �   `   �      8       �   �       ,      �   {   N          �   �          �       �       *      d       �       4  D   \   �           >  ,          �   u   �   �         ?  �   J   �              �   G   �   �   (   �         �   �   �   :      �           1      R   ;   (      $  �   &  &   �   �   y       .  m       )  �   �     E   �   �   v   7  g       �             �   "      L   �   �     �   =  7   �   �       U                	   �   r       '   3                  S   �       j        �   �          �   �   �   �   �   �   0   �       A       A          .   �   T   �   �   �   5      
   �     �   �   h   �   4      �   ~   �   2        9     #          n   =   1   +   �   ?       �       /        )           

Please consult the error log for all the gory details ! 
Categories are used to group events. An event can only belong to one category. All events that belong to the same category are displayed with the same background color.
 
Categories can be managed in the *Edit Categories* dialog (*Timeline* > *Edit Categories*). To edit an existing category, double click on it.

The visibility of categories can also be edited in the sidebar (*View* > *Sidebar*).
 
Episode %s%s%s%s: 
If you have more questions about Timeline, or if you want to get in contact with users and developers of Timeline, send an email to the user mailing list: <thetimelineproj-user@lists.sourceforge.net>. (Please use English.)
 
No. The events will still be there but they will not belong to a category.
 
The *Create Event* dialog can be opened in the following ways:

- Select *Timeline* - *Create Event* from the menu.
- Double click with the *left* mouse button on the timeline.
- Press the *Ctrl* key, thereafter hold *left* mouse button down on the timeline, drag the mouse and release it.
 
The date data object used does not support week numbers for weeks that start on Sunday at present.  We plan on using a different date object that will support this in future versions.
 
There is no save button. Timeline will automatically save your data whenever needed.
 
Timeline is developed and translated by volunteers. If you would like to contribute translations you are very much welcome to contact us.
 
To delete an event, select it and press the *Del* key. Multiple events can be deleted at the same time.
 
To select an event, click on it. To select multiple events, hold down the *Ctrl* key while clicking events.
  %s known problems
  %s of units: %s
  (last confirmed %s)  ***** CONFIDENTIAL *****  Comment: %s
  Created during encounter: %s (%s - %s)   [#%s]  Details: %s
  External reference: %s
  Health issue: %s
  Invoice ID: %s
  Invoice: %s
  Items billed: %s
  Items billed: 0
  Most recent: %s - %s  Noted at age: %s  Patient: #%s
  Receiver: #%s
  VAT would be: %(perc_vat)s%% %(equals)s %(curr)s%(vat)s
  VAT: %(perc_vat)s%% %(equals)s %(curr)s%(vat)s
  VAT: does not apply
  Value + VAT: %(curr)s%(val)s
  contributed to death of patient %d Events not duplicated due to missing dates. %s document part                 [#%s]
 %s encounter(s) (%s - %s): %s events hidden %s parts &Duplicate Selected Event... &Edit &File &Help &Legend &Measure Distance between two Events... &Navigate &Timeline *does* have allergies 1 part 1-period 10-period 100-period 1000-period A: Sign Active billable item Add Category Add Container Add more events after this one Add new Add... Alert Algorithm: Allergies and Intolerances
 Allergies/Intolerances Allergy detail: %s Apr Are you sure you want to delete category '%s'? Are you sure you want to delete this event? Aug B: Cluster of signs Backward Battery: Billed item Body Surface Area: height not found Body Surface Area: height not in m, cm, or mm Body Surface Area: height not numeric Body Surface Area: no patient Body Surface Area: weight not found Body Surface Area: weight not in kg or g Body Surface Area: weight not numeric Both C: Syndromic diagnosis Can't move locked event Can't scroll more to the left Can't scroll more to the right Can't zoom deeper than 5 Categories Category name '%s' already in use. Category name '%s' not valid. Must be non-empty. Category: Center Click on it.

Hold down Ctrl while clicking events to select multiple. Clinical data generated during encounters under this health issue: Clinical data generated during encounters within this episode: Close Closed bill Color: Concepts Contact Container: Could not find page '%s'. Create &Event... Create Event Create Timeline Create a new timeline Create event Created during encounter: %s (%s - %s)   [#%s] D: Scientific diagnosis Date && Time Day Dec Delete Delete event Description Device(%s): Direction Displayed period must be > 0. Distance between selected events Documents: Documents: %s Double click on an event. Double click somewhere on the timeline.

Hold down Ctrl while dragging the mouse to select a period. Duplicate Event Duplicate... ERROR: unknown allergy state [%s] Edit Edit &Categories Edit Categories Edit Category Edit Container Edit Event Edit categories Edit event Edit... Encounters: %s (%s - %s): End must be > Start Ends today Episode %s%s%s   [#%s] Episodes: %s (most recent: %s%s%s) Error Error retrieving episodes for this health issue. Event Properties Events Events are overlapping or distance is 0 Events belonging to '%s' will no longer belong to a category. Events belonging to '%s' will now belong to '%s'. Exit the program Export to Image Export to SVG Family History Family History: %s Features Feb Field '%s' can't be empty. File '%s' does not exist. File '%s' exists. Overwrite? First Date First select me and then drag the handles. First time using Timeline? Font Color: Forward Frequency: Fri Getting started tutorial Go back one page Go forward one page Go to Date Go to Time Go to home page Goto URL Health Issue %s%s%s%s   [#%s] Health issue Help Help contents Hold down Ctrl while scrolling the mouse wheel.

Hold down Shift while dragging with the mouse. Hospitalizations: %s Hover me! Hovering events with a triangle shows the event description. Hyperlink Icon Image files Images will be scaled to fit inside a %ix%i box. Implanted: Inactive billable item Information Intermediate results: Intro Invalid date Jan Jul Jun Last worked on: %s
 Left click somewhere on the timeline and start dragging.

You can also use the mouse wheel.

You can also middle click with the mouse to center around that point. Locked MDRD (4 vars/IDMS): creatinine value not numeric MDRD (4 vars/IDMS): formula does not apply at age [%s] (17 < age < 85) MDRD (4 vars/IDMS): unknown serum creatinine unit (%s) Mar May Measurements and Results: %s Mon Monday Month Move event vertically NOT corrected for non-average body surface (average = 1.73m²) Name New No encounters found for this health issue. No timeline opened. No timeline set Nov Number of duplicates: Occupations Oct Open &Recent Open Timeline Open an existing timeline Open bill Open most recent timeline on startup Opening timeline instead of creating new. Page not found Period Preferences Print Problem Procedures performed: %s Question Questions and answers Related pages Resize and move me! Reviewed SVG files Sat Saving Scroll Search results for '%s' Select Icon Select event Select events to be deleted and press the Del key. Select region to zoom into Selecting events Sensing: Sep Show time Source: Status Sun Sunday Synopsis THIS IS NOT A VERIFIED MEASUREMENT. DO NOT USE FOR ACTUAL CARE. Tasks Test Text Text: The specified timeline already exists. This timeline is stored in memory and modifications to it will not be persisted between sessions.

Choose File/New/File Timeline to create a timeline that is saved on disk. Thu Timeline Timeline files Tue URL: Unable to copy to clipboard. Unable to open timeline '%s'. Unable to read from file '%s'. Unable to read timeline data from '%s'. Unable to save timeline data to '%s'. File left unmodified. Unable to take backup to '%s'. Unable to write configuration file. Unable to write timeline data. Unattributed episodes Unknown format. Unreviewed Use inertial scrolling Vaccinations Vaccinations: Variables: Wed Week Week start on: Welcome Welcome to Timeline Where do the week numbers go if I start my weeks on Sunday? Where is the save button? Why is Timeline not available in my language? Will associated events be deleted when I delete a category? Year You can't change time when the Event is locked Zoom bilateral clinically relevant closed day days eGFR (Schwartz): creatinine value not numeric episode     : %s ethnicity: GNUmed does not know patient ethnicity, ignoring correction factor finished generic error message generic info message health issue: %s hour hours last check: minute minutes modified entry no known allergies no parts not available not clinically relevant ongoing original entry programmer forgot to specify error message programmer forgot to specify info message programmer forgot to specify warning read-only today unknown allergy state unknown body surface area unknown reaction Project-Id-Version: GNUmed
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 

Prosím, přečtěte si protokol chyb pro všechny podrobnosti! 
Kategorie uspořádávají události do skupin. Událost může být součástí pouze jedné kategorie. Všechny události, které patří do stejné kategorie jsou zobrazovány se stejném pozadí.
 
Kategorie mohou být spravovány v dialogovém okně *Spravovat kategorie*  (*Časová osa* > *Spravovat kategorii*). Existující kategorii lze spravovat dvojitým kliknutím na ni.

Viditelnost kategorií lze upravovat na postranním panelu. (*Zobrazit* > *Postranní panel*).
 
Případ %s%s%s%s: 
Pokud máte více otázek ohledně Timeline nebo chcete kontaktovat uživatele a vývojáře Timeline, zašlete email do e-mailové konference: <thetimelineproj-user@lists.sourceforge.net>. (Používejte, prosím, angličtinu.)
 
Ne. Události budou zachovány, pouze nebudou patřit do kategorie.
 
Dialogové okno *Vytvořit Událost* může být otevřeno několika způsoby:

- Vyberte *Timeline* - *Vytvořit Událost* z nabídky.
- dvojitý klik  *levým* tlačítkem myši na časové ose.
- Držte *Ctrl*, potom  *levým* tlačítkem myši na časové ose posuňte a pusťte.
 
Datový objekt, zpracovávající data, v současnosti nepodporuje čísla týdnů, které začínají nedělí.  V budoucnu plánujeme různé varianty datových objektů, které budou tuto možnost podporovat.
 
Není zde tlačítko uložit. Timeline automaticky ukládá vaše data kdykoliv je to zapotřebí.
 
Timeline je programována a překládána dobrovolníky. Pokud byste chtěl(-a) pomoci s překladem neváhejte nás kontaktovat, rádi vás uvítáme.
 
Pro smazání události ji vyberte a zmáčkněte klávesu *Del*. Může být smazáno více událostí současně.
 
Klikněte na událost pro její výběr. Pro výběr několika událostí zmáčkněte klávesu *Ctrl*  a označte požadované události.
  %s známých problémů
  %s jednotek: %s
  (poslední potvrzená %s)  ***** DŮVĚRNÉ *****  Poznámka: %s
  Vytvořeno během návštěvy: %s (%s - %s) [#%s]  Detaily: %s
  External reference: %s
  Zdravotní problém: %s
  Číslo faktury: %s
  Faktura: %s
  Položek účtováno: %s
  Položek účtováno : 0
  Poslední: %s - %s  Zaznamenal ve věku: %s  Pacient: #%s
  Příjemce: #%s
  DPH by melo být: %(perc_vat)s%% %(equals)s %(curr)s%(vat)s
  DPH: %(perc_vat)s%% %(equals)s %(curr)s%(vat)s
  DPH: se neuplatňuje
  Hodnota + DPH: %(curr)s%(val)s
  přispělo k smrti pacienta %d události nebyly duplikovány, chybí data. %s část dokumentu [#%s]
 %s návštěva(y) (%s - %s): %s událostí skryto %s části &Duplikovat vybranou událost... &Upravit &Soubor &Nápověda &Legenda &Měřit Vzdálenost mezi dvěma Událostmi... &Navigovat &Časová osa *má*  alergie 1 část 1-období 10-období 100-období 1000-období A: Příznak Aktívní zúčtovatelná položka Přidat kategorii Přidat kontejner Po této události přidat další Přidat nový Přidat... Upozornění Algoritmus: Alergie a nesnášenlivosti
 Alergie/nesnášenlivost Podrobnosti alergie: %s Dub Jste si jsti, že chcete smazat kategorii  '%s'? Opravdu chcete smazat tuto událost? Srp B: Soubor příznaků Zpět Baterie: Vyúčtovaná položka Tělesný Povrch: nenalezena výška Tělesný Povrch: výška není v m, cm nebo mm Tělesný povrch: výška není číslo Tělesný povrch: žadný pacient Tělesný povrch: váha nenalezena Tělesný povrch: váha není v kg nebo g Tělesný povrch: váha není číslo Obojí C: Diagnóza syndromů Nelze přesunout uzamčenou událost. Nelze rolovat více vlevo Nelze rolovat více vpravo Nelze přiblížit více než 5 Kategorie Jméno kategorie '%s' je již použito. Jmého kategorie '%s' je neplatné. Nesmí být prázdné. Kategorie: Na střed Klikněte zde.

Podržte Ctrl když klikáte na událost, aby jste zvolili  multiple. Klinické údaje získané v průběhu setkání v rámci tohoto zdravotního problému: Klinické údaje získané v průběhu setkání v rámci tohoto případu: Zavřít Uzavřený účet Barva: Koncepty Kontakt Kontejner: Nelze najít stránku '%s'. Vytvořit &Událost... Vytvořit událost Vytvořit Časovou osu Vytvořit novou časovou osu Vytvořit událost. Založeno během setkání: %s (%s - %s) [#%s] D: Vědecká diagnóza Datum && Čas Den Pro Smazat Smazat událost Popis Zařízení(%s): Směr Zobrazené období musí být > 0. Vzdálenost mezi dvěma událostmi Dokumenty: Dokumenty: %s Dvojití klik na událost. Dvojitý klik někde na časové ose.

Podržte Ctrl zatímco táhnete myší, aby jste vybrali období. Duplikovat událost Duplikovat... ERROR: neznámý stav na alergie[%s] Upravit Upravit &Kategorie Upravit kategorie Upravit kategorii Upravit kontejner Upravit událost Upravit kategorie Upravit událost. Upravit... Návštěvy: %s (%s - %s): Konec musí být > Začátek Končí dnes Případ %s%s%s   [#%s] Případy: %s (poslední: %s%s%s) Chyba Chyba při načítání případu tohoto zdravotní problému. Vlastnosti události Události Události se překrývají nebo je mezi nimi vzdálenost 0 Událost patřící do  '%s' nebude již déle patřit do kategorie. Událost patřící do  '%s' bude nyní patřit do '%s'. Ukončit program Exportovat do obrázku Export do SVG Rodinná anamnéza Rodinná anamnéza: %s Vlastnosti Úno Pole '%s' nemůže být prázdné. Soubor '%s' neexistuje. Soubor '%s' již existuje. Přepsat? Počáteční datum Nejprve mě vyber a potom táhni úchytkami. Je to poprvé co používáte Timeline? Barva písma: Vpřed Frekvence: Pá Začínáme tutoriál Jít o jednu stranu zpět Jít o jednu stranu vpřed Jít na datum Jít na Čas Přejdi na domovskou stránku Přejít na URL Zdravotní problém %s%s%s%s   [#%s] Zdravotní problém Nápověda Obsah nápovědy Podržte Ctrl zatím co  otáčíte kolečko myši.

Podržte Shift zatím co táhnete myší. Hospitalizace: %s Najeďte kurzorem! Najetím trojúhelníků na událost se ukáže popis události. Odkaz Ikona Obrázkové soubory Obrázky budou zmenšeny, aby se vyšly do boxu %ix%i. Implantované: Neaktivní zúčtovatelná položka Informace Průběžné výsledky: Úvod Neplatné datum Led Čvc Čer Naposledy zpracováno: %s
 Klikněte levým tlačítkem někde na časové ose a začněte táhnout.

Můžete také použít kolečko myši.

Můžete kliknout myší doprostřed aby jste vycentrovali tento bod. Zamčeno MDRD (4 vars/IDMS): nečíselná hodnota kreatininu MDRD (4 vars/IDMS): vzorec nelze použít na věk [%s] (17 < věk < 85) MDRD (4 vars/IDMS): neznámá jednotka séra kreatininu(%s) Bře Kvě Měření a Výsledky: %s Po Pondělí Měsíc Posuňte událost vertikálně NEupraveno na ne-průměrnou tělesnou plochu  (průměr = 1.73m²) Jméno Nová Nenalezena žádná setkání pro tento zdravotní problém. Není otevřena žádná časová osa. Není stanovena časová osa Lis Počet kopii: Zaměstnání Říj Otevřít &nedávné Otevřít Časovou osu Otevřít existující časovou osu Otevřený účet Otevřít poslední časovou osu při startu Otevření časové osy namísto vytvoření nové. Stránka nenalezena Období Předvolby Tisk Problém Provedené procedury: %s Dotaz Otázky a odpovědi Podobné stránky Změň velikost a posuň mě! Přezkoumáno SVG soubory So Ukládání Posouvat Výsledky hledání pro '%s' Vybrat ikonu Vybrat událost. Vyberte událost, která má být smazána a zmáčkněte klávesu Del . Vyberte oblast pro přibližení Výběr událostí Snímání: Zář Zobrazit čas Zdroj: Stav Ne Něděle Souhrn TOTO NENÍ OVĚŘENÉ MĚŘENÍ. NEVYUŽÍVEJTE V MOMENTÁLNÍ LÉČBĚ. Úkoly Vyzkoušet Text Text: Stanovena časová osa již existuje. Tato časová osa je uložena v paměti a její modifikování nebude přetrvávat mezi sezeními.

Zvolte soubor/Nový/Soubor Timeline k vytvoření časové osy, která bude uložena na disk. Čt Časová osa Soubory Timeline Út URL: Nelze kopírovat do schránky. Nemohu otevřít časovou osu '%s'. Nemohu číst ze souboru '%s'. Nemohu číst data časové osy z '%s'. Nemůžu uložit data časové osy do  '%s'. Soubor zůstane nezměněn. Nelze vytvořit zálohu do '%s'. Nelze vytvořit konfigurační soubor. Nelze zapsat data časové osy. Nepřiřazené případy Neznámý formát. Nepřezkoumáno Použít inerciální posouvání Očkování Očkování: Proměnné: St Týden Týden začína v: Vítejte Vítejte na Časové ose. Jak budou pokračovat čísla týdnu, pokud začnu můj týden nedělí? Kde je tlačítko uložit? Proč není Timeline dostupná v mém jazyce? Budou sdružené události vymazány pokud smažu kategorii? Rok Nemůžete změnit čas pokud je Událost uzamčená. Přiblížit bilaterální klinicky relevantní uzavřený den dny eGFR (Schwartz): creatinine value not numeric případ     : %s etnikum: GNUmed nerozlišuje pacientovu rasu, ignorující korekční faktor dokončeno obecná chybová zpráva obecná informační hláška zdravotní problém: %s hodina hodiny poslední kontrola: minuta minuty změněná položka žadné známé alergie žádné části nedostupné není klinicky relevantní pokračující původní položka programátor zapoměl zadat chybovou hlášku programátor zapoměl zadat informační hlášku programátor zapomněl zadat upozornění pouze ke čtení dnes neznámý stav na alergie neznámý tělesný povrch neznámá reakce 