��    B      ,  Y   <      �  #   �     �     �     �     
          0     E  /   _     �      �     �     �  ;   �     +     3     7     K     T     k     �     �  
   �     �  !   �     �  "   �  0     <   N     �  
   �  
   �     �     �     �  *   
	  !   5	     W	  (   p	     �	     �	  )   �	  ,   �	  	   �	     
     
  	   $
     .
     B
     I
     i
     z
     �
     �
     �
     �
     �
     �
     �
     �
                    !     7  -  H  #   v     �     �     �     �     �            /   4     d      z     �     �  ;   �                           )     @     V     n  
   z     �  !   �     �  "   �  0   �  <   #     `  
   ~  
   �     �     �     �  *   �  !   
     ,  (   E     n     r  )   {  ,   �  	   �     �     �  	   �                    >     O     k     t     �     �     �     �     �     �     �     �     �     �          -       A         >      7   6   (   5      0   1   3   /              )          =   ,              .   
                      4      %   *      '   #       :   <                     &      !      @      8                                9           +   $                     B   "      ?             	      2       ;          your time: %s - %s  (@%s = %s%s)
  %s active medications
  %s documents
  %s encounters from %s to %s
  %s known problems
  %s test results
  (last confirmed %s)  ***** CONFIDENTIAL *****  Created during encounter: %s (%s - %s)   [#%s]  Most recent: %s - %s  contributed to death of patient %s: encounter (%s) *does* have allergies 1st and (up to 3) most recent (of %s) encounters (%s - %s): A: Sign AOE B: Cluster of signs Battery: C: Syndromic diagnosis Connecting to backend D: Scientific diagnosis Device(%s): Documents: Documents: %s ERROR: unknown allergy state [%s] Encounters: %s (%s - %s): Episodes: %s (most recent: %s%s%s) Error retrieving episodes for this health issue. Error running pdflatex. Cannot turn LaTeX template into PDF. Health Issue %s%s%s%s   [#%s] Impedance: Implanted: Last worked on: %s
 Measurements and Results: Measurements and Results: %s No encounters found for this health issue. PDF output file cannot be opened. Procedures performed: %s Progress notes in most recent encounter: RFE Sensing: There are no encounters for this episode. There are no episodes for this health issue. Threshold Unattributed episodes active bilateral clinically relevant closed daylight savings time in effect episode     : %s error with placeholder [%s] finished health issue: %s inactive last check: left modified entry no known allergies not clinically relevant ongoing original entry right unknown allergy state unknown reaction Project-Id-Version: GNUmed
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
   your time: %s - %s  (@%s = %s%s)
  %s active medications
  %s documents
  %s encounters from %s to %s
  %s known problems
  %s test results
  (last confirmed %s)  ***** CONFIDENTIAL *****  Created during encounter: %s (%s - %s)   [#%s]  Most recent: %s - %s  contributed to death of patient %s: encounter (%s) *does* have allergies 1st and (up to 3) most recent (of %s) encounters (%s - %s): A: Sign AOE B: Cluster of signs Battery: C: Syndromic diagnosis Connecting to backend D: Scientific diagnosis Device(%s): Documents: Documents: %s ERROR: unknown allergy state [%s] Encounters: %s (%s - %s): Episodes: %s (most recent: %s%s%s) Error retrieving episodes for this health issue. Error running pdflatex. Cannot turn LaTeX template into PDF. Health Issue %s%s%s%s   [#%s] Impedance: Implanted: Last worked on: %s
 Measurements and Results: Measurements and Results: %s No encounters found for this health issue. PDF output file cannot be opened. Procedures performed: %s Progress notes in most recent encounter: RFE Sensing: There are no encounters for this episode. There are no episodes for this health issue. Threshold Unattributed episodes active bilateral clinically relevant closed daylight savings time in effect episode     : %s error with placeholder [%s] finished health issue: %s inactive last check: left modified entry no known allergies not clinically relevant ongoing original entry right unknown allergy state unknown reaction 