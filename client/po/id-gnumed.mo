��    ]           �      �     �     �                    !     %     )  
   .  	   9  F   C     �     �     �     �     �     �     �     �     �     �  	   �     �     	  !   	     @	     E	  
   S	     ^	     f	     m	     v	     z	     �	     �	     �	  
   �	     �	  
   �	     �	     �	     �	     �	     
     
     
     
     
     "
     )
     -
     1
     5
     ;
     @
     F
     J
     N
     R
     a
     i
     p
     t
     {
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                               %     +     2     :     I     \     _  -  u     �     �     �  	   �  
   �     �     �     �     �  	   �  H        Q     X     _     x  
        �     �     �     �  	   �     �     �     �  -   �               !     2     :     @     F     J     i     �     �  
   �     �  
   �     �     �     �     �  	   �  
   �     �     �     �                                   "     (     -     1     5     M     T     \  	   `     j     q  
   �     �     �     �     �     �     �     �     �     �     �     �     �     �     �                              "     &     ,     2     D     [  !   ^     W      (            I   5   %   .                       R   :                                 D   )             /       S       G   '           T   ]   3   P   J   $      C      4              ;   =       ?              X   L   \   K               O   Z             Q          "   !                     >             7   V   [         E   #   U   M   F   0       1   N   H   *   +   	          ,           @       A   B   9      -   <   Y   &   2   6   
   8                   *does* have allergies Add Category Add new Add... Alert Apr Aug Both Categories Category: Click on it.

Hold down Ctrl while clicking events to select multiple. Color: Concepts Connecting to backend Contact Create event Day Dec Delete Delete event Description Direction Double click on an event. Duplicate... ERROR: unknown allergy state [%s] Edit Edit Category Edit event Edit... Events Features Feb Field '%s' can't be empty. File '%s' exists. Overwrite? Font Color: Forward Frequency: Fri Go to Date Help Help contents Icon Image files Information Intro Jan Jul Jun Locked Mar May Mon Month Name Name: New Nov Oct Page not found Parent: Period Sat Saving Scroll Search results for '%s' Select Icon Select event Sep Sun Tasks Text Text: Thu Timeline Tue URL: Wed Week Welcome Where is the save button? Year Zoom day days hour hours minute minutes modified entry no known allergies to unknown allergy state Project-Id-Version: GNUmed
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 memiliki alergi Tambah Kategori Tambah baru Tambah... Peringatan Apr Agu Keduanya Kategori Kategori: Klik pada ini.

Hold down Ctrl while clicking events to select multiple. Warna: Konsep Menghubungkan ke backend Kontak Buat acara Hari Des Hapus Hapus acara Deskripsi Arah Klik ganda pada sebuah acara Duplikat... ERROR: pernyataan alergi tidak diketahui [%s] Edit Sunting Kategori Sunting Kegiatan Edit... Acara Fitur Feb Kolom '%s' tidak boleh kosong. Berkas '%s' sudah ada. Timpa? Warna Huruf: Maju Frekuensi: Jum Ke Tanggal Bantuan Daftar isi bantuan Ikon Berkas gambar Informasi Perkenalan Jan Jul Jun Terkunci Mar Mei Sen Bulan Nama Nama: Baru Nop Okt Halaman tidak ditemukan Induk: Periode Sab Menyimpan Gulung Hasis pencarian untuk '%s' Pilih Ikon Pilih acara Sep Min Tugas Teks Teks: Kam Garis Waktu Sel URL: Rab Minggu Selamat Datang Dimana tombol simpan? Tahun Perbesar hari hari jam jam menit menit perubahan catatan alergi tidak diketahui ke pernyataan alergi tidak diketahui 