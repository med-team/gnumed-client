��    �     4&  [  LL      �e  9   �e  B   �e  x   6f     �f  �   �f  .   �g  �   �g  q   �h  �   @i     �i     �i  
   �i  #   �i     j     *j     9j     Wj     kj     }j     �j     �j     �j     �j  !   �j     k  /   $k     Tk     jk     }k     �k     �k      �k     �k  +   �k     l     9l     Wl     ul     �l     �l     �l     �l     �l  D   �l  	   *m     4m  	   Bm     Lm  
   Qm  
   \m     gm     pm     xm     m  	   �m     �m     �m  	   �m     �m     �m     �m     �m     �m  
   �m     �m  
   �m     �m     n     n     n     n     &n     .n  "   2n  	   Un  	   _n     in     {n     �n     �n     �n     �n     �n     �n     �n     �n  	   �n     �n     o     o     o     0o     Ao     Wo     co     qo  &   uo     �o     �o     �o     �o  
   �o     �o     �o     �o  &   
p     1p  ;   8p     tp     �p     �p     �p     �p     �p     �p     q     #q     :q     Iq     bq     yq     �q     �q     �q     �q  9  �q  >   s     Ls     ls  0   �s  '   �s  `   �s  '   @t  W   ht  	   �t     �t  	   �t     �t  	   �t     �t  #   �t  7   u     Lu     Yu     hu  �   pu  ,   �u     'v  .   0v  5   _v  9   �v     �v     �v  K   �v  !   2w     Tw  $   pw  X   �w  '   �w     x     0x     Lx  !   _x     �x     �x     �x     �x  ;   �x     y     y     %y     4y     :y     >y     Cy     Iy     Qy  !   ay     �y     �y  3   �y  ;   �y  X   z  y   jz     �z  $   |  @   *|  C   k|  (   �|  S   �|     ,}  �   8}  .   �}  E   	~  6   O~  ;   �~  �   �~  
   m     x     �  
   ��     ��     ��     ��     ��     с     ׁ  *   ��  -   �     9�     K�     P�     g�     l�     s�     ��     ��     ��  4   ��     ��     ��  <   �  %   B�  *   h�  '   ��  ,   ��  B   �     +�  2   D�     w�     ��  +   ��     ˄  +   �  Y   �     o�  Q   ��  I   ܅  5   &�  1   \�  ,   ��  0   ��  D   �  I   1�  (   {�  %   ��     ʇ  $   ܇  '   �  1   )�     [�  &   v�  +   ��  )   Ɉ  X   �  -   L�  +   z�  X   ��  +   ��  ,   +�  W   X�  p   ��  Z   !�  +   |�  .   ��  '   ׋     ��     �  	   �     �  M   +�  E   y�     ��  ,   ǌ     �  D   �  T   K�  /   ��  ,   Ѝ  T   ��  ,   R�  -   �  #   ��     ю  I   ݎ  Z   '�  ^   ��  N   �  +   0�  2   \�  �   ��  2   L�  A   �  N   ��     �     ,�  )   2�  )   \�     ��     ��  $   ��     ڒ     ��     �  -   �     5�  1   G�  =   y�     ��     ͓     ܓ     �     ��  	   �  ,   �  %   :�     `�     z�     ��  7   ��  	   ̔     ֔     ޔ     �  	   ��  "   �     &�     3�  /   A�  =   q�  /   ��  E   ߕ     %�  3   E�  .   y�     ��  	   Ɩ     Ж     �     ��     �  '   	�  
   1�  #  <�     `�  +   h�     ��     ��  !   Ș     �     �     �     �  ?   *�  
   j�     u�     ��  -   ��     ș     ՙ     �     �  2   �  5   G�     }�  
   ��     ��     ��     ��     ��      ˚     �     �     ��  `   �  :   q�  =   ��  B   �     -�     L�     l�     ~�     ��     ��          ڜ     �  
   ��     	�     �     !�  .   )�     X�     d�  
   ��     ��  3   ��     ȝ  2   ؝  !   �  2   -�  ,   `�     ��  @   ��     ۞     ߞ     �  �   	�     ��  
   ��     ��     ��     ��     ��     ş     ҟ     �     �     ��     ��     �  %   �     5�     C�     H�     a�  	   u�     �  &   ��     ��  0   ̠     ��     �  9    �  )   Z�     ��     ��     ��     á     �     ��     �     .�     D�     L�     T�     Z�     h�     x�  ,   ��  	   ��     Ƣ     Ԣ     �     ��     �     �     �     )�  U   F�  |   ��  $   �  ]   >�  B   ��  �   ߤ  8   r�  b   ��  ^   �  W   m�  +   Ŧ  3   �  A   %�  w   g�  U   ߧ  (   5�    ^�  �   v�  j   �     }�     ��  "   ��     ��     ��  &   ��  )   �  *   �  -   =�  0   k�  (   ��     ū  <   �  $   "�     G�  6   b�     ��  �   ��     v�     {�     ��     ��     ��  �   ­     S�     [�     _�  !   d�     ��     ��  
   ��     ��  (   ��  K   �     1�  
   =�  
   H�     S�     a�     n�     w�     {�     ��     ��     ��     ��     ʯ     �     ��     ��  $   �     -�  "   6�  (   Y�  !   ��  !   ��  	   ư     а     ְ     ߰     ��     ��     �     $�  F   1�     x�     ��     ��  	   ��     ��     ��  Y   ݱ  �   7�     �     ��      �  �   	�  K   ų     �  
   -�  
   8�  	   C�  �   M�  	   )�     3�     B�     Q�     ^�     q�     ~�     ��  ,   ��     µ  ;   ӵ     �  �   �     �     �     9�     M�     Z�     b�     d�  
   h�     s�     |�  	   ��     ��  
   ��  	   ��     ��     ��     ѷ  
   ڷ     �     ��     �     �     .�  %   <�  %   b�    ��  2   ��  .   ڹ     	�     �     ;�  
   J�     U�     ^�  	   e�     o�     v�     ~�     ��  :   ��  #   ܺ      �  D   �  	   d�     n�  !   �  L   ��     �     �     �     9�  
   M�     X�     h�     m�     r�     ��     ��     ��  B   ��      �  !   �     3�     E�     [�  $   b�     ��     ��  '   ��     ѽ     �  9   ��  #   /�     S�     V�     Z�     p�     ��     ��     ��     ��  	   ��     þ     Ǿ  	   �     �     �  !   �  *   )�  4   T�     ��     ��     ��     ҿ  
   �     ��  .   �  1   D�     v�     }�  Y   ��     ��     ��     ��     ��     �  	   �  
   �  
    �     +�     2�     5�  x   E�     ��  
   ��     ��  	   ��  3   ��  +   0�  y   \�     ��     ��     ��  
   ��  !   ��     !�     )�     1�     >�     R�     [�     k�     s�     {�     ��     ��     ��     ��     ��  	   ��     ��     ��  A   ��  _   �      z�     ��     ��  +   ��  "   �  
   '�     2�     >�     M�     \�     j�     ~�     ��     ��     ��     ��  >   ��     �  #   �     >�     L�  (   h�  !   ��     ��     ��     ��     ��     ��     �     �     �     �     �     *�     F�     U�     f�     z�  	   ��     ��  	   ��     ��     ��     ��  #   ��     ��  X   �  *   f�     ��     ��     ��     ��     ��     ��     ��     �     �      �  $   )�     N�  {   ^�  �   ��  Y   s�  e   ��  B   3�  $   v�      ��     ��  '   ��     ��     ��     ��     �     �  ,   �     K�     P�     h�  (   {�  #   ��  .   ��     ��     �  y   (�  -   ��  +   ��     ��     �     4�     F�     L�     j�     ~�     ��     ��     ��  
   ��  L   ��  2   ��      (�     I�  0   P�     ��  -   ��  /   ��     ��  3   �     :�  .   R�  5   ��  -   ��  2   ��  0   �      I�     j�  *   ��     ��  e   ��  /   5�  2   e�  -   ��  *   ��  a   ��  M   S�  J   ��  e   ��  u   R�  `   ��  c   )�  �   ��  -   �  E   M�     ��     ��     ��     ��  	   ��     ��     ��     ��     ��     �     �     ,�  <   <�     y�  8   ��  /   ��  ,   ��     �  8   %�     ^�     ��  F   ��     *�     6�     O�  
   T�     _�  �  m�     �  -   �     C�  %   a�  )   ��     ��  	   ��     ��  S   ��  7   $�  p   \�  7   ��  	   �     �  #   #�     G�  /   O�     �     ��     ��     ��  
   ��  	   ��     ��     ��     ��     ��     ��     ��     ��     ��     ��     �     �     %�     3�     8�     L�     Y�     ^�  -   l�  @   ��  "   ��  /  ��  �   .�  ]   ��  �   Y�  @   �  a  P�  F   ��  (   ��  J   "�  &   m�  ;   ��  9   ��  8   
�    C�  �  ��  B   X�  :   ��  B   ��  2   �  v   L�  \   ��  C    �     d�  I   v�  q   ��  �   2�  �   ��  3   ��  C   �     U�  :   u�  7   ��  >   ��  )   '�  ,   Q�  9   ~�  3   ��  O   ��  A   <�  K  ~�  =   ��  %   �  (   .�  c   W�  *   ��  �   ��  �   ��  �   =�  	   ��  *   ��     �     �     �  H   �  #   d�  M   ��  <   ��     �     �     #�     (�     7�  ,   L�     y�  A   ��  1   ��     �  ]    �  Z   ~�     ��  c   ��     [�     v�     |�     ��  {   ��  G   	�     Q�     g�     s�     y�     �     ��  <   ��  g   ��     9�     M�     T�     g�     y�     ��     ��     ��     ��     ��     ��  O   ��     �  
   �     '�     /�  %   5�  8   [�     ��     ��     ��  %   ��     ��     �  )   �  -   ?�     m�  &   z�     ��     ��  @   ��  �   ��  
   ��  p   ��    2�  �   A�    %�  8   .  *   g  v   �  9   	 w   C 0   � -   �     '   & 
   N 	   Y    c    u 2   y    � $   �    � 3   � K   + .   w I   � t  � �   e    b    n    r    {    �    � 	   �    �    �    �    � 	   �    �    �    �    �            5    U    \    i    z    �    �    �    �    �    �    � 
    *        K '   \    �    � 5   �    �    �    �    � 
   	 	   	    	    1	    F	    ^	 +   c	    �	    �	 -   �	    �	    �	 
    
    
    
    +
    >
    G
 %   f
    �
    �
    �
    �
    �
     
   3 \   >    �    �    �    � 	   �    � 	   �    �                ,    H    W    m    �    �    �    �    �    �    �    �         .     A    b %   y *   �    � $   �    �        " 
   :    E    Q    Y    n    q    �    �    �    � <   �    � !    *   % )   P +   z $   �    �    �    �    �    �    	         ? 
   T    _    k    w    |    � 
   � 
   �    �    �    �    �    �    � '       7 '   P    x    ~    � 	   � 
   �    �    �    � g  � D   P X   � �   �    � /  � 6   � &  � e    �   �        &    4 %   @    f    ~    �    �    �    �    �    � 4    #   G 8   k 3   � 0   �    	          4    U    h $   �    � +   �    �     
    +    I    ]    r    �    �    � Q   �            '    /    5 	   I    S 	   \    f    n 
   v    � !   �    �    � 
   �    �    �    � 	   �     	   	            %    -    3    ?    M /   R 
   � 
   �    �    � 
   �    �    �    �    �    �        , 	   9    C    Q    a    h    �    �    �    �    � &   � %   �        *    , 
   /    :    H    W 1   o    � ;   �    �    �      "        B     `     y     �     �     �     �     �     !    -!    G!    a!    o! 7  |! A   �" )   �"     # &   ?# /   f# b   �# 3   �# ^   -$    �$ 	   �$    �$    �$ 
   �$    �$ "   �$ 2   �$    (%    5%    J% �   P% +   �%    & &   & 6   ?& 3   v&    �&    �& Z   �& %   '    C' ,   a' T   �' 9   �' #   ( ,   A(    n( )   �( !   �(    �(    �( 	   ) *   )    <)    N)    e)    t)    {)    �)    �)    �)    �) "   �)    �)    �) 2   �) <   '* c   d* �   �* =  ^+ #   �, H   �, X   	- 6   b- c   �-    �- �   	. +   �. 9   �. 6   �. ?   1/ �   q/    #0    /0 =  F0    �2    �2    �2    �2 !   �2    �2    �2 <   �2 7   '3    _3    x3    �3    �3    �3    �3    �3    �3    �3 C   �3    C4    L4 G   ]4 #   �4 ,   �4 ,   �4 2   #5 6   V5    �5 =   �5    �5    �5 H   6 /   `6 I   �6 p   �6    K7 Y   f7 P   �7 C   8 1   U8 >   �8 ?   �8 V   9 _   ]9 1   �9 L   �9    <: ?   \: 1   �: B   �: *   ; 8   <; =   u; +   �; z   �; 8   Z< =   �< q   �< @   C= D   �= n   �=    8> s   �> I   ,? E   v? :   �?    �?    @ 
   @    @ H   4@ B   }@    �@ 0   �@    �@ >   A M   PA %   �A C   �A �   B /   �B -   �B #   �B    C O   (C V   xC Y   �C c   )D +   �D (   �D �   �D >   �E J   �E E   .F &   tF    �F /   �F /   �F     G    %G /   =G 5   mG    �G    �G I   �G    H ,   H F   FH    �H    �H    �H    �H    �H    �H 6   �H (   .I    WI    sI    �I B   �I    �I    �I    �I    �I 
   J #   J    5J    BJ %   QJ Q   wJ 8   �J P   K $   SK O   xK =   �K '   L    .L    :L    NL    `L    iL *   rL 
   �L .  �L    �M /   �M    N &   &N $   MN    rN    yN    �N    �N Q   �N    �N    O    'O 7   :O    rO '   �O 4   �O    �O %   �O )   P    7P 	   IP    SP    hP 	   }P    �P     �P    �P    �P    �P c   �P /   ?Q :   oQ M   �Q &   �Q '   R    GR %   ZR    �R    �R    �R    �R    �R    �R    �R    �R    S 4   S    JS "   [S    ~S 	   �S .   �S    �S =   �S (   'T 4   PT 5   �T    �T 8   �T    U    U !   %U �   GU    �U    �U    �U 
   
V    V    V    &V    6V    RV    ^V    eV    iV    yV &   �V '   �V    �V    �V     W    #W    /W /   ?W     oW 3   �W #   �W #   �W <   X &   IX    pX    �X    �X     �X    �X    �X    Y    (Y    EY    LY    RY    XY    lY    ~Y 6   �Y    �Y    �Y    �Y    �Y 	   Z    Z 	   Z    )Z $   @Z _   eZ �   �Z '   N[ h   v[ F   �[ �   &\ 0   �\ Z   ] a   `] l   �] *   /^ 0   Z^ C   �^ ~   �^ Z   N_ '   �_ ?  �_ �   a h   �a 	   'b    1b %   ?b    eb    mb 0   rb %   �b .   �b -   �b 1   &c :   Xc &   �c K   �c 0   d    7d :   Td 	   �d    �d    �e (   �e    �e    �e    f �   'f    �f    �f    �f !   �f (   �f    g    g    g ,   &g Y   Sg    �g    �g    �g    �g    �g    �g    �g    �g    �g    �g    	h "   "h ,   Eh    rh    ~h    �h $   �h 
   �h !   �h 8   �h    i    :i 
   Yi    di    ji    ri    i    �i #   �i    �i [   �i    *j    Hj    Tj    Zj    ij    �j j   �j �   k 
   �k 
   �k 
   �k �   �k P   �l    �l    m    m    )m �   6m 	   n    n    -n    Bn    On    cn    pn    yn     �n    �n C   �n    o   o    p     +p    Lp    cp    xp    p    �p    �p    �p 	   �p    �p    �p    �p 	   �p 
   �p    �p 	   q    q    q    5q    Iq    Zq    sq 3   �q -   �q    �q 0   	s -   :s    hs $   ~s    �s 
   �s    �s    �s    �s    �s    �s    �s     t <   t +   Zt &   �t >   �t 	   �t    �t !   u O   *u    zu    �u    �u    �u    �u    �u    �u    v    	v    #v    +v     =v C   ^v    �v "   �v    �v    �v 
   w $   w    1w     7w 3   Xw    �w    �w I   �w &   x    4x    7x    ;x    Wx    ox !   �x    �x    �x 	   �x    �x %   �x    �x    �x &   �x 1    y +   Ry 9   ~y    �y "   �y ,   �y    %z    Ez '   Tz 2   |z ,   �z    �z    �z q   �z    d{    j{    r{    u{ 	   �{ 	   �{ 
   �{    �{    �{    �{    �{ �   �{ (   �| 
   �|    �| 
   �| 7   �| 2   } }   A}    �}    �}    �} 	   �} 9   �}    "~    +~    3~    H~    \~    b~    q~    �~    �~    �~    �~    �~    �~    �~ 
   �~    �~    �~ C   �~ Y   @ )   � !   � *   � %   � $   7�    \�    h�    l�    t�    ��    ��    ��    ��    ǀ    ߀    � D   �    J� #   _�    ��    �� -   �� ,   ށ    �    �    *� 
   A�    L�    e�    g�    z� 	   ��    ��    ��        ق    �    ��    �    �    5�    B� 	   I� 	   S�     ]� $   ~� l   �� -   �    >� "   R� 
   u�    ��    ��    ��    Ƅ    ք    ݄    � $   ��    � {   2� �   �� b   L� }   �� J   -�     x�    ��    �� .    
   �    ��    �    �    -� 4   6�    k� &   r�    �� B   �� &   �� =    �    ^�    v� �   �� 0   � "   N�    q� #   ��    ��    Ȋ &   ϊ    ��    �    �    �    '� 
   6� c   A� J   �� /   ��     � 0   )� 	   Z� .   d� 4   �� +   Ȍ 6   �    +�    E� @   _� 7   �� @   ؍ *   � #   D�    h� ,   ��    �� x   Ҏ 5   K� 9   �� /   �� )   � J   � J   `� B   �� c   � x   R� U   ˑ Y   !� �   {� -   (� D   V�    ��    ��    ˓    ѓ    � 
   ��    � 	   �    �    +�    ?�    S� D   n�    �� 8    7   �� 2   3�    f� 4   }� �   ��    8� X   @�    �� "   ��    ɖ    ϖ    ޖ �  �    �� 5   ��    �� 6   � :   M�    ��    ��    �� c   �� 5   � o   H� >   ��    ��    � .   �    I� -   R�    ��    ��    ��    �� 
   ��    ��        Λ    ՛    ݛ    �    �    ��    �    �    #�    8�    ?�    N�    T�    l�    ��    �� 6   �� H   Ҝ )   � _  E� �   �� o   [� �   ˟ E   m� �  �� 7   7� )   o� O   �� .   � I   � ;   b� 8   �� �  ף �  z� =   8� ,   v� 6   �� .   ڧ q   	� h   {� L   �    1� H   D� v   �� 	  �   � 1   '� +   Y� '   �� ,   �� 8   ڬ 9   � )   M� 3   w� 6   �� (   � a   � :   m� O  �� ?   �� +   8� ,   d� k   �� '   �� �   %� �   ѱ �   s�    � -   �    J�    P�    X� F   ]�     �� ;   ų P   �    R�    Y�    `�    e�    w�    ��    ��    ̴ !   �    
� _   &�    ��     �� g   ǵ    /�    N�    T�    X� �   e� f   �    V�    l�    t�    }�    ��    �� >   �� x   � 	   c�    m�    u�    ��    ��    ø    ϸ    ۸    �    ��    � ^   �    k� 	   t�    ~�    �� 1   �� A   �� -   �� '   -�    U� *   ]� 
   ��    �� *   �� 5   Ѻ    � .   �    F�    V� @   n� �   ��    �� �   ��   '� �   >� )  #� [   M� B   �� m   �� ;   Z� �   �� +   ,� 5   X�    �� )   ��    �� 	   ��    ��    �� /   ��    +�    7�     T� 4   u� P   �� :   �� N   6� y  ��   ��    �    +�    :�    >� !   C�    e�    l�    ~�    ��    ��    �� 	   ��    ��    ��    ��    ��    �� #    � $   $� 	   I�    S�    `�    t�    ��    ��    ��    �� 
   ��    ��    ��    � 7   2�    j� 7   x�    ��    �� 1   ��    �    �    !� 	   '�    1�    ?�    M�    h�    ��    �� /   ��    ��    �� -   ��    ,�    C�    S�    \�    j�    ��    �� .   �� /   ��    	�     �    =�    \�    z�    ��    �� a   ��    &�    =�    M� 	   V�    `�    n�    ��    ��    �� 	   ��    ��    ��    ��    � !   )�    K�    P�    e�    ~�    ��    ��    ��    �� '   ��    � '   ;� 0   c� %   �� 7   ��    �� 4   �    6�    P�    p� 
   ��    ��    ��    ��    ��    ��    ��    ��    �    � B   � 
   ^�     i�    ��    �� 9   ��    �� 	   � (   �    A�    S�    [�    r� %   ��    ��    ��    ��    ��    ��    ��    �    � 	   ,�    6�    C�    H�    Q�    p�    �� >   �� !   �� 4   ��    .�    3�    H�    U�    c�    t�    �� 
   ��    }  �   >  !      �   P  E   I  K  �  �   `  m  w  d   �  �  m       �   �      k  �       `  =   �      d  {   �      1     �       q  �  �  �  e   $  �      N  �             �                  �                  �  D          �    �  �      a          �  �  �   $        �          �      �           x  �  �     �   )  I  n  �      L  �   �       q      �          �  f      !          �      /       +  �  �  x  �  �  �  �  _          �   �   �   �  �       �  �  V    �  z        t   A    Y          �  (  �    &  d  2   �  �      �  l               �  H  )  �      �  e  �    �              �      �   y   c      %          D    �  �      w         G      �  6  #   �  -           a       �  "  �  �        �      �   -  2      �  �   V   S  m  Z    s  �        �  E  �  H      ~  �          �      ]  g  �   &  c       1  e      �  �  I          \  (  '      V  �  !  R      \  v    �      �   1  @  �  �    
  3  �  =    �  �     �  �      �  /   a  �   �       �  �      �      �          �  r  �   �  K  �   ,  C  �       �                �  m  �   �   �          @             �   �  �  i  r   �  {          �             �  W      �          �  T  �   Z      �  T  �        t  �  �  "  '  �       �      k  �      C  �  �   �  �  u   e  ,  b  �  V  g        U  /  
  q      �          Y  �          n  �      �  R  �  )  �         ^      �      �  &  7      �  ?          �  �                �  y  �      /    �  -  Y  7    .  �         �  �    P  `  ]          *  �   o  �        #  �         �  8      )  �  �  �         #  N  �  r    �      �  �  '  �          �  �  �  �          �             W  L           X           �   �  !  �  �  "  �  �  �      X  S   �  w  �  �    @  �      _       �   F      �  %  8   �  �  g      n   �  �  �  �  �   �  3  ^    9      ?   �  ;          �  �       6  �            z  �   �   �          �       w  �  �         }  �  i  ?  �      �   �  �  ]      �   W  �      �  :  N       �  2  �  �   �  {  �        ?  D            �      �           �          ,   |  \  f    �          �  �     �  k  �  �  .  �      �  �    �  _          �   F  h      �  �  �  �   ;      �    �   �    R  �       T    >       A      �  B          �    �     Y  �   �  [         �           `            �  �   �           �          �      �  �   �  �  �   j              �  b     f      �  �       �  �  �  $           �  u        �  ~  X      �  �       �  5              �  h  �          $  �  +     �  �  �   �   �  G          }  7      2    �   O  �  �  L  v      �  v  �      �      �  @  f   ~  �   n        �  �   P  	      �      2  �  �  j   Z  Q  W  �     �  a  Z     �      �      �  �    �   �    �  J  �       s  �   �  �  �  Q  ^  Q      �      �  �       �      �  �   �  ~  L      �    d      0       �   *  5  $      �  �  '       {  3      �  �  �  �  {  �  +      o   �          �   P  �  �    �  �  F   �              o  �  �   \   �    �  �  �  �  �  �  �   �  �      j  �      M      e  -      �    	  �  y  F  �  �  �   �  �  �  �               �      �  �  �      �      �   �  t    <          �       �       �          �  >  <          T       z     <  �   �   g      �  �  p    �       1  �  
          �      =  �       �   �    �                   Y   �       �  t  �    �  �  @    �        �   #      �  o      �  �  �  J  �   �      �  O  9  C   
              %  �  l  �  �  �  :   *  r  4  �           �  �    �  �  9   +  �  �  �  }  �   [      �   X             y      �      �  �                    i   y  �      o  �   �  �  J   �  �  j  �  �  �   ]  k  �    �  G  �  �      H      -      p   �  a  �   �      �  �      �   �      �  �     /  �          5     :  B  l  <  q   �  4  U      �      �  G           �   �  �  �   �  �   �   �    |  �  �  �  q  B      U  �  Q    �       �   �  �      �    <   �      �   :  �  �  I   �      M  �  �      �          �  A      �          v    �  R  (       �          |  �      �  �   b  �      �  �              x   7   �        d  ;  �  ;           D    �            u  M       O  |   Z  *       (   U  w    6  l     �  h  �  L  �  .  O  "   u  E  �  3        &   �   �  �  �  �    �     T  j      9      [  Q   �  �  8      �  G      E  m  �      �      ]   �          �  7  ;  �      4  5  �  �   8      �  H   �      �  =            �  u  h  I  �  �  �              ,  �                             �  �   A      �      �  �   R   �  �  8      1      O   &      �  �  c  z  �  �  �  �           �      �  �  �       ^  �      5  r  �   �      �  �     �  �   b  4     �    �      �  '  *        �  S                            �  0  �   �  �  �  �  .      #        %  }     �        i  4      �   M  x  [  �   )       _    %  �  �               f     �      �  k   �   �  �          �      J  U   	  x  �      i      ,      �    �       =  �  9  (      M  D   �  s      S    |      �       X      �         �   ^   �  ?          �   H      �  N    �  :  �      �  �   J      c  N  �  `   0      p  �      z          �  s  �  p  
   l  "  B  �  �      �  ~   >  �   �      \          �  �  �  v   B   �  �   s     _  �  �          �  S     �  �               �  �   t        b     [  �  �    �  C      �     0  K  >                 	  F      c  p  .   P               �          	   K       n  !       6      C  �   V      �      h   �           +  �    0      �                          �  3       A           W   6       �      K          g       �  E  �          �   �   

Please consult the error log for all the gory details ! 
 In the list below mark the health issues you want to report on.
 
 These are the episodes known for the health issues just selected.

 Now, mark the the episodes you want to report on.
 
Command
(SQL) 
GNUmed database version mismatch.

This database version cannot be used with this client:

 client version: %s
 database version detected: %s
 database version needed: %s

Currently connected to database:

 host: %s
 database: %s
 user: %s
 
Select the encounter type you want to edit !
 
The client will, however, continue to start up because
you are running a development/test version of GNUmed.

There may be schema related errors. Please report and/or
fix them. Do not rely on this database to work properly
in all cases ! 
There is a serious problem with the database settings:

%s

You may have to contact your administrator for help. 
You must connect to a different database in order
to use the GNUmed client. You may have to contact
your administrator for help.      REFERRALS         Alerts      risk: %s   your time: %s - %s  (@%s = %s%s)
  %s active medications
  %s documents
  %s encounters from %s to %s
  %s known problems
  %s test results
  (L)   (last confirmed %s)  ***** CONFIDENTIAL *****  - bug fixes and new features
  - bug fixes only
  - database fixups may be needed
  - database upgrade required
  Created during encounter: %s (%s - %s)   [#%s]  Most recent: %s - %s  New version: "%s"  Noted at age: %s  Postal Address   Your current version: "%s"
  contributed to death of patient  option [%s]: %s %(number)s %(street)s, %(urb)s %(postcode)s %d. of %s (last month) - a %s %d. of %s (next month) - a %s %d. of %s (this month) - a %s %s (%s last year) %s (%s next year) %s (%s this year) %s Bytes %s: encounter (%s) %sAllergy: %s, %s (noted %s)
 %sEpisode "%s" [%s]
%sEncounters: %s (%s - %s)
%sLast worked on: %s
 &Abnormal &Actions ...  &Activate &All &All users &Allergies &Allergy &Cancel &Clear &Close &Contacts &Correspondence &Debug mode &Definite &Delete &Discard &Drug Resources &EMR &Edit &Encounter &Help &Knowledge &Me only &New &Normal &Note &Occupation &Office &Ok &Past history (health issue / PMH) &Reassign &Relevant &Report Generator &Reset &Restore &Scan page(s) &Select &Sensitivity &Summary &Take responsibility &Tolerates others in class &Tools &Urgency: &Vaccinations &Write letter &Zone: &clinically relevant &review and sign &technically abnormal &xDT viewer (%s patients) (R) (you are/are not the primary reviewer) ** DOB unknown ** *does* have allergies + -- ... Reason ... encounter level ... episode level ... issue level ... test results listing goes here ... 1 Byte 1st and (up to 3) most recent (of %s) encounters (%s - %s): 63 - Normal - 79 63< Normal >79 <No embed string for [%s]> <allergy state unasked> <channel> cannot be empty <current allergy state> <issuer> cannot be empty <last confirmed> <name> cannot be empty <not supplied> <number> cannot be empty <restricted procedure> <street> cannot be empty <urb> cannot be empty <zip> cannot be empty ?ongoing ?short-term A GNUmed slave client has been started because
no running client could be found. You will now
have to enter your user name and password into
the login dialog as usual.

Switch to the GNUmed login dialog now and proceed
with the login procedure. Once GNUmed has started
up successfully switch back to this window.
 A clinical assessment of the measurement.
Usually by a doctor. A comment on the allergy state. A comment on this review. A long, descriptive name for this form template. A new version of GNUmed is available.

 A short alias identifying the GNUmed user. It is used in the clinical record among other places. A short, catchy name for this template. A technical comment on the result.
Usually by the entering Medical Technical Assistant. A&bnormal A: Sign ALLERGIES AOE ATC/Class Abort Abort and do NOT connect to GNUmed. Abort the dialog and do not change the current setting. About GNUmed About database Account Acquire a page from an image source (scanner, camera). This may bring up an intermediate dialog. It uses Sane (Linux) or TWAIN (Windows). Act on the items selected in the above list. Activate Activate patient and remove from waiting list. Activate patient but do not remove from waiting list. Activate the respective patient by double-clicking a row. Active Active Problems Add a file from the filesystem as a new part. Shows a file selector dialog. Add a new item to the list above. Add a new item to the list. Add a new user to the GNUmed system. Add a past/previous medical history item (health issue) to the EMR of the active patient Add above input as a new document type. Adding a new health issue Adding document description Adding new address Adding new communications channel Adding new encounter type Adding new external ID Adding new name Address Address details must be filled in completely or not at all. Adjusted Values Admin password Administrative Adult Age Age: Alias Allergy Allergy Manager Allergy and Sensitivity - Summary Allergy details Allergy state An alphanumeric range of technically normal values. An alphanumeric target range for this test in this patient. An error occurred while Demographic record export
Please check the log file for details. An error occurred while retrieving a text
dump of the EMR for the active patient.

Please check the log file for details. An external code for this drug.

In most cases either a national drug identifier or the ID of this drug in an external database.

In any case, GNUmed will never actively do anything with this code apart from display and making it available when passing this drug to external applications. An unhandled exception has occurred. Announce database maintenance downtime to all connected clients. Apply review to entire document rather than just this part or page. Apply selection to all unsigned results. Apply selection to those unsigned results for which you are to take responsibility. Approved of Are you sure you want to delete
the following form template ?

 "%s (%s)"

You can only delete templates which
have not yet been used to generate
any forms from. Are you sure you want to delete the document ? Are you sure you want to delete this
description from the document ?
 Are you sure you want to delete this episode ?

 "%s"
 Are you sure you want to delete this health issue ?

 "%s"
 Are you sure you want to remove this
address from the patient's addresses ?

The address itself will not be deleted
but it will no longer be associated with
this patient. Assessment Associate to episode: At any time there can only be one open (ongoing)
episode for each health issue.

When you try to open (add data to) an episode on a health
issue GNUmed will check for an existing open episode on
that issue. If there is any it will check the age of that
episode. The episode is closed if it has been dormant (no
data added, that is) for the period of time (in days) you
set here.

If the existing episode hasn't been dormant long enough
GNUmed will consult you what to do.

Enter maximum episode dormancy in DAYS: Auto-check B: Cluster of signs BMI BMI Calculator Backup log file Batch Battery: Below you can add a document description.
 Below you can edit the document description.
 Birthday reminder Born Born   : %s, age: %s

 Both Branch Bug report has been emailed. Bug tracker Bullous C: Syndromic diagnosis Can't connect to database without login information! Cancel Cancel Login. Cancel and discard review, that is, do NOT sign off results. Cancel and do NOT delete the episode. Cancel and do NOT delete the health issue. Cancel editing the allergy/intolerance. Cancel editing the data and discard changes. Cancel moving the narrative entries and leave them where they are. Cancel person selection. Cancel this dialog, do not enlist new GNUmed user. Cancel this dialog. Cancel this review. Cannot access KOrganizer transfer file [%s] Cannot access xDT file

 [%s] Cannot add health issue. No active patient. Cannot attach episode [%s] to health issue [%s] because it already has a running episode. Cannot call IFAP via [%s]. Cannot connect to OpenOffice.

The UNO bridge module for Python
is not installed. Cannot connect to OpenOffice.

You may want to increase the option

 <%s> Cannot connect to database without login information! Cannot create IFAP <-> GNUmed transfer file [%s]. Cannot create new health issue:
 [%(issue)s] Cannot delete encounter type [%s]. It is in use. Cannot delete episode. There is still clinical data recorded for it. Cannot delete health issue. There is still clinical data recorded for it. Cannot delete the only name of a person. Cannot export EMR. No active patient. Cannot find image Cannot interpret input as timestamp. Cannot merge Kirk into another patient. Cannot merge active patient into another patient. Cannot open xDT file.
[%s] Cannot parse [%s] into valid interval. Cannot read SQL from [%s]. Not a text file. Cannot read version information from:

%s Cannot reset fields in edit area.

Programmer forgot to override method:
  <%s.reset_ui> Cannot retrieve version information from:

%s Cannot run script after patient activation. Cannot save data from edit area.

Programmer forgot to override method:
  <%s.save_data> Cannot save report definition without name. Cannot save report definition without query. Cannot set allergy state to <None> because there are allergies stored for this patient. Cannot set allergy state to <some> because there are neither allergies nor a comment available for this patient. Cannot set allergy state to <unknown> because there are allergies stored for this patient. Cannot show EMR summary. No active patient. Cannot start new encounter. No active patient. Cannot write letter. No active patient. Category Caused death Certainty Change Font Change the type of all documents currently having the selected document type. Change translation of selected document type for your local language. Channel Check for new releases of the GNUmed client. Check for updates Check if this condition contributed to causing death of the patient. Check if this condition is to be kept confidential and not disclosed to anyone else. Check if this is a clinically relevant problem. Check if this is an active, ongoing problem. Check if this reaction applies to this drug/generic only, not the entire drug class. Check if this result is clinically relevant. Check if this result is technically abnormal. Check if you want to save a review. Check scope Check this if the communications channel is to be treated confidentially. Check this if this allergy/intolerance is known to exist for sure as opposed to suspected. Check this if you intend to take over responsibility for this document and not just review it. Check this if you want GNUmed to remember your decision and not ask you again. Check this if you want to edit your review. Check this to make this the currently active name. Check this to mark the document as reviewed upon import. If checked you can (and must) decide on "technically abnormal" and "clinically relevant", too. The default can be set by an option. Check this to run GNUmed client in debugging mode. Check this to run GNUmed client in slave mode for remote control. Check this to take over responsibility for initiating action on these results. Checking for client updates Child Choose a column to be used as the X-Axis: Choose a column to be used as the Y-Axis: Choose a form template file Choose an xDT file Choose column from query results ... Chronological EMR Journal
 Class notes Clear Clear all fields or reset to database values. Clear all fields. Clear the editor for the displayed progress note. Clear the fields of the edit area. Will discard unsaved data. Client parameters ... Clinical Notes Clinically relevant Close Close GNUmed Close now Close the dialag. Will discard unsaved data. Close this GNUmed client immediately. Close this GNUmed client. Close this dialog. Closed Closed episodes older than %s days on health issue [%s] Colouring Comment Comment (%s): %%s Comment: Condition Condition diagnosed in the future. Confidential Configuration Configure birthday reminder proximity interval. Configure opening multiple new episodes on a patient at once. Configure review dialog after document display. Configure the chunk size used when exporting BLOBs from the database. Configure the database language Configure the database welcome message (all users). Configure unique ID dialog on document import. Configuring database language Confluent Connect to GNUmed Connecting to backend Contacts Context Continue the existing recent encounter. Contribute Contribute this report definition to the GNUmed community. The report name and SQL command will be sent to the mailing list.

Patient data will NOT be put AT RISK.

You may want to be careful about the SQL itself as it just might contain bits of sensitive data in, say, the WHERE conditions. Country Create a new health issue with another name Create health issue Creating a new health issue ... Creating new allergy not allowed. Crust Current encounter: Current height/mass Current medication Currently there is no active patient. Cannot retrieve EMR text. Currently: D: Scientific diagnosis DICOM viewer Data of current patient to be displayed here. Database ... Database shutdown warning Database welcome message editor Date Date and time when the current (!) encounter ends. Date and time when the current (!) encounter started. Date of birth Deactivate Deactivate record Debugging ... Decrypt Default type Default type for new encounters. Delete Delete Item Delete all Items Delete selected document type. Note that you can only delete document types that are not in use. Delete the allergy selected in the list from the database. Delete the episode if possible (it must be completely empty). Delete the health issue if possible (it must be completely empty). Delete this report definition. Deleted report definition [%s]. Deleting document Deleting document description Deleting episode Deleting external ID Deleting form template. Deleting health issue Deleting name Department Depigmented Description Details Details are found on <http://wiki.gnumed.de>.
 Device(%s): Dialog was cancelled by user Directions Discard Discard the editor for the displayed progress note. Discharge staff Discharge the selected person from the staff list. Display fullscreen snellen chart. Displays drug classe(s) along with their ATC code. Displays the list of persons to select from. Distribution Do you really want to delete this
external ID from the patient ? Doc Document Properties Document handling ... Document signs and symptoms. If reaction is to a drug also document time of onset after drug administration (<24h, 24-72h, >72h). Document type Documents: Documents: %s Dormancy Dose Drug Drug Classes Drug Interactions E&xit	Alt-X E-MAIL EKG EMR ... EMR Summary EMR successfully exported to file: %s EMR text dump Edit Edit Allergy/Intolerance Edit Document Types Edit Item Edit details Edit details of selected staff member. Edit document properties Edit occupation details for the current patient. Edit staff details Edit staff list Edit the (first or only) item selected in the list above. Edit the details for the encounter below: Edit the selected item. Editing a health issue Editing address Editing communications channel Editing document description Editing external ID Editing primary occupation Editing progress note Editor: Elderly Email Email address Empty documents Enable &remote control Encoding missing in xDT file. Assuming [%s]. Encounter Encounter ... Encounter type Encounters: %s (%s - %s): Encrypt Ended Enlist Enlist current patient Enlist person as GNUmed user Enlist this person as a GNUmed user and associate it with the given database account. Enlisting GNUmed users is a priviledged operation.
You must enter the password for the database administrator "gm-dbo" here. Enter a comment on this external ID. Enter a comment on this review. Only available if the review applies to a single result only. Enter a comment relevant to this name (such as "before marriage"). Enter a short comment on what you were trying to do with GNUmed. This information will be added to the logfile for easier identification later on. Enter a short descriptive name for the new health issue: Enter an indicator for the degree of abnormality.
Often +, -, !, ?, () or any combination thereof. Enter any additional data or commentary you wish to provide such as what you were about to do. Enter any additional notes and comments on this address which didn't fit anywhere else. Enter or select a type for the external ID. Enter the actual ID (number, identifier, ...) here. Enter the address or number for this communications channel here. Enter the age in years when this condition was diagnosed. Setting this will adjust the "in the year" field accordingly. Enter the condition (health issue/past history item) here. Keep it short but precise. Enter the house number for this address. Enter the shell command with which to start the
the IFAP drug database.

GNUmed will try to verify the path which may,
however, fail if you are using an emulator such
as Wine. Nevertheless, starting IFAP will work
as long as the shell command is correct despite
the failing test. Enter the waiting zone you want to filter by here.
If you leave this empty all waiting patients will be shown regardless of which zone they are waiting in. Enter the year when this condition was diagnosed. Setting this will adjust the "at age" field accordingly. Episode Episode ... Episodes: %s (most recent: %s%s%s) Eroded Error Error deleting report definition [%s]. Error exporting form template

 "%s" (%s) Error retrieving encounters for episode
%s Error retrieving encounters for this episode. Error retrieving episodes for this health issue. Error running gnuplot. Cannot plot data. Error running hook [%s] script. Error running pdflatex. Cannot turn LaTeX template into PDF. Error saving report definition [%s]. Error searching documents. Error setting responsible clinician for this document. Evolving Examplary filename. Mainly used for deriving a suitable file extension since that matters to some form engines. Most of the time this should already be set correctly when the template data is imported initially. Exit Export chunk size Export for Medistar Exported: %s
 External tools ... External version information such as the exact version/release/revision of a paper form onto which to print data with the help of this template. Exudate FAX Face Failed to  delist patient from %s Failed to enrol patient in %s Fax Field name Filename Find help on http://wiki.gnumed.de, too. Finish and save current work first, then manually close this GNUmed client. Finish work First Date First name First name(s) Firstname(s) Flexural For Form Free eMedicine GNUmed Plugin GNUmed contributors GNUmed exception handler GNUmed generic EditArea dialog GNUmed wiki Gender Generalised Generic multi line text entry dialog Generics Global Search Of Contacts Database Go to the GNUmed bug tracker on the web. Go to the GNUmed wiki on the web. Go to the User Manual on the web. Goal mass Hands Happened Has allergies Head Office Health Issue Health Issue %s%s%s%s   [#%s] Health issue Health issue cannot have been noted at age %s. Patient is only %s old. Health issue selector Height (cm) Help Help desk Help for login screen Help requested
-------------- Here you can add arbitrary text which will be used for sorting health issues in the tree. Here you can choose which plugin you want
GNUmed to display after initial startup.

Note that the plugin must not require any
patient to be activated.

Select the desired plugin below: ID Type ID type Identity If there is only one external patient
source available do you want GNUmed
to immediately go ahead and search for
matching patient records ?

If not GNUmed will let you confirm the source. If this box is checked the episode is over. If not it is currently ongoing. Immediate source activation Impedance: Implanted: In charge In this area GNUmed will place the notes of the
previous encounter as well as notes by other
staff for the current encounter.

Note that this may change depending on which
active problem is selected in the editor below. Indicator Initial plugin Injection site Instructions Intended reviewer: Interactions Internet Invalid Input Invoke actions on the selected measurements. Invoke inspector Invoke the widget hierarchy inspector (needs wxPython 2.8). Issuer It is often advisable to keep old names around and
just create a new "currently active" name.

This allows finding the patient by both the old
and the new name (think before/after marriage).

Do you still want to really delete
this name from the patient ? Itchy KOrganizer is not installed. Keep *this* patient Keep running Keyword L Lab Lab result Language Last Last Date Last confirmed: Last given Last name Last updated Last worked on: %s
 Lastname Laterality Latest vaccinations Launch KOrganizer Leave as is Letter template export Letter writer List not known to be patient-related. List of known communication channels. Lists previous reviews for this document part.

The first line (marked with an icon) will show your previous review if there is one.
The second line (marked with a blue bar) will display the review of the responsible provider if there is such a review.

 You can edit your review below. Lists the allergies known for this patient if any. Lists the existing users in the GNUmed system. Load an xDT file. Load template data from a file. Local language Local name Log file Logged Long-term Lot \# Macular Maculo-Papular Manage descriptions Manage documentation of allergies for the current patient. Manage the components of this drug. Managing document descriptions Mark this checkbox if you want this template to be active in GNUmed. Mass (kg) Maximum duration Maximum duration of an encounter. Maximum length of dormancy after which an episode will be considered closed. Measurement details Measurements and Results: Measurements and Results: %s Medical links (www) Medication Medication list Meds Memo Menu reference (www) Merge Merging patients Merging patients: confirmation Minimum age of an empty encounter before considering for deletion. Minimum duration Minimum duration of an encounter. Minimum empty age Missing GNUmed module Mobile Monty the Serpent && the FSF Present Move Move data to another episode Move episode into existing health issue Move patient down. Move patient up. Move the narrative from the source to the target episode. Moving narrative to another episode Mr Mrs Multiple new episodes Must enter first name. Must enter lastname. Must select gender. N/A Name Narrative New New notes in current encounter Nick name No No EMR data loaded. No data returned for this report. No encounters found for this health issue. No episodes recorded for the health issues selected. No known allergies No matching patient found. No test results to format. No vaccines were chosen No, cancel No, do not send the bug report. No, let me confirm the external patient first. No, only allow editing one new episode at a time. Normal Not relevant Note, however, that this version may not yet
be available *pre-packaged* for your system. Notes Number OK OOo startup time Obese Objective Occupation Occurrence Office Ok Open EMR to ... Open a new progress note editor.

There is a configuration item on whether to allow several new-episode editors at once. Open to encounter level OpenOffice Opening xDT file Operation Optional: A free-text comment on this staff member. Optional: A free-text document description. Optional: A short comment identifying the document. Good comments give an idea of the content and source of the document. Options Or year: Organisation Overweight PDF output file cannot be opened. Painful Papular Part Sources Pass phrase expired Password Password, again Past Hx Patient Patient: Patient: %s (%s), No: %s
 Pattern Per plan Phone Pick &file(s) Pigmented Place Plan Please enter a short note on what you
were about to do in GNUmed: Please enter the primary occupation of the patient.

Currently recorded:

 %s (last updated %s) Please enter your email address. Please enter your pass phrase: Please select a health issue: Please select a person from the list below. Please visit http://www.gnumed.org Population Postal code Preferred Name Prescribed For Prescriptions Previous Encounters Print Print All Items Print Single Item Problem Procedures performed: %s Proceed and try to connect to the newly started GNUmed client. Proceed with login. Programmer must override this text. Progress Note Progress notes handling ... Progress notes in most recent encounter: Promoting episode to health issue Purpura Put caption here. Put question here. Quantity Query result visualizer R RFE Raw content Reaction Reaction Type Reason for discontinuation. Recall Details Recall or Review Recalls and Reviews Recalls/Reviews Reference Reference information Referrals Region Relevant Reload Reload appointments from KOrganizer Remember and don't ask again. Remove the part selected in the above list. Will ask before physical deletion from disk. Remove the selected item(s) from the list. Removing address Removing communication channel Report Report returned no data. Reports Reprint All Items Reprint Item Request Request summary Requests Requests generated this consultation Required fields Required: A short signature for this staff member such as the concatenated initials. Preferably not more than 5 characters. Required: Enter the provider who will be notified about the new document so it can be reviewed. In most cases this is the primary doctor of the patient. Required: The database account assigned to this staff member. Must exist in the database. Required: The organisation role for this staff member. Currently the only supported role is "doctor". Required: The primary episode this document is to be listed under. Required: The type of this document. Required: lastname (family name) Reset Resolving two-running-episodes conflict Results Review Review dialog Reviews by others Role Run the query and present the results below. Save Save All Items no print Save Item no print Save a screenshot of this GNUmed client. Save and confirm the allergy state. Save encounter details and all progress notes. Save finished document. Save patient's EMR as... Save the allergy details in the edit area as either a new allergy or as an update to the existing allergy selected above. Save the allergy/intolerance in the database. Save the currently displayed progress note. Save the encounter details. Save the report definition. Save your review. Save: Saved report definition [%s]. Saving contact data Scale Schema Scope Scratch Pad Screenshot Script must be readable by the calling user only (permissions "0600"): [%s]. Script must be readable by the calling user: [%s]. Script must not be a link: [%s]. Search Search for data in the EMR of the active patient Select Select an address by postcode or street name. Select desired test option from the 'File' menu Select letter or form template. Select or enter the zone the patient is waiting in. Select person from list Select the description you are interested in.
 Select the episode you want to move the narrative to. Select the episodes you are interested in ... Select the health issues you are interested in ... Select the person highlighted in the list above. Select the type of address here. Select the type of address. Select the type of communications channel. Select the type of encounter. Select the urgency for this patient.

Default is 0. Range is 0-10.
Higher values mean higher urgency. Select this if the patient has known allergies. Select this if the patient has no known allergies. Select this if the reaction is a sensitivity. Select this if the reaction is an allergy. Select this if there is no information available on whether the patient has any allergies or not. Select this if you think the selected results are clinically not significant. Select this if you think the selected results are cliniccally significant. Select this if you think the selected results are normal regardless of what the result provider said. Select this if you think the selected results are technically abnormal - regardless of what the result provider said. Select this if you want to agree with the current decision on clinical relevance of the results. Select this if you want to agree with the current decision on technical abnormality of the results. Select this if you want to enable all GNUmed users to invoke this expansion (unless they have defined their own expansion with the same keyword). Select this if you want to keep this patient. Select this if you want to use this text expansion just for yourself. Selecting health issue Send report Sender Sending bug report Sensation Sensing: Seq # Sequence Set "%s" to <False>. Set "%s" to <True>. Set &translation Set pass phrase Set the time to wait for OpenOffice to settle after startup. Set to: Show "kompendium.ch" drug database (online, Switzerland) Show a page of links to useful medical content. Show information about the current database. Show log file Show the database schema definition in your web browser. Shows the episode associated with this document. Select another one or type in a new episode name to associate a different one. Sign Sign off test results and save review status for all selected results. Significant Signing off test results Site Skin level Snellen chart Some network installations cannot cope with loading
documents of arbitrary size in one piece from the
database (mainly observed on older Windows versions)
.
Under such circumstances documents need to be retrieved
in chunks and reassembled on the client.

Here you can set the size (in Bytes) above which
GNUmed will retrieve documents in chunks. Setting this
value to 0 will disable the chunking protocol. Sort documents by Sort documents by the episode they belong to. Sort documents by their type. Sort newest documents to top of tree. Sort unreviewed documents to top of tree. Source episode Specialty Start Start DICOM viewer (%s) for CD-ROM (X-Ray, CT, MR, etc). On Windows just insert CD. Start a new encounter for the active patient right now. Start a new encounter. If there are any changes to the current encounter you will be asked whether to save them. Start a new encounter. The existing one will be closed. Start new Start new encounter Start over (discards current data). Started Started a new encounter for the active patient. Status Street Street info Strength Subjective Substance Subunit Suburb Suburb: Sudden Summary Sun-exposed Surface Target Target episode Technically abnormal Template Template type Test Test error handling Test results Text Text document The URL to retrieve version information from. The command [%s] is not found. This may or may not be a problem. The currently selected patient is: The database account for this user.

The account will be created in the database with proper access rights. Privacy restrictions are currently hardcoded to membership in the PostgreSQL group "gm-doctors".

You can use the name of an existing account but it must not be used by any other GNUmed user yet. The database currently holds no translations for
language [%s]. However, you can add translations
for things like document or encounter types yourself.

Do you want to force the language setting to [%s] ? The database password must be typed again to enable double-checking to protect against typos. The database will be shut down for maintenance
in a few minutes.

In order to not suffer any loss of data you
will need to save your current work and log
out of this GNUmed client.
 The database will be shut down for maintenance in a few minutes. The database you are connected to is marked as
"in-production with controlled access".

You indicated that you want to include the log
file in your bug report. While this is often
useful for debugging the log file might contain
bits of patient data which must not be sent out
without de-identification.

Please confirm that you want to include the log ! The doctor in charge who will have to assess and sign off this result. The document type in the local language. The document type under which to store forms generated from this template. The document type, usually in English. The email address of the user for sending bug reports, etc. The episode the displayed narrative currently belongs to. The episode you want to move the displayed narrative to. The following people kindly contributed to GNUmed.
Please write to <gnumed-devel@gnu.org> to have your
contribution duly recognized in this list or to have
your name removed from it for, say, privacy reasons.

Note that this list is sorted alphabetically by last
name, first name. If the only identifier is an email
address it is sorted under the first character of
the user name.
%s The keyword you want to trigger this text expansion.

Try to avoid words or abbreviations in their day-to-day form as you may want to use them verbatim. Rather prefix or suffix your keywords with, say, "*" or "$". It is wise to not suffix keywords with typical word separators, such as:

       ! ? . , : ; ) ] } / ' " SPACE TAB LINEBREAK

as those are needed to detect when to trigger keyword expansion. The lab request already exists but belongs to a different patient. The lower bound of the range of technically normal values. The lower bound of the target range for this test in this patient. The medical problem this test results pertains to. The new episode:

 "%s"

will remain unassociated despite the editor
having been invoked from the health issue:

 "%s" The original filename (if any). Only editable if invoked from a single part of the document. The password for the new database account. Input will not be shown. The query failed. The result of the measurement. Numeric and alphanumeric input is allowed. The sequence index or page number. If invoked from a document instead of a page always applies to the first page. The server and client clocks are off
by more than %s minutes !

You must fix the time settings before
you can use this database with this
client.

You may have to contact your
administrator for help. The server and client clocks are off
by more than %s minutes !

You should fix the time settings.
Otherwise clinical data may appear to
have been entered at the wrong time.

You may have to contact your
administrator for help. The type of the external code of this drug, if any. The type of this template. The intended use case for this template. The units this result comes in. The upper bound of the range of technically normal values. The wxPython GUI framework hasn't been initialized yet! There already is a health issue

 %s

What do you want to do ? There are no encounters for this episode. There are no episodes for this health issue. There is no narrative for this episode in this encounter. There is no version information available from:

%s These patients are waiting.

Doubleclick to activate (entry will stay in list). This displays the current allergy state as saved in the database. This documents why the encounter takes place.

It may be due to a patient request or it may be prompted by other reasons. Often initially collected at the front desk and put into a waiting list comment. May turn out to just be a proxy request for why the patient really is here.

Also known as the Reason For Encounter/Visit (RFE). This field lists the parts belonging to the current document. This is not correctly encrypted text! This lists the available document types. This shows the list of active problems, They include open episodes as well as active health issues. This shows the list of previous encounters This signing applies to ALL results currently selected in the viewer.

If you want to change the scope of the sign-off
you need to narrow or widen the selection of results. This summarizes the outcome/assessment of the consultation from the doctors point of view. Note that this summary spans all the problems discussed during this encounter. This will send a notification about database downtime
to all GNUmed clients connected to your database.

Do you want to send the notification ?
 Threshold Throw an exception to test error handling. Tips and Hints Title To To properly create an address, all the related fields must be filled in. Today's KOrganizer appointments ... Total number of vaccinations recorded for the corresponding target condition. Translate this or i18n into <en_EN> will not work properly ! Trigger Truncal Type Type (English) Type name of disease Type or select a city/town/village/dwelling. Type or select a country. Type or select a first name (forename/Christian name/given name). Type or select a last name (family name/surname). Type or select a street. Type or select a title. Note that the title applies to the person, not to a particular name ! Type or select an alias (nick name, preferred name, call name, warrior name, artist name). Type or select an occupation. Type or select the description for this episode. It should be a summary for the episode of illness. Type or select the suburb. Type: URL UUID display Unable to display the file:

 [%s]

Your system does not seem to have a (working)
viewer registered for the file type
 [%s] Unable to send mail. Cannot contribute report [%s] to GNUmed community. Unattributed episodes Underweight Unit: Units Unknown Unlock mouse Unlock mouse pointer in case it got stuck in hourglass mode. Unspecified error saving data in edit area.

Programmer forgot to specify proper error
message in [%s]. Update handling ... Urgent User interface ... User manual (www) User parameters: User1 User2 User3 Username Vaccination Vaccinations Vaccinations by Recommender (X = patient on regime, O = patient not on regime)  Vaccinations: Vaccinator Vaccine Value Value [%s] not valid for option <%s>. Verify which patient this lab request really belongs to. Verifying database settings Verifying database version Version Version information loaded from:

 %s View log View the log file. View the part selected in the above list. View the reference for menu items on the web. Waiting list Waiting list details for this patient. Waiting time Welcome message What is the patient here for. Could be the Reason for Encounter. When GNUmed cannot find an OpenOffice server it
will try to start one. OpenOffice, however, needs
some time to fully start up.

Here you can set the time for GNUmed to wait for OOo.
 When Noted When a patient is activated GNUmed can
be told to switch to a specific plugin.

Select the desired plugin below: When a patient is activated GNUmed checks the
chart for encounters lacking any entries.

Any such encounters older than what you set
here will be removed from the medical record.

To effectively disable removal of such encounters
set this option to an improbable value.
 When a patient is activated GNUmed checks the
proximity of the patient's birthday.

If the birthday falls within the range of
 "today %s <the interval you set here>"
GNUmed will remind you of the recent or
imminent anniversary. When adding progress notes do you want to
allow opening several unassociated, new
episodes for a patient at once ?

This can be particularly helpful when entering
progress notes on entirely new patients presenting
with a multitude of problems on their first visit. When checking for updates, consider latest branch, too ? When was the allergy state last confirmed. When was this result actually obtained. Usually the same or between the time for "sample taken" and "result reported". Whether this document report technically abormal results. Whether this document reports clinically relevant results. Note that both normal and abnormal resuslts can be relevant. Whether to allow saving documents without parts. Whether to auto-check for updates at startup. With Sample Write a letter for the current patient. XDT Viewer XDT field XDT field content Yes Yes, allow for multiple new episodes concurrently. Yes, delete Yes, search for matches immediately. Yes, send the bug report. You are logged in as %s%s.%s (%s). DB account <%s>. You must provide an xDT file on the command line.
Format: --xdt-file=<file> You must select a template file before saving. You tried to log in as [%s] with password [%s].
Host:%s, DB: %s, Port: %s You want to associate the running episode:

 "%(new_epi_name)s" (%(new_epi_start)s - %(new_epi_end)s)

with the health issue:

 "%(issue_name)s"

There already is another episode running
for this health issue:

 "%(old_epi_name)s" (%(old_epi_start)s - %(old_epi_end)s)

However, there can only be one running
episode per health issue.

Which episode do you want to close ? Your bug report will be sent to:

%s

Make sure you have reviewed the log file for potentially
sensitive information before sending out the bug report.

Note that emailing the report may take a while depending
on the speed of your internet connection.
 Your review Zip Zip code Zone [%s] is not a readable file active all files all files (Win) already reported animal any name part bilateral both boy button_1 child clinically relevant close existing episode "%s" close moving (new) episode "%s" closed confidential corrected result d::day_abbreviation dD_keys_day date of birth date of birth/death daylight savings time in effect definite discard entry and cancel edit encounter details emr-export enrolled regime vaccinations not yet given episode     : %s error retrieving unreviewed lab results error with placeholder [%s] external patient ID external patient source (name, gender, date of birth) fatal female final finished first name full name generic error message generic info message generic warning message girl gm_ctl_client: starting slave GNUmed client h::hour_abbreviation hH_keys_hour hdwmy (single character date offset triggers) health issue: %s icon-future_mom icon_inbox icon_letter_A in %d day(s) - %s in %d week(s) - %s inactive include log file in bug report initialize input fields for new entry internal patient ID interval_format_tag::days::d interval_format_tag::months::m interval_format_tag::weeks::w interval_format_tag::years::y invalid allergy state [%s] kg to lose lab [%s], request ID [%s], expected link with patient [%s], currently linked to patient [%s] last check: last confirmed %s
 left likely load file loading xDT file long-term m::month_abbreviation mM_keys_month male missing, reported later mo::month_only_abbreviation modified entry must have a recipient must select address name name of xDT file name, date of birth name, gender, date of birth name: first-last name: last, first name: last-first names: first last names: first-last, date of birth names: last, first names: last-first, date of birth names: last-first, dob ndmy (single character date triggers) need lab request when inserting lab result new episode no hook specified, please report bug no known allergies no viewer installed not clinically relevant occupation old episode ongoing only documents added or ordered by brand original entry partial per target condition pgAdmin III pgAdmin III: Browse GNUmed database(s) in PostgreSQL server. preliminary primary occupation of the patient programmer forgot to specify error message programmer forgot to specify info message programmer forgot to specify status message programmer forgot to specify warning relevant reset entry review document right right now (%s, %s) s::second_abbreviation save entry into medical record saving clinical data short-term significant subdivision tab1 take over responsibility technically abnormal text files today (%s) tomorrow (%s) type units unknown allergy state unknown gender unknown reaction unknown test results output format [%s] unknown test status [%s] unknown vaccinations output format [%s] until w::week_abbreviation wW_keys_week xDT files xDT viewer y::year_abbreviation yYaA_keys_year yesterday (%s) Project-Id-Version: gnumed
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-07-17 11:08+0000
Last-Translator: Luis Mendonça <Unknown>
Language-Team: Portuguese <pt@li.org>
Language: pt
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2010-07-22 16:41+0000
X-Generator: Launchpad (build Unknown)
 

Por favor consulte o log de mensagens de erro para mais detalhes ! 
 Na lista abaixo marque os problemas de saúde nos quais deseja realizar o relatório.
 
 Esses são os episódios conhecidos para o problema de saúde selecionado.

 Agora, marque os episódios nos quais deseja realizar o relatório.
 
Command
(SQL) 
Versão do banco de dados incorreta.

Essa versão de banco de dados não pode ser usada com esse cliente:

 versão do cliente: %s
 versão do banco de dados detectado: %s
 versão do banco de dados necessária: %s

Atualmente conectado ao banco de dados:

 host: %s
 banco de dados: %s
 usuário: %s
 
Selecione o tipo de consulta que você quer editar !
 
O cliente será iniciado de qualquer maneira pois você
está usando uma versão teste/desenvolvimento do GNUmed

Pode ser que existam erros com o esquema de banco de dados. 
Por favor relate ou os concerte. Não confie que esse banco de dados funcionará
de maneira correta em todos os casos! 
Há um problema sério com o banco de dados:

%s

Você poderá ter que contactar seu administrador. 
Você deve conectar a outro banco de dados para
poder usar o cliente GNUmed. Você deverá contactar seu
administrador para obter ajuda.      Referências         Alertas      risco: %s   Seu horário: %s - %s (@%s = %s%s)
  %s medicação activa
  %s documentos
  %s consulta(s) de %s a %s
  %s Problemas conhecidos
  %s Resultados dos testes
  (E)   (Confirmado a %s)  ***** CONFIDENCIAL *****  - correcção de problemas e novas funcionalidades
  - apenas correcção de problemas
  - podem ser necessárias correcções à base de dados
  - necessária uma actualização da base de dados
  Criado durante a consulta: %s (%s - %s)   [#%s]  Mais recente: %s - %s  Nova versão: "%s"  Primeira manifestação aos: %s  Endereço Postal   A sua versão corrente: "%s"
  contribuiu para a morte do paciente  opção [%s]: %s %(number)s %(street)s, %(urb)s %(postcode)s %d. de %s (mês passado) - a %s %d. de %s (próximo mês) - a %s %d. de %s (deste mês) - a %s %s (%s último ano) %s (%s próximo ano) %s (%s este ano) %s Bytes %s: consulta (%s) %sAlergia: %s, %s (anotado %s)
 %sEpisodio "%s" [%s]
%sConsultas: %s (%s - %s)
%sEditado pela última vez em: %s
 &Anormal &Ações ...  &Ativar &Tudo &Todos os usuários &Alergias &Alergia &Cancelar &Limpar &Fechar &Contactos &Correspondências &Acionar Modo Relatório de Erros &Definitivo &Apagar &Descartar &Medicamentos &Prontuário &Editar &Consulta &Ajuda &Recursos &Somente eu &Novo &Normal &Nota &Profissão &Consultório &Sim &História Pregressa (problema de saúde / HMP) &Redefinir &Relevante &gerador relatórios &Limpar &Restaurar &Escanear página(s) &Selecionar &Sensibilidade &Resumo &Assumir Responsabilidade &Tolera outros da classse &Ferramentas &Urgencia &Vacinações &Escrever Carta &Local &clinicamente relevante &revisar e assinar &tecnicamente anormal &visualizador xDT (%s pacientes) (D) (você é/não é o revisor primário) ** Data de nascimento desconhecida ** Tem alergia(s) + -- ... Motivo ... consultas ... episódios ... questões de saúde ... lista de exames deverá ser inserida aqui ... 1 Byte 1st and (up to 3) most recent (of %s) encounters (%s - %s): 63 - Normal - 79 63< Normal >79 <No embed string for [%s]> <estado alérgico não perguntado> <channel> não pode ser vazio <estado alérgico atual> <issuer> não pode ser vazio <última vez confirmada> <name> não pode estar vazio <not supplied> <number> não pode ser vazio <procedimento restrito> <street> não pode ser vazio <urb> não pode ser vazio <zip> não pode ser vazio ?em progresso ?curto-prazo Um cliente GNUmed escravo foi iniciado porque
mais nenhum outro cliente foi encontrado. Terá agora
de introduzir a sua identificação e senha na
janela de login como habitualmente

Mude para a janela de login agora e prossiga
com o login. Assim que o GNUmed tiver começado
com sucesso volte para esta janela
 Uma avaliação clínica do resultado.
Geralmente por um médico. Um comentário sobre o estado da alergia. Um comentário dessa revisão. Um nome longo descrevendo esse modelo. Está disponível uma nova versão do GNUmed.

 Pseudónimo curto para identificar o utilizador. É utilizado na ficha clínica entre outros usos. Um nome curto e fácil de lembrar para esse modelo. Comentário técnico do exame.
Geralmente dado pelo Técnico Laboratorial que assinou o laudo. &Anormal A: Sinais ALERGIAS Resumo da Consulta Classe/ATC Interromper Imterromper e NÃO ligar ao GNUmed Cancelar a janela e não mudar as opções atuais. Sobre GNUmed Sobre Banco de Dados Conta Adquire uma página de uma fonte de imagem (scaner, câmera). Isso pode trazer uma janela intermediária. É usado o Sane (Linux) ou TWAIN (Windows) Editar os itens selecionados na lista acima Ativar Ativar paciente e removê-lo da lista. Ativar paciente, mas não o remover da fila de espera. Ativar o paciente clicando duas vezes na sua linha. Ativo Problemas activos Adicione um arquivo como uma porção. Será mostrado uma janela de seleção de arquivos. Adicionar um novo item a lista acima. Adicionar novo item à lista. Adicione um novo usuário ao sistema GNUmed. Adicionar um problema de saúde (doença pregressa) ao Prontuário do paciente ativo Adicione o digitado acima como novo formato de documento. Adicionando novo problema de saúde Adicionar uma descrição sobre o documento. Adicionando novo endereço Adicionando novo canal de comunicações. Adicionando novo tipo de consulta Adicionando novo ID externo Adicionando novo nome Endereço Dados do endereço deverão ser completos. Valores ajustados Senha do administrador Administrativo Adulto Idade Idade: Alcunha Alergia Gestor de alergias Alergias e Sensibilidades - Resumo Detalhes da alergia Estado da alergia Um valor alfanumérico de normalidade desse exame. Um valor ideal alfanumérico para esse exame nesse paciente. Ocorreu um erro ao exportar os registros Demográficos
Por favor consulte o log para mais detalhes. Ocorreu um erro ao gerar a listagem
do registo médico electrónico (RME) para o paciente activo.

Por favor consulte os detalhes no ficheiro de log. Um código externo para esta droga.

Na maioria dos casos seja uma droga de identificação nacional ou a identificação da droga em um banco de dados externo.

Em qualquer caso, o GNUmed nunca irá fazer nada com este código além de mostrar e disponibilizá-lo quando passar esta droga para aplicações externas. Ocorreu uma exceção não prevista Anunciar manutenção de Banco de Dados a todos os usuários conectados. Aplicar a revisão para todo o documento ao invés de apenas a essa página ou porção. Aplique a seleção a todos os resultados não vistos. Aplique a seleção para aqueles resultados não vistos pelos quais você tomará responsabilidade. Aprovado de em certeza que quer apagar o

"%s (%s)"

Você só poderá apagar modelos que
não foram usados para gerar quaisquer
tipo de formulários. Você realmente quer apagar esse documento? Você realmente quer apagar esse
ID externo do paciente?
 Tem certeza que deseja apagar esse episódio ?

 "%s"
 Tem certeza que deseja apagar esse problema de saúde? 

 "%s"
 Você tem certeza que quer remover esse 
endereço da lista de endereços do paciente? 

O endereço em si não será deletado, 
mas não estará mais associado a esse
paciente. Avaliação Associar ao episódio: Só pode haver um episódio (ativo) para qualquer problema de saúde.


Quando tentar abrir (adicionar dados) a um episódio de um problema
de saúde o GNUmed checará se existe um episódio aberto para esse 
problema. Se existir consultará a idade daquele episódio. O episódio 
será fechado se estiver suspenso (i.e. nenhum dado adicionado) pelo
período de tempo (em dias) que será configurado aqui.


Se um episódio não esteve suspenso por tempo suficiente o GNUmed
perguntará o que deve fazer.

Configure o número máximo de DIAS para suspensão de episódios: Automático B: Grupo de sinais IMC Calculadora de IMC Faça um backup do arquivo de log Lote Bateria: Abaixo pode-se adicionar uma descrição sobre o documento.
 Abaixo pode-se editar a descrição sobre o documento.
 Lembrete de aniversário Nascido Nascido: %s, idade: %s

 Ambas Filial Relatório de Erro foi enviado. Localizador de Erros Bolhoso C: Diagnóstico sindrômico Não é possível ligar à base de dados sem informação de login! Cancelar Cancelar o Login Cancelar e descartar a revisão, isso é, NÃO dar visto aos resultados Cancelar e NÃO apagar o episódio. Cancelar e NÃO apagar o problema de saúde. Cancelar a edição da alergia/intolerância Cancelar a edição de dados e descartar mudanças Cancelar mover a descrição e deixá-las onde estão. Cancelar seleção de pessoa. Cancele essa janela, não adicione esse novo usuário GNUmed. Cancelar essa janela. Cancelar essa revisão. Não é possível acessar o arquivo de transferência do KOrganizer [%s] Não é possível aceder ao ficheiro xDT

 [%s] Não foi possível adicionar problema de saúde. Não há paciente ativo. Não foi possível anexar o episódio[%s] ao problema de saúde [%s] porque o mesmo já tem um episódio aberto. Cannot call IFAP via [%s]. Não foi possível conectar ao OpenOffice.

O módulo UNO do Python
não está instalado. Não foi possível conectar ao OpenOffice.

Você deve aumentar a opção

 <%s> Não é possível ligar à base de dados sem informação de login! Cannot create IFAP <-> GNUmed transfer file [%s]. Não é possível criar nova questão de saúde: 
 [%(issue)s] Não é possível apagar o tipo de consulta [%s]. Está em uso. Não foi possível apagar o episódio. Ainda há dados clínicos armazenados para ele. Não foi possível apagar a questão de saúde. Ainda há dados clínicos armazenados para ela. Não é possível apagar o único nome da pessoa. Não é possível exportar Prontuário Eletrônico. Não há paciente ativo. Não foi possível achar imagem Não foi possível interpretar a digitação como data válida. Não é possível mesclar Kirk com outro paciente Não é possível fundir informação do paciente activo com outro Não é possível abrir ficheiro xDT.
[%s] Não é possível traduzir [%s] em um intervalo válido. Não foi possível ler SQL de [%s]. Não é arquivo de texto. Não foi possível ler informação de:

%s Não foi possível restaurar dados do campo de edição

Programador esqueceu de impossibilitar o método:
  <%s.reset_ui> Não foi possível obter informação de versão de:

%s Não é possível rodar o script depois de ativar o paciente. Não foi possível salvar dados do campo de edição

Programador esqueceu de redefinir método:
  <%s.save_data> Não é possível salvar uma definição de relatório sem nome. Não é possível salvar uma definição de relatório sem pesquisa. Não é possível modificar o estado alérgico para <Nenhuma> pois existem alergias salvas para esse paciente. Não é possível modificar o estado alérgico para <algum> pois não existem nem alergias nem comentários para esse paciente. Não é possível modificar o estado alérgico para <desconhecido> pois existem alergias salvas para esse paciente. Não é possível mostrar resumo de Prontuário. Não há paciente ativo. Não foi possível iniciar nova consulta. Não há um paciente ativo. Não é possível escrever carta. Não há paciente ativo. Categori­a Causou o Óbito Certamente Alterar Tipo de Letra Mudar o formato de todos os documentos que tenham o formato selecionado. Mudar a tradução do formato de documento para sua língua local. Canal Checar por novos lançamentos do cliente GNUmed. Checar por atualizações Clique se essa condição contribuiu para a morte do paciente. Clique se essa condição é confidencial e não foi contada a mais ninguém. Clique se for relevante clinicamente. Clique se o problema está ativo atualmente ou condição crônica. Clique aqui se essa reação se aplica para esse medicamento/genérico especificamente, sem incluir a classe inteira a qual ele pertence. Clique se esse exame é clinicamente relevante. Clique se esse exame é tecnicamente anormal. Clique se quer salvar uma revisão. Checar abrangência Clique aqui se esse ccanal de comunicação deve ser tratado como confidencial. Verificar se esta alergia/intolerância existe de facto em vez de se ruma suspeição. Clique se você quer se responsabilizar por esse documento ao invés de apenas o revisar. Seleccione aqui se deesejar que o GNUmed memorize esta decisão e não volte a efectuar a pergunta. Clique se você deseja editar sua revisão. Clique para escolher o nome ativo atual. Clique para marcar o documento como tendo sido revisto ao ser importado. Se clicado você deverá decidir se é "tecnicamente anormal" ou "clinicamente relevante". Isso poderá ser padronizado. Clique aqui se você deseja acionar o modo Relatório de Erros Clique aqui para rodar o cliente GNUmed em modo escravo para acesso remoto Clique para tomar responsabilidade por agir com base nesse resultado. Checando por atualizações do cliente Criança Escolha uma coluna para ser usada como o eixo X Escolha uma coluna para ser usada como o eixo Y Escolha um modelo de formulário Escolha um ficheiro xDT Escolha a coluna dos resultados da pesquisa ... Diário cronológico do registo médico electrónico
 Detalhes da classe Limpar Apagar todos os campos ou reinicializar para os valores da base de dados. Limpar os campos. Limpar o editor da atual Nota de Evolução. Limpa os campos na área de edição. Descarta os dados não gravados. Parâmetros do Cliente ... Anotações Clínicas Relevante Clinicamente Fechar Fechar GNUmed Fechar Agora Fechar a janela. Serão perdidos os dados não salvos. Fechar esse cliente GNUmed imediatamente Fechar esse cliente GNUmed. Fechar essa janela Fechado Episódios fechados há mais de %s dias no problema de saúde [%s] Coloração Comentário Comentário: %s Comentário: Condição Condição diagnosticada no futuro. Confidencial Configuração Configure o lembrete de aniversário. Configure abrir múltiplos episódios novos em um único paciente ao mesmo tempo. Configure a janela de revisão após mostrar documentos. Configure o tamanho do pedaço (chunk size) ao exportar BLOBs do banco de dados. Configure o idioma do Banco de Dados Configure a mensagem de boas vindas do bano de dados (para todos os usuários). Configure o ID único da janela na importação de documentos Configurando o idioma do banco de dados Confluentes Ligação ao GNUmed Ligando a backend Contatos Contexto Continue a consulta recente já existente. Contribuir Contribuir esse relatório para a comunidade GNUmed. Esse relatório e o comando SQL serão enviados para a lista de e-mails.

Dados do paciente NÃO serão colocados em RISCO

Você deve ter cuidado com o SQL em si, pois há a possibilidade que ele contenha dados importantes, como a condição WHERE. País Criar um novo problema de saúde com outro nome Criar Problema de Saúde Criando um novo problema de saúde ... Criar nova alergia não é permitido Crosta Consulta Atual Atual altura/peso Medicação atual Atualmente não há paciente ativo. Não é possível abrir texto do Prontuário. Actualmente: D: Diagnóstico científico visualizador DICOM Dados do paciente corrente deverão ser mostrados aqui. Banco de Dados ... Aviso de desligamento de Banco de Dados Editor da mensagem de boas-vindas do banco de dados. Data Data hora do fim da consulta corrente Data hora de início da consulta corrente Dia de Nascimento Desativar Desativar o registro Console de Erros ... Decriptar Tipo inicial Tipo padrão de novas consultas. Apagar Apagar item Apagar todos itens Apagar o formato de documento selecionado. Só podem ser apagados formatos que não estejam em uso. Apagar da base de dados a alergia seleccionada. Apagar o episódio se possível (ele deverá estar vazio). Apagar a problema de saúde se possível (deverá estar completamente vazio). Apagar essa definição de relatório. Definição de relatório [%s] apagada. Apagando documento Apagar descrição sobre o documento. Apagando episódio Apagando ID externo Apagar do modelo Apagando o problema de saúde Apagando nome Departamento Despigmentado Desccrição Detalhes Procurar mais detalhes em <htttp://wiki.gnumed.de>.
 Dispositivo(%s): Janela foi cancelada pelo usuário Informações Adicionais Descartar Descarta o editor da atual Nota de Evolução. Demitir membro da equipe Demitir (retirar) a pessoa selecionada dos membros da equipe. Mostre cartaz de Snellen em Tela Inteira Mostrar a classe da droga junto com seu código ATC. Mostra a lista de pessoas que podem ser selecionadas. Distribuição Você realmente quer apagar esse
ID externo do paciente? Médico Propriedades do Documento Administração de documentos ... Registre sinais e sintomas. Se a reação é a um medicamento descreva também o tempo de início deles após a administração (<24h, 24-72h, >72h). Tipo de Documento Documentos: Documentos: %s Dormência Dose Medicamento Classe da Droga Interações medicamentosas E&xit	Alt-X E-MAIL ECG Prontuário ... Resumo do Prontuário Prontuário exportado para arquivo: %s Saída em forma de texto do Prontuário Editar Editar Alergia/Intolerância Editar os Formatos de Documentos Editar Item Editar detalhes Editar detalhes do membro da equipe selecionado Editar propriedades do documento Editar detalhes de profissão para o paciente atual Editar detalhes de membro da equipe Editar a lista de membros da equipe Editar (somente o primeiro) item selecionado na lista acima. Editar os detalhes da consulta abaixo: Editar o item selecionado. Editando um problema de saúde Editando endereço Editando canal de comunicações Editar descrição do documento Editando ID externo Editando a profissão Editando Notas de Evolução Editar Idoso Email Endereço de e-mail Documentos vazios Acionar &acesso remoto Codificação em falta no ficheiro xDT. Assumido [%s]. Consulta Consulta ... Tipo de Consulta Consultas: %s (%s - %s): Encriptar Terminou Adicionar Incluir paciente atual Adicione pessoa como usuário GNUmed Adicione essa pessoa como usuário GNUmed e a associe com a respectiva conta no banco de dados. Adicionar usauários GNUmed é uma operação privilegiada.
Você deve digitar a senha do administrador do banco de dados "gm-dbo" aqui. Digite um comentário a esse ID externo Digite um comentário a essa revisão. Disponível somente se essa revisão se aplica a apenas um exame. Faça um comentário relativo a esse nome (como "antes do casamento"). Digite um pequeno comentário sobre o que você estava tentando fazer com o GNUmed. Essa informação será adicionada ao arquivo de log para ser mais fácil de identificar. Digite um nome para esse novo problema de saúde Digite um indicador para anormalidade
Geralmente +, -, !, ?, () ou uma combinação deles. Entre aqui qualquer comentário você desejaria adicionar, como por exemplo o que estava fazendo. Preencha qualquer dado adicional ou comentários sobre esse endereço (se não for adequado em outro lugar). Digite ou selecione um tipo de ID externo. Digite o ID (número, identificador, etc.) aqui. Entre o endereço ou número para esse canal de comunicação aqui. Digite a idade em anos quando essa condição foi diagnosticada. Essa informação automaticamente ajustará o campo "no ano". Entre o problema (problema de saúde/história pregressa) aqui. Seja conciso, mas preciso. Preencha o número para esse endereço. Entre aqui o comando de shell que iniciará o
banco de dados IFAP.

O GNUmed tentará verificar o caminho, no entanto isso 
poderá falhar se estiver usando um emulador como o 
Wine. Mesmo assim iniciar o IFAP funcionará contanto
que o comando de shell esteja correto não importando 
o teste do caminho tendo falhado. Defina aqui a zona de espera que você quer filtrar.
Se estiver vazio todos os pacientes em todas as zonas de espera serão mostrados, não importando em qual zona estejam. Digite o ano do diagnóstico dessa doença. Essa informação automaticamente ajustará o campo "idade". Episódio Episódio ... Episódios: %s (mais recente: %s%s%s) Erodida Erro Erro ao apagar a definição de relatório [%s]. Erro ao exportar do modelo

"%s" (%s) Erro ao obter consultas para esse episódio
%s Error retrieving encounters for this episode. Erro ao obter dados para este problema de saúde. Erro ao correr o gnuplot. Não é possível traçar dados. Erro ao executar o script de hook [%s] Erro correndo pdflatex. Não foi possível converter o modelo LaTeX em PDF. Erro ao salvar a definição de relatório [%s]. Erro ao procurar documentos. Erro ao alocar o prestador responsável por esse documento Evolutiva Exemplo de nome de arquivo. Será usado na maioria das vezes para derivar uma extensão de arquivo adequada, isso é importante para alguns formulários externos. Na maioria das vezes isso deverá estar já configurado corretamente quando os dados do modelo forem inicialmente importados. Sair Exportar tamanho do pedaço (chunk size) Exportar para Medistar Exportado: %s
 Ferramentas externas ... Versão externa como a exata versão/lançamento/revisão do formulário em papel para qual esse modelo vai possiblitar imprimir dados. Exsudato FAX Face Falha ao remover o paciente de %s Falha ao alocar paciente ao regime de %s Fax Nome do campo Arquivo Para mais ajuda vá em http://wiki.gnumed.de Terminar e salvar trabalho atual primeiro, depois manualmente fechar esse cliente GNUmed. Terminar trabalho 1ª data Nome Nome Nome Flexão Para De eMedicina Gratuita Plugins GNUmed Contribuem para o GNUmed Ocorreu uma exceção com o GNUmed Janela genérica do GNUmed para Editar Área GNUmed wiki Gênero Generalizado Generic multi line text entry dialog Genéricos Procura global na BD de contactos Localizador de Erros do GNUmed na internet (Bug tracker) Wiki do GNUmed na internet. Manual do usuário na internet Peso ideal Mãos Ocorreu Tem alergias Sede Problema de Saúde Problema de saúde %s%s%s%s   [#%s] Problema de Saúde Problema de saúde não pode ter sido notada com idade %s. Paciente tem apenas %s de idade. Seletor de Problema de Saúde Altura (cm) Ajuda Setor de Ajuda Ajuda para a janela de login Solicitada ajuda Aqui pode-se adicionar um texto qualquer que será usado para organizar os problemas de saúde na árvore. Aqui você pode decidir qual o plugin que 
quer que o GNUmed mostre após iniciar. 

Esse plugin não pode exigir qualquer 
paciente para ser ativado

Selecione o plugin desejado abaixo: Tipo de ID Tipo de ID Identidade Se há apenas uma fonte externa de pacientes
você quer que o GNUmed já se adiante e
procure automaticamente por pacientes 
nessa fonte externa? 

Se não o GNUmed deixará que confirme a fonte. Se essa caixa estiver marcada o episódio acabou. Se não ele ainda está ativo. Ativação imediata de fonte Impedância: Implantado: Responsável Nessa área o GNUmed colocará as notas da
consulta prévia, assim como as notas de outro
membro da equipe para a consulta atual.

Repare que isso pode mudar conforme qual
Problema Ativo seja selecionado no editor abaixo. Indicador Plugin inicial Local de aplicação Instruções Revisor Pretendido: Interações Internet Digitação Inválida Agir nos resultados selecionados Invocar Inspetor Invocar o widget inspetor de hierarquia (necessita de wxPython 2.8) Emitente É recomendável que se mantenha os nomes antigos,
apenas crie um novo "nome ativo".

Isso permite que se ache o paciente tanto pelo nome antigo
quanto pelo novo nome (i.e. antes e depois do casamento).

Você ainda assim quer definitivamente apagar
esse nome do paciente? Pruriginosa KOrganizer não está instalado. Manter *este* paciente Continuar executando Atalho E Laboratório Resultado exame Idioma Sobrenome Última data Última confirmação: Última Sobrenome Atualizado Última consulta a: %s
 Sobrenome Lateralidade Últimas vacinações Executar KOrganizer Deixe como está Exportar Modelo de Carta Escritor de Cartas Lista não reconhecida como relacionada a paciente. Lista dos canais de comunicação conhecidos. Lista as revisões prévias para essa porção de documento.

A primeira linha (marcada com ícone) mostrará sua última revisão, se houver.
A segunda linha (marcada com barra azul) mostrará a revisão de responsabilidade do provedor, se houver.

Você pode editar sua revisão abaixo. Listar as alergias conhecidas para este paciente Listar os usuários atuais no sistema GNUmed. Abrir um arquivo xDT. Abrir dados de modelo de um arquivo. Língua local Nome Local Arquivo de Log Logado Uso contínuo Lote \# Macular Máculo-Papular Administrar as descrições. Administrar documentação de alergias para o paciente atual Gerenciar os componentes deste medicamento. Administrar descrições de documentos Clique se você quer que esse modelo esteja ativado no GNUmed. Peso (kg) Duração máxima Duração máxima de uma consulta Tempo máximo de dormência após o qual o episódio será considerado fechado. Detalhes dos Resultados Medidas e resultados: Medidas e Resultados: %s Links médicos (www) Medicação Lista de medicamentos Mediamentos Nota Referência do menu (www) Agrupar Agrupar pacientes Mesclar pacientes: confirmação Idade mínima de uma consulta vazia antes de considerar apagá-la . Duração mínima Duração mínima de uma consulta. Idade mínima vazia Fala o Módulo GNUmed Telemóvel Monty a Serpente && o FSF Apresentam Mover Mover dados para outro episódio Mover episódio para o problema de saúde existente Mover paciente para baixo. Mover o paciente para cima. Mover a descrição de de seu episódio atual para o episódio escolhido. Mover descrição para outro episódio Sr Sra Múltiplos novos episódios É necessário um Nome. É necessário um Sobrenome. É necessário selecionar o sexo. N/D Nome Narrativa Novo Notas adicionais na consulta corrente Apelido Não Não há dados abertos no Prontuário. Não foram retornados dados para esse relatório. Sem consultas para este problema de saúde. Não há episódios registrados para o problema de saúde Não tem alergias conhecidas Não foi encontrado esse paciente. Não há resultados de exames para formatar. Não foram selecionadas vacinas Não, cancelar Não, não enviar o relatório de erro. Não, me deixe confirmar o paciente externo antes. Não, apenas abra um episódio novo por vez. Normal Não relevante Note, poreḿ, que esta versão pode ainda não
estar disponível num "pacote de instalaçã" para o seu sistema. Notas Número OK Tempo de início do OOo Obesidade Objectivo Profissão Ocorrência Escritório Aceitar Abrir Prontuário até ... Abre novo Editor de Notas de Evolução.

Há um item em Configurações que permite ou não que vários "novos episódios" sejam abertos simultaneamente. Abrir arvóre até o nível de Consultas OpenOffice Abrindo ficheiro xDT Operação Opcional: Uma descrição livre desse membro da equipe. Opcional: Uma descrição livre sobre o documento. Opcional: Um comentário identificando o documento. Comentários úteis dão uma idéia da fonte e do conteúdo do documento. Opções ou Ano: Organização Sobrepeso Não foi possível abrir o ficheiro de saída formato PDF Dolorosa Papular Fontes das Porções Frase senha expirou Senha Senha, de novo História Pregressa Paciente Paciente Paciente: %s (%s), Num: %s
 Padrão Conforme Plano Telefone Escolha &arquivos Pigmentado Lugar Plano Coloque uma pequena discrição do que
estava fazendo com o GNUmed: Digite a profissão principal do paciente.

Atualmente registrada:

%s (atualizado em %s) Por favor informe seu endereço de email. Por favor digite sua frase senha: Por Favor selecione um problema de saúde: Selecione uma pessoa da lista abaixo. Favor visitar  http://www.gnumed.org População CEP Apelido Prescrito para Prescrições Consultas anteriores Imprimir imprimir todos os items imprimir um único item Problema Procedimentos efectuados: %s Prossiga e tente uma ligação com o recém iniciado cleinet GNUmed. Proceda com o login. Programmer must override this text. Notas de Evolução Editor de Evolução ... Notas de evolução na consulta mais recente: Fazer episódio se tornar Problema de Saúde Púrpura Colocar legenda aqui. Colocar pergunta aqui. Quantidade Visualizador de pesquisa D Motivo da Consulta Conteúdo em bruto Reacção Tipo de reacção Motivo da interrupção. Detalhes da Reconsulta Recall or Review Recalls and Reviews Reconsulta/Revisão Referência Informação referente Referências Estado Relevante Atualizar Atualizar agenda pelo KOrganizer Memorizar e não voltar a perguntar. Remover a porção selecionada na lista acima. Será perguntado se deverá ser apagado fisicamente do disco. Remover o(s) item(s) selecionado(s) da lista. Removendo endereço Removendo canal de comunicações. Relatório Relatório não retornou dados. Relatórios reimprimir todos os items reimprimir item Pedido Resumo de pedidos Pedidos Solicitações geraram essa consulta Campos obrigatórios. Obrigatório: Uma assinatura curta para esse membro da equipe, como as iniciais. De preferência não mais de 5 caracteres. Obrigatório: Digite o provedor a ser notificado sobre o novo documento para que seja revisado. Na maioria dos casos esse é o médico principal do paciente. Obrigatório: A conta do banco de dados desse membro da equipe. Deverá existir no banco de dados. Obrigatório: A função dentro da organização desse membro da equipe. Atualmente a única função suportada é "médico". Obrigatório: Episódio primário sob o qual esse documento será listado. Obrigatório: Tipo do documento. Necessário: sobrenome Restaurar valores Resolvendo conflito "dois episódios abertos". Resultados Revisão Janela de revisão Revisões por outros Função Execute a pesquise e apresente os resultados abaixo. Salvar guardar todos os items para impressão guardar item para impressão Salvar uma screenshot (imagem da tela atual) desse cliente GNUmed. Gravar e confirmar o estado da alergia Salvar os detalhes da consulta e todas as Notas de Evolução Salvar documento pronto Salvar o Prontuário como ... Grava os detalhes da alergia na área de edição como uma nova alergia ou como uma actualização da alergia seleccionada anteriomente. Guardar a alergia/intolerância na base de dados Salvar a Nota de Evolução atual. Salvar os detalhes da consulta Salvar a definição de relatório. Salvar sua revisão Salvar Salvar definição de relatório [%s]. Salvando dados do contato Escama Esquema Engloba Bloco de Notas Screenshot Script tem que ter acesso de leitura apenas para o utilizador que o invoca (máscara "0600"): [%s]. Script tem que ter acesso de leitura para o utilizador que o invoca: [%s]. Script não pode ser uma ligação (link): [%s] Pesquisa Pesquisar dados do Prontuário do paciente ativo Selecione Selecione um endereço por CEP ou nome da rua. Selecione o teste / exame desejado do menu 'Arquivo' Selecione o modelo de carta ou formulário. Selecione ou defina o local em que o paciente aguarda. Selecione pessoa da lista Selecione a descrição.
 Selecione o episódio para o qual você quer mover a descrição Selecione os episódios nos quais está interessado ... Selecione os problemas de saúde nos quais está interessado ... Selecione a pessoa marcada na lista acima. Selecione o tipo de endereço aqui. Selecione o tipo de endereço. Selecione o tipo do canal de comunicações. Selecciona o tipo de consulta Selecione a urgência desse paciente.

O padrão é 0. A escala é de 0-10.
Valores mais altos informam maior urgência. Selecione se o paciente sofre de alergias conhecidas. Seleccione se este paciente não tem alergias conhecidas. Seleccionar se a reacção é uma sensibilidade Seleccionar se a reacção é uma alergia Selecione se não há informações em se o paciente é alérgico ou não. Selecione se você acha que o resultado não é clinicamente significante. Selecione se você acha que o resultado é clinicamente relevante. Selecione se você acha que o resultado é normal não importando o que o laboratório / laudo diz. Selecione se você acha que os resultados são tecnicamente anormais - não importando o que o laboratório / laudo diz. Selecione se você concorda com a opinião atual de resultado clinicamente relevante. Selecione se você quer aceitar a decisão de taxar esse exame como tecnicamente anormal. Selecione se você quiser que esse atalho seja usado por todos os usuários GNUmed, a não ser que eles mesmos tenham definido sua própria expansão com esse mesmo atalho. Selecione se você quer manter esse paciente. Selecione se você quer que esse atalho seja usado apenas por você. Selecionar problema de saúde Enviar relatório Autor Enviando relatório de erro Sensibilidade Sensorial: Seq # Sequencia Set "%s" to <False>. Set "%s" to <True>. Definir &tradução Configurar a frase / senha Tempo que deve ser esperado para o OpenOffice após inicialização. Definido como: Show "kompendium.ch" drug database (online, Switzerland) Mostra uma página de links de conteúdo médico útil. Mostra informações sobre o banco de dados atual. Mostrar arquivo de Log Mostre o esquema do banco de dados no seu navegador. Mostra o episódio associado com esse documento. Selecione ou digite o nome de outro episódio a qual deseja associar esse documento. Assinar Dar visto nos resultados de exames e rever o status de todos os resultados selecionados. Significante Dar visto nos resultados de exames Local Nível da Pele Cartaz de Snellen Algumas instalações de rede não conseguem abrir 
documentos de tamanho arbitrário do banco de dados
(geralmente observado em versões antigas do Windows).

Assim sendo os documentos devem ser adquiridos em pedaços
e remontados pelo cliente.

Aqui pode-se determinar o tamanho (em Bytes) acima do qual 
o GNUmed vai adquirir os documentos em pedaços. Determinando
esse valor em 0 vai desabilitar o protocolo de obtenção de pedaços. Organizar documentos por Organizar documentos pelo episódio a qual pertencem. Organizar documentos por tipo. Organizar documentos mais recentes no topo da árvore. Organizar os documentos não revisados no topo da árvore. Episódio Atual Especialidade Começo Iniciar visualizador DICOM (%s) para CD-ROM (Raio X, TAC, RNM, etc). No Windows apenas insira o CD. Iniciar nova consulta para o paciente ativo *agora* ! Começa uma nova consulta. Se houver alterações na consulta corrente será pedido para as gravar ou rejeitar. Iniciar nova consulta. A anterior já existente será fechada. Iniciar Nova Iniciar nova Consulta Começar novamente (descarta os dados atuais). Começou Iniciado nova consulta para o paciente ativo. Estado Rua Informação da rua Intensidade Subjectivo Substância Complemento Bairro Bairro: Súbita Resumo Exposto ao sol Superfície Alvo Episódio Selecionado. Tecnicamente Anormal Modelo Tipo de modelo Exame Testar console de erros Resultados dos exames Texto Documento de Texto O endereço onde buscar informações sobre a versão. O comando [%s] não foi encontrado. Isso pode, ou não, ser um problema. O paciente correntemente seleccionado é: Conta na base dados para este utilizador.

A conta será criada na base de dados com direitos de acesso específicos. As restrições de privacidade estão correntemente associadas no código ao grupo "gm-doctors" do PostgreSQL.

Pode utilizar o nome de uma conta existente, mas para já não poderá ser utlizada por mais nenhum utilizador do GNUmed. O banco de dados atualmente suporta os
idiomas [%s]. No entanto pode-se adicionar traduções
para coisas como tipos de documentos ou consultas.

Deseja forçar o idioma para [%s] ? A senha de caesso à base de dados deve ser inserida de novo de modo a detectar eventuais erros de digitação. O banco de dados será desligado para manutenção 
em alguns minutos.

Para não perder dados você
deverá salvar seu trabalho atual e sair do
cliente GNUmed.
 O banco de dados será desligado para manutenção em alguns minutos. O banco de dados ao qual você está conectado está
marcado como "em produção com controle de acesso".

Você indicou que quer incluir o arquivo de log no relatório
de erro. Embora isso ajude na hora de consertar o erro,
o log pode conter informações do paciente que não devem ser
enviadas sem se retirar a identificação do mesmo.


Confirme que mesmo assim quer incluir o log ! O médico que deverá avaliar e dar vistos nesse exame. O formato de documentos na língua local. Tipo de documento em que serão salvos formulários produzidos com esse modelo. O formato de documento, geralmente em Inglês. O e-mail do usuário que será usado para mandar notícias de erro e etc. O episódio em que essa descrição atualmente se encontra. O episódio para o qual você quer mover a desccrição. As pessoas abaixo gentilmente contribuíram para o GNUmed.
Por favor escreva para <gnumed-devel@gnu.org> para ter
sua contribuição registrada nessa lista ou seu nome removido
se esse for seu desejo, digamos, por razões de segurança.

Note que a lista é mostrada alfabeticamente, sobrenome e
depois nome. Pode ser que apareçam só os e-mails, nesse caso
será considerado a primeira letra do nome do usuário.

%s O atalho que você queira que retorne essa expressão.

Tente evitar palavras e abreviações do seu dia-a-dia pois você poderá querer usá-las verbatim. Ao invés, prefixe ou sufixe suas palavras chaves com pontuações, digamos, "*" ou "$". Tente não sufixar palavras com separadores comuns como:

     ! ? . , : ; ) ] } / ' " ESPAÇO TAB QUBRADELINHA

pois esses símbolos são necessários para detectar quando usar a expansão do atalho. O pedido de exames já existe, mas pertence a outro paciente. O valor inferior de normalidade desse exame. O valor ideal inferior desse exame para esse paciente. O problema médico a qual esse exame pertence. O novo episódio:

 "%s"

continuará não associado mesmo sendo iniciado
de dentro do problema de saúde:

 "%s" O Arquivo original (se houver). Somente pode ser editado se apenas uma porção do documento for pedida. Senha para a nova conta na base dados. Entrada de dados não será mostrada. A pesquisa falhou. O resultado do exame. Aceita-se números e letras (valor alfanumérico). I índice da sequência ou o número da página. Se invocado de um documento será sempre invocado a primeira página. Os relógios do servidor e do cliente estão
com diferença de mais de %s minutos!

Você deve acertar as configurações de horário
antes de usar esse banco de dados com esse cliente

Pode ser que voccê tenha que contactar seu
administrador para conseguir ajuda. Os relógios do servidor e do cliente estão
com diferença de mais de %s minutos!

Você deve acertar as configurações de horário
ou o horário de entrada de dados clínicos poderá estar errado.

Pode ser que voccê tenha que contactar seu
administrador para conseguir ajuda. O tipo de código externo desta droga, se houver. Tipo do modelo. Uso provável desse modelo. As unidades de medida padrão do exame. O valor superior de normalidade desse exame. O gráfico para o wxPython, ainda não foi inicializado! Já existe um problema de saúde

 %s

O quer quer fazer? There are no encounters for this episode. Não há ocorrências para este problema de saúde. Não há narrativa para esse episódio nessa consulta. Não há informação de versão de:

%s Esses pacientes estão esperando.

Clique duas vezes para ativar (registro continuará na lista). Mostra o estado alérgico atual salvado no banco de dados. Documenta o porquê da consulta.

Pode ser devido a uma busca direta de um paciente ou pode ter sido necessária por outras razões. Normalmente essa razão é coletada na recepção e colocada nos comentários da lista de espera. Não é necessariamente a real razão pela consulta do paciente.

Também chamada de Motivo da Consulta. Esse campo lista as porções que pertencem ao documento atual. Esse texto não foi encriptado corretamente Lista os formatos de documentos disponíveis Mostra a lista de Problemas Ativos. Isso inclui consultas abertas assim como os problemas de saúde ativos. Mostra a lista das consultas anteriores Esse visto se aplica a TODOS os resultados atualmente selecionados.

Se você quer mudar quais resultados modificar deverá aumentar ou diminuir
a seleção de resultados. Resumo da definição ou da avaliação da consulta do ponto de vista médico. Repare que esse resumo inclui todos os problemas discutidos durante essa consulta. Isso enviará uma mensagem sobre o desligamento do banco de dados
a todos os clientes GNUmed conectados a esse banco de dados.

Quer enviar essa notificação ?
 Limiar Entre em erro para testar o console de erros. Dicas Título Para Para criar um endereço válido todos os campos devem ser preenchidos. Agenda de hoje do KOrganizer ... Total de vacinas registradas para a doença correspondente. Traduza caso contrário a internacionalização não vai funcionar correctamente Agente Tronco Tipo Formato (Inglês) Digite nome da Paologia Digite ou selecione uma cidade. Digite ou selecione um país Digite ou selecione um nome Digite ou selecione um sobrenome. Digite ou selecione uma rua Digite ou selecione um título. Note que isso se aplica a pessoa não a um nome em particular ! Digite ou selecione um apelido. Digite ou selecione a profissão Digite ou selecione uma descrição para esse episódio. Deverá ser um resumo do episódio da doença. Digite ou selecione um bairro. Tipo: URL UUID display Não foi possível mostrar o ficheiro:

 [%s]

O sistema não parece ter um visualizador
registado adequado a este tipo de ficheiro
 [%s] Impossível enviar o e-mail. Não foi possível contribuir o relatório [%s] para a comunidade GNUmed. Unattributed episodes Magreza Unidade: Unidade Padrão Desconhecido Destrava o mouse Destrava o mouse caso tenha ficado preso em modo de ampulheta. Erro não específico ao salvar dados na área de edição. 

Programador esqueceu de especificar erro
mensagem em [%s]. Atualizar Urgente Interface do usuário ... Manual do Usuário (www) Parâmetros do utilizador: Utilizador1 Utilizador2 Utilizador3 Nome de Usuário Vacinação Vacinas Vacinações Recomendadas (X = paciente está atualizado, 0 = paciente não está atualizado)  Vacinas: Vacinador Vacina Valor Valor [%s] não é válido para essa opção <%s> Verifique a que paciente este pedido de exames realmente pertence Verificando configurações do banco de dados Verificando a versão do banco de dados Versão Informação de versão carregada de:

 %s Veja o log Veja o arquivo log Ver a porção selecionada na lista acima. Veja a referência para os itens do menu na internet. Lista de espera Detalhes da fila de espera para esse paciente. Tempo de Espera Mensagem de Boas Vindas Porque o paciente veio consultar. Pode ser o Motivo da Consulta. Quando o GNUmed não consegue achar um servidor
OpenOffice ele tentará iniciar um automaticamente,
no entanto o OpenOffice precisa de tempo para iniciar.

Aqui pode-se definir quanto tempo o GNUmed deve esperar pelo OOo.
 Quando descoberto Quando um paciente é ativado o GNUmed pode
ser configurado para mudar para um plugin específico. 

Escolha o plugin desejado abaixo: Quando um paciente é ativado o GNUmed checa 
o seu prontuário para consultas sem dados.

Quando uma dessas consultas sem dados for mais
velha do que o que você definir aqui será removida. 

Para efetivamente impedir isso defina nessa opção 
um valor enorme e improvável.
 Quando um paciente é ativado o GNUmed checa a
proximidade do seu aniversário.

Se o aniversário cai no intervalo entre 
 "hoje %s <intervalo que você define aqui>"
o GNUmed o lembrará de um aniversário recente
ou eminente. Quando adicionando notas de evolução você quer
permitir que se abra vários novos episódios não 
associados para um paciente por vez? 

Isso pode ser benéfico por exemplo quando o paciente 
vem na primeira consulta com vários problemas de saúde
ou queixas sem relação umas com as outras. Quando checar por atualizações, considerar também a árvore em desenvolvimento também ? Quando foi que o estado alérgico foi confirmado pela última vez. Quando esse resultado foi realmente obtido. Geralmente é o mesmo que "coleta da amostra" ou "data do exame". Se esse documento retrata resultados anormais tecnicamente. Se esse documento retrata resultados clinicamente relevantes. Lembrar que tanto resultados normais quanto anormais podem ser clinicamente relevantes. É possível salvar documentos sem partes ? Checar automaticamente por atualizações ao iniciar. Com Exemplo Escrever uma carta para o paciente atual. Visualizador XDT campo XDT conteúdo do campo  XDT Sim Sim permita múltiplos episódios concomitantes Sim, apagar Sim procure automaticamente. Sim enviar o relatório de erro. Você está logado como %s%s.%s (%s). Conta BD <%s>. Introduzir um ficheiro xDT pela linha de comandos
Formato: --xdt-file=<ficheiro> Você deve selecionar um modelo de aquivo antes de salvar. Você tentou faze login como [%s] com a senha [%s].
Host: %s, DB: %s, Port: %s Quer associar o novo episódio: 

"%(new_epi_name)s" (%(new_epi_start)s - %(new_epi_end)s)

com o problema de saúde>

"%(issue_name)s"

Já exise um outro episódio aberto para esse problema de saúde:

 "%(old_epi_name)s" (%(old_epi_start)s - %(old_epi_end)s)

No entanto, só pode haver um episódio aberto
 para cada problema de saúde

Qual desses episódios quer fechar ? Seu relatório de erros será enviado para: 

%s

Tenha certeza que você já editou o arquivo de log para que não
envie informações potencialmente perigosas antes de mandar o relatório

Repare que enviar o relatório de erro pode demorar, dependendo de sua
velocidade de internet.
 Sua revisão Código postal CEP Zona [%s] não é um ficheiro legível activo todos os arquivos todos os arquivos (Win) já relatado animal qualquer parte do nome bilateral ambas rapaz botão_1 criança Relevante Clinicamente fechar episódio já existente "%s" fechar e mover episódio (novo) "%s" encerrado confidencial resultado corrigido d::day_abbreviation dD_keys_day Data de nascimento data de nascimento/óbito hora de verão activa definitivo descartar entrada e cancelar editar detalhes da consulta Exportar o Prontuário vacinações para esse paciente ainda não disponíveis episódio: %s erro ao buscar resultados de exames ainda não revistos erro com o marcador [%s] ID externa do paciente paciente externo (nome, sexo, data de nascimento) fatal feminino final terminado nome próprio nome completo mensagem de erro genérica mensagem informativa genérica mensagem de aviso genérica rapariga gm_ctl_client: iniciando cliente GNUmed escravo h::hour_abbreviation hH_keys_hour hdwmy (single character date offset triggers) problema de saúde: %s icon-future_mom icon_box icon_letter_A dentro de %d dia(s) - %s dentro de a %d semana(s) - %s inactivo incluir o arquivo de log no relatório de erro iniciar campos de digitação para nova entrada ID interna do paciente interval_format_tag::days::d interval_format_tag::months::m interval_format_tag::weeks::w interval_format_tag::years::y estado alérgico inválido [%s] kg a perder exame [%s], pedido [%s], esperada ligação ao paciente [%s], actualmente ligado ao paciente [%s] Última verificação: atualizado: %s
 esquerda provável abrir arquivo Carregando ficheiro xDT longo-prazo m::month_abbreviation mM_keys_month masculino em falta, relatado mais tarde mo::month_only_abbreviation Dados modificados brigatório um destinatário obrigatório selecionar endereço nome nome do ficheiro xDT nome, data de nascimento nome, sexo, data de nascimento noem: nome-apelido nomes:  apelido nome próprio nome: apelido-nome nomes: nome apelido nomes: nome-apelido, data de nascimento nomes: apelido, nome próprio nomes: apelido-nome, data de nascimento nomes: apelido-nome próprio, data de nascimento ndmy (single character date triggers) necessário pedido de exames antes de inserir resultado novo episódio não há hook especificado, favor reportar esse bug. Sem alergias (conhecidas) não há visualizador isntalado Clinicamente não relevante profissão episódio antigo em progresso somente documentos adicionados ou ordenado por marca entrada original parcial por doença pgAdmin III pgAdminIII:Navegue o banco de dados GNUmed no servidor PostgreSQL. preliminar ocupação principal do paciente falta mensagem de erro falta mensagem informativa programador se esqueceu de especificar mensagem de status falta mensagem de aviso relevante restaurar valores de entrada/digitação revisar documento direita neste momento (%s, %s) s::second_abbreviation salvar entrada no prontuário médico salvando dados clínicos curto-prazo significante subdivisão tab1 se responsabilizar tecnicamente anormal arquivos de texto hoje (%s) amanhã (%s) tipo unidades Alergia de origem desconhecida sexo desconhecido reação desconhecida formato de saída do resultado dos exames é desconhecido [%s] estado do teste desconhecido [%s] formato de saída das vacinações desconhecido [%s] até w::week_abbreviation wW_keys_week Ficheiros xDT visualizador xDT y::year_abbreviation yYaA_keys_year ontem (%s) 