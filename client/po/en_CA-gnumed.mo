��    �      T  �   �
        #   	     -     <     P     j     �     �     �     �      �          !     ?     ]     o     �     �     �  D   �     �  	   �            9    '   U     }     �  #   �     �     �     �  X   �  y   *  
   �     �  4   �     �  '     5   *  T   `  Z   �  N        _  -   z     �  -   �  2   �       �     
   �     �     �  	   �     �     �  *   �  -   )  0   W     �     �  
   �     �     �     �     �  	   �     �  *   �  	                  /     I  >   N  (   �     �     �     �     �  -   �  L   +  2   x      �  -   �  *   �  
   %  "   0  ]   S  B   �  C   �  7   8  )   p  ,   �  <   �       {        �     �  8   �     �     �     �     �                         2     9     J     V     d     x     �  '   �     �  5   �          "     (  
   1  	   <     F  +   K     w  -   �     �     �     �     �     �  \        b     p     u     �     �     �     �     �     �     �                8      K     l  %   �  *   �  $   �     �           $      ,      ;      C      O      X   
   k      v      �      �      �      �      �      �   z  �   #   c"     �"     �"     �"     �"     �"     �"     #     *#      <#     ]#     {#     �#     �#     �#     �#     �#     �#  D   	$     N$  	   W$     a$     n$  =  u$  '   �%     �%     �%  #   �%     	&     &     '&  X   /&  y   �&  
   '     '  4   $'     Y'  '   `'  5   �'  T   �'  Z   (  N   n(     �(  -   �(     )  -   )  2   F)     y)  �   })  
   �)     
*     *  	   1*     ;*     U*  *   \*  -   �*  0   �*     �*     �*  
   �*     +     +     1+     4+  	   8+     B+  *   E+  	   p+     z+     }+     �+     �+  >   �+  (   �+     ,     &,     9,     =,  -   [,  L   �,  2   �,      	-  -   *-  *   X-  
   �-  "   �-  ]   �-  B   .  C   R.  7   �.  )   �.  ,   �.  <   %/     b/  {   j/     �/     �/  8   0     A0     E0     L0     ]0     d0     r0     v0     |0     �0     �0     �0     �0     �0     �0     �0  '   1     /1  5   C1     y1     �1     �1  
   �1  	   �1     �1  +   �1     �1  -   �1     2     !2     32     F2     O2  \   c2     �2     �2     �2     �2     �2     �2     3     /3     @3     R3     c3      u3     �3      �3     �3  %   �3  *   4  $   24     W4     j4     �4     �4     �4     �4     �4     �4  
   �4     �4     �4     �4     5     5     (5     75         [   V       \   �       A   s                  �           |   �   �   B      �   �   W   Y   j       �   Q          �   =      �      p           +      k   %   _       ,   L   /           m   �          D      *       d   �   �   �   n   �   �      C       -   P   F             ?   �   5   �   G   g   '               4   �   ^   �   E   �   )   w   �          �   
   7       z   y      q   O   1       8       .   3   i          �       �       6   &   �   �           T           0      �          	          2      r   M   c       �   �   U   !   o   �   f          b   v   �   u   ;   �       }       I       `      ~      (              >   #   K          e      H       l   :   N      R   x       Z   {      X      J   @       �   ]   t   �           <   �   $          �   �                  �   "   S   9           �   h               �   a      your time: %s - %s  (@%s = %s%s)
  %s documents
  %s known problems
  ***** CONFIDENTIAL *****  - bug fixes and new features
  - bug fixes only
  - database upgrade required
  Most recent: %s - %s  Noted at age: %s  contributed to death of patient %d. of %s (last month) - a %s %d. of %s (next month) - a %s %d. of %s (this month) - a %s %s (%s last year) %s (%s next year) %s (%s this year) %s Bytes %s: encounter (%s) %sEpisode "%s" [%s]
%sEncounters: %s (%s - %s)
%sLast worked on: %s
 &Allergy &Definite &Sensitivity 1 Byte A GNUmed slave client has been started because
no running client could be found. You will now
have to enter your user name and password into
the login dialog as usual.

Switch to the GNUmed login dialog now and proceed
with the login procedure. Once GNUmed has started
up successfully switch back to this window.
 A new version of GNUmed is available.

 AOE Abort Abort and do NOT connect to GNUmed. Admin password Administrative Allergy An error occurred while Demographic record export
Please check the log file for details. An error occurred while retrieving a text
dump of the EMR for the active patient.

Please check the log file for details. Assessment Born   : %s, age: %s

 Can't connect to database without login information! Cancel Cancel editing the allergy/intolerance. Cannot connect to database without login information! Check if this reaction applies to this drug/generic only, not the entire drug class. Check this if this allergy/intolerance is known to exist for sure as opposed to suspected. Check this if you want GNUmed to remember your decision and not ask you again. Chronological EMR Journal
 Clear all fields or reset to database values. Connect to GNUmed Data of current patient to be displayed here. Displays drug classe(s) along with their ATC code. Doc Document signs and symptoms. If reaction is to a drug also document time of onset after drug administration (<24h, 24-72h, >72h). Documents: Documents: %s Edit Allergy/Intolerance Encounter Encounters: %s (%s - %s): Enlist Error retrieving encounters for episode
%s Error retrieving encounters for this episode. Error retrieving episodes for this health issue. Exported: %s
 Lab Lab result Last worked on: %s
 Measurements and Results: Mr Mrs Narrative No No encounters found for this health issue. Objective Ok Password, again Patient: %s (%s), No: %s
 Plan Proceed and try to connect to the newly started GNUmed client. Progress notes in most recent encounter: Put caption here. Put question here. RFE Remember and don't ask again. Save the allergy/intolerance in the database. Script must be readable by the calling user only (permissions "0600"): [%s]. Script must be readable by the calling user: [%s]. Script must not be a link: [%s]. Select this if the reaction is a sensitivity. Select this if the reaction is an allergy. Subjective The currently selected patient is: The database password must be typed again to enable double-checking to protect against typos. The lab request already exists but belongs to a different patient. The password for the new database account. Input will not be shown. The wxPython GUI framework hasn't been initialized yet! There are no encounters for this episode. There are no episodes for this health issue. Translate this or i18n into <en_EN> will not work properly ! Trigger Unable to display the file:

 [%s]

Your system does not seem to have a (working)
viewer registered for the file type
 [%s] Unattributed episodes Vaccination Verify which patient this lab request really belongs to. Yes active already reported animal any name part boy child clinically relevant closed corrected result dD_keys_day date of birth date of birth/death daylight savings time in effect episode     : %s error retrieving unreviewed lab results external patient ID external patient source (name, gender, date of birth) female final finished first name full name girl gm_ctl_client: starting slave GNUmed client hH_keys_hour hdwmy (single character date offset triggers) health issue: %s in %d day(s) - %s in %d week(s) - %s inactive internal patient ID lab [%s], request ID [%s], expected link with patient [%s], currently linked to patient [%s] mM_keys_month male missing, reported later modified entry name name, date of birth name, gender, date of birth name: first-last name: last, first name: last-first names: first last names: first-last, date of birth names: last, first names: last-first, date of birth names: last-first, dob ndmy (single character date triggers) need lab request when inserting lab result no hook specified, please report bug no known allergies not clinically relevant ongoing original entry partial preliminary relevant right now (%s, %s) today (%s) tomorrow (%s) unknown gender unknown reaction unknown test status [%s] wW_keys_week yYaA_keys_year yesterday (%s) Project-Id-Version: gnumed
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2010-02-09 16:17+0000
Last-Translator: Brett Alton <brett.jr.alton@gmail.com>
Language-Team: English (Canada) <en_CA@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Launchpad-Export-Date: 2011-09-14 21:59+0000
X-Generator: Launchpad (build 13921)
   your time: %s - %s  (@%s = %s%s)
  %s documents
  %s known problems
  ***** CONFIDENTIAL *****  - bug fixes and new features
  - bug fixes only
  - database upgrade required
  Most recent: %s - %s  Noted at age: %s  contributed to death of patient %d. of %s (last month) - a %s %d. of %s (next month) - a %s %d. of %s (this month) - a %s %s (%s last year) %s (%s next year) %s (%s this year) %s Bytes %s: encounter (%s) %sEpisode "%s" [%s]
%sEncounters: %s (%s - %s)
%sLast worked on: %s
 &Allergy &Definite &Sensitivity 1 Byte A GNUmed slave client has been started because
no running client could be found. You will now
have to enter your user name and password into
the login dialogue as usual.

Switch to the GNUmed login dialogue now and proceed
with the login procedure. Once GNUmed has started
up successfully switch back to this window.
 A new version of GNUmed is available.

 AOE Abort Abort and do NOT connect to GNUmed. Admin password Administrative Allergy An error occurred while Demographic record export
Please check the log file for details. An error occurred while retrieving a text
dump of the EMR for the active patient.

Please check the log file for details. Assessment Born   : %s, age: %s

 Can't connect to database without login information! Cancel Cancel editing the allergy/intolerance. Cannot connect to database without login information! Check if this reaction applies to this drug/generic only, not the entire drug class. Check this if this allergy/intolerance is known to exist for sure as opposed to suspected. Check this if you want GNUmed to remember your decision and not ask you again. Chronological EMR Journal
 Clear all fields or reset to database values. Connect to GNUmed Data of current patient to be displayed here. Displays drug classe(s) along with their ATC code. Doc Document signs and symptoms. If reaction is to a drug also document time of onset after drug administration (<24h, 24-72h, >72h). Documents: Documents: %s Edit Allergy/Intolerance Encounter Encounters: %s (%s - %s): Enlist Error retrieving encounters for episode
%s Error retrieving encounters for this episode. Error retrieving episodes for this health issue. Exported: %s
 Lab Lab result Last worked on: %s
 Measurements and Results: Mr Mrs Narrative No No encounters found for this health issue. Objective Ok Password, again Patient: %s (%s), No: %s
 Plan Proceed and try to connect to the newly started GNUmed client. Progress notes in most recent encounter: Put caption here. Put question here. RFE Remember and don't ask again. Save the allergy/intolerance in the database. Script must be readable by the calling user only (permissions "0600"): [%s]. Script must be readable by the calling user: [%s]. Script must not be a link: [%s]. Select this if the reaction is a sensitivity. Select this if the reaction is an allergy. Subjective The currently selected patient is: The database password must be typed again to enable double-checking to protect against typos. The lab request already exists but belongs to a different patient. The password for the new database account. Input will not be shown. The wxPython GUI framework hasn't been initialized yet! There are no encounters for this episode. There are no episodes for this health issue. Translate this or i18n into <en_CA> will not work properly ! Trigger Unable to display the file:

 [%s]

Your system does not seem to have a (working)
viewer registered for the file type
 [%s] Unattributed episodes Vaccination Verify which patient this lab request really belongs to. Yes active already reported animal any name part boy child clinically relevant closed corrected result dD_keys_day date of birth date of birth/death daylight savings time in effect episode     : %s error retrieving unreviewed lab results external patient ID external patient source (name, gender, date of birth) female final finished first name full name girl gm_ctl_client: starting slave GNUmed client hH_keys_hour hdwmy (single character date offset triggers) health issue: %s in %d day(s) - %s in %d week(s) - %s inactive internal patient ID lab [%s], request ID [%s], expected link with patient [%s], currently linked to patient [%s] mM_keys_month male missing, reported later modified entry name name, date of birth name, gender, date of birth name: first-last name: last, first name: last-first names: first last names: first-last, date of birth names: last, first names: last-first, date of birth names: last-first, dob ndmy (single character date triggers) need lab request when inserting lab result no hook specified, please report bug no known allergies not clinically relevant ongoing original entry partial preliminary relevant right now (%s, %s) today (%s) tomorrow (%s) unknown gender unknown reaction unknown test status [%s] wW_keys_week yYaA_keys_year yesterday (%s) 