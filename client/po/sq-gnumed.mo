��    �     t    �      @!  9   A!     {!  �   �!  �   4"  �   #  L   �#  #  H$  �   l%  V   &&  �   }&  i   	'  m   s'  #   �'     (     (     ,(     J(     _(  1   s(     �(     �(     �(     �(     �(  
   )     )     )     7)     M)  	   Z)     d)     |)     �)  /   �)     �)     �)     �)     �)     *     $*     5*     C*     V*     l*     ~*     �*     �*     �*     �*     �*      �*  .    +     /+     6+     Q+  5   b+     �+     �+     �+     �+     �+     �+     ,     ,     ,  '   ,  	   >,  	   H,     R,     X,     n,     �,     �,  	   �,  
   �,     �,  1   �,     �,      -     -     0-     I-     b-     x-     �-     �-     �-     �-     �-     �-     �-     �-     �-     .     .     .     .     3.     J.     ].     t.  .   x.  +   �.     �.     �.     �.     �.     �.     	/     /     %/     =/     [/     z/     �/  
   �/  "   �/  0   �/  	   0     0  F   0  B   `0  >   �0     �0     �0     �0     �0     1     1  
   "1     -1     G1     X1     e1     u1     �1  .   �1     �1     �1     �1     �1     �1     �1     2     2  	    2     *2      H2  
   i2     t2     �2  d   �2     3     3     3  !   #3     E3     J3     [3     k3     y3  
   �3     �3  
   �3     �3     �3     �3  
   �3     �3  "   4     )4  0   /4  (   `4  <   �4  B   �4  6   	5  7   @5     x5     �5  '   �5  =   �5  1   �5     (6     96     I6     W6     f6     y6     �6     �6     �6     �6  *   �6     7     7     *7  
   27     =7  	   L7     V7     _7     k7  
   s7     ~7     �7     �7     �7     �7     �7  
   �7  
   �7     �7     �7     8     8     $8  _   28     �8  	   �8  <   �8     �8     �8  0   �8  
   09  
   ;9     F9     ]9     i9     o9     |9     �9     �9     �9  �   �9     ?:     F:     J:     N:     h:  
  �:     �;     �;     �;     �;     �;     �;     �;     �;  !   �;  *   �;      <     ;<     O<     _<     c<     y<     �<     �<     �<     �<  	   �<  	   �<  $   �<  )   �<     !=  !   7=     Y=     h=     p=     w=     �=     �=     �=     �=  (   �=     �=     �=     >     >     >     &>  	   :>     D>     H>     O>     V>     n>     z>  2   �>     �>     �>     �>     �>  	   �>     �>     ?     ?  	   ?  =   &?     d?     {?     ?     �?     �?     �?     �?  B   �?  &   �?  ,   @  �   ;@  	   �@     �@     �@     �@     A     A     /A     MA  '   lA  ;   �A     �A  #   �A     B     2B     HB     XB     oB     |B  8   �B     �B     �B     �B     �B     �B     �B  ;   �B     9C  -   SC  ;   �C     �C  .   �C     �C     �C     �C     D     D  	   )D     3D     GD     ND     SD     WD     \D     eD  '   vD     �D  5   �D     �D     �D     E     $E     <E     ME     RE     XE  \   aE     �E     �E  	   �E     �E     �E     �E  *   �E     "F     5F     EF     SF     kF     sF     �F  *   �F  )   �F  $   �F  	   G     G  
   G  	   (G     2G     5G  
   ;G     FG     LG     bG  '   sG  -  �G  ;   �H     I  �   I    �I  �   �J  L   �K  ?  �K  �   9M  h   �M  �   eN  o   �N  �   aO  "   �O     P     +P     :P     SP     fP  =   P     �P     �P     �P     Q     Q     )Q     5Q     EQ     `Q     vQ     �Q     �Q     �Q     �Q  -   �Q     R     R     R     +R     FR     ]R     oR     |R     �R     �R     �R     �R     �R     �R     �R     S  "   (S  :   KS     �S     �S     �S  6   �S     �S     T     T     )T  $   :T     _T     eT     kT     qT  '   yT  	   �T  	   �T     �T     �T     �T     �T  
   �T     �T     �T     U  3   U     JU     jU     �U     �U     �U     �U      V     V     +V  	   9V     CV     GV     cV     qV  !   �V     �V     �V     �V     �V     �V     �V     �V     W     %W  1   +W  .   ]W     �W     �W     �W     �W     �W     �W     �W  )   �W     X     .X  #   NX  "   rX     �X  .   �X  D   �X  	   Y     Y  b   &Y  H   �Y  ;   �Y     Z     Z     %Z     -Z  $   6Z     [Z  
   cZ      nZ     �Z     �Z     �Z     �Z     �Z  +   �Z     [     1[     >[     D[     L[     R[  
   a[     l[     y[  '   �[  +   �[  
   �[     �[      �[  }   \     �\     �\  
   �\  (   �\     �\     �\     �\     ]     ]     2]     D]     X]     m]     y]     �]     �]     �]  #   �]     �]  7   �]  =   2^  J   p^  R   �^  Q   _  ]   `_     �_     �_  -   �_  F   	`  6   P`     �`     �`     �`     �`     �`  
   �`     �`  !   �`     a  "   1a  9   Ta  &   �a     �a     �a     �a     �a     �a     	b     b     (b  
   1b     <b     Cb     Ib     [b     nb     �b     �b     �b     �b      �b     �b     �b     �b  |   c     �c     �c  H   �c     �c     �c  ;   d  
   Gd     Rd     ^d     }d     �d     �d     �d     �d     �d     �d  �   �d  
   �e     �e     �e     �e     �e  	  �e     �f     �f     g     g     g      g     %g     +g  +   0g  1   \g  &   �g     �g     �g     �g     �g     h     h      h     ,h  "   >h     ah     mh  (   yh  <   �h     �h  !   �h     i     &i  	   ,i     6i     Bi     Ri     Yi     ai  1   yi     �i     �i     �i     �i     �i  "   �i  	   j     j     &j     5j     Aj     ]j     lj  8   }j  "   �j     �j     �j     �j     k     k     k     0k  	   6k  4   @k     uk     �k     �k     �k     �k     �k     �k  @   �k  &   l  )   +l  �   Ul     m     m     !m     .m     Em     Lm  ,   am  (   �m  9   �m  I   �m  '   ;n  +   cn  7   �n     �n     �n     �n  
   o     o  7   %o  	   ]o     go     lo     }o  "   �o     �o  8   �o     �o  ,   p  >   :p     yp  :   }p     �p     �p     �p     �p     �p     �p     q     q  
   q     )q     /q     5q     Bq  @   Oq     �q  8   �q  
   �q     �q     r  #   +r     Or     dr     ir     nr  r   vr     �r     �r  	   s     s     s     s  M   2s     �s     �s     �s     �s     �s     �s     �s  ;   t  :   Ct  6   ~t     �t     �t     �t     �t     �t     �t     �t     �t     u     u  0   2u     +  �         a        ,   ]      W  d   W   D   �               �       Z  9                    �   |  o          �      `       �       �   g   a   K                   	   �     �   �       f   "  �   �          �              �   :  7   �   �       
  *       *  #   E      s  �   A      �   ?  (    �   ]           -      �           T  %  +           �                  �   5           �   g          ;   `      R      D          Y   $       �     q  �          5   �       |   �       �   �       %           U   �      [       �   4        j   P  F       �   x      
   �   �  �       )  �   O      u  i  E  �   �   @   ?   �   {           �          �   �   �   �   r   �   �  �   �   K       G  �   V               J  !   _   I  &     1  �   �   �   e  �   �   p       �   <         '   e           i   �   �      �     �   �   �   �   v   �       Q  �             6           �   ~                >                    &  d  �       [  0   \      �   �   c      �   �   �   h   X      �     �          "   �   �   x       v      �   �      y   2      L   �       �   �   �       (   �   �     �           /  7  q   C  �   �       @    I   ^     A   �       m       T   l  H   8         =           m  =   �           �     	  }      �   �  �  B   V  �   �          P   �   �   3  r         !  /     �   Q   �       $  �          p  �   t   z   J      k   s       S          �   U  ^  k  X           �      �   �                             N       M  �   O   �   ,  Y        2       �       �          �       �       M         o   u         �      :       j  C           �  �   '  �          6  �   N  �   h          #  n     �              0  n   l   �   ~       )   �     �   �       _  �   ;  {      �       S   y  L      w  H  -   1   >     8       �      �         �   G       �   �   �   B  �   �   �   9          t    �   .    Z      �  �       F      �   <           3   �   �   z  �   w   c   b  }       \   .   b       R           �   f  4    

Please consult the error log for all the gory details ! 
 On bill: %s 
Categories are used to group events. An event can only belong to one category. All events that belong to the same category are displayed with the same background color.
 
Categories can be managed in the *Edit Categories* dialog (*Timeline* > *Edit Categories*). To edit an existing category, double click on it.

The visibility of categories can also be edited in the sidebar (*View* > *Sidebar*).
 
If you have more questions about Timeline, or if you want to get in contact with users and developers of Timeline, send an email to the user mailing list: <thetimelineproj-user@lists.sourceforge.net>. (Please use English.)
 
No. The events will still be there but they will not belong to a category.
 
The *Create Event* dialog can be opened in the following ways:

- Select *Timeline* - *Create Event* from the menu.
- Double click with the *left* mouse button on the timeline.
- Press the *Ctrl* key, thereafter hold *left* mouse button down on the timeline, drag the mouse and release it.
 
The date data object used does not support week numbers for weeks that start on Sunday at present.  We plan on using a different date object that will support this in future versions.
 
There is no save button. Timeline will automatically save your data whenever needed.
 
Timeline is developed and translated by volunteers. If you would like to contribute translations you are very much welcome to contact us.
 
To delete an event, select it and press the *Del* key. Multiple events can be deleted at the same time.
 
To select an event, click on it. To select multiple events, hold down the *Ctrl* key while clicking events.
   your time: %s - %s  (@%s = %s%s)
  %s active medications
  %s documents
  %s encounters from %s to %s
  %s hospitalizations  %s known problems
  %s known problems, clinically relevant thereof:
  %s of units: %s
  %s performed procedures  %s test results
  (last confirmed %s)  (long-term)  (ongoing)  (short-term)  ***** CONFIDENTIAL *****  ATC (substance): %s
  Advice: %s
  Aim: %s
  Amount multiplier: %s
  Charge date: %s  Closed: %s
  Created during encounter: %s (%s - %s)   [#%s]  Details: %s
  Discontinued %s
  Episode: %s
  External reference: %s
  Health issue: %s
  Invoice ID: %s
  Invoice: %s
  Items billed: %s
  Most recent: %s - %s  Noted at age: %s  Patient: #%s
  Reason: %s
  Receiver: #%s
  Regimen: %s
  Started %s%s%s
  VAT: does not apply
  contributed to death of patient %d Events not duplicated due to missing dates. %s ago %s encounter(s) (%s - %s): %s events hidden %s other episodes touched upon during this encounter: %s parts %s: %s by %.8s (v%s)
%s %s: encounter (%s) &Balloons on hover &Duplicate Selected Event... &Edit &File &Help &Legend &Measure Distance between two Events... &Navigate &Timeline &View *does* have allergies , most recently:
%s
 1 part 1-period 10-period 100-period 1000-period <%s>: unknown diagnostic certainty classification <channel> cannot be empty <issuer> cannot be empty <name> cannot be empty <number> cannot be empty <street> cannot be empty <urb> cannot be empty <zip> cannot be empty ?ongoing ?short-term A: Sign AOE Active billable item Add Category Add Container Add more events after this one Add new Add... Aim Alert Allergies and Intolerances
 Allergies/Intolerances Allergy detail: %s Amount per dose: %s %s Apr Are you sure you want to delete category '%s'? Are you sure you want to delete this event? Aug B: Cluster of signs Backward Battery: Billed item Both C: Syndromic diagnosis Can't move locked event Can't scroll more to the left Can't scroll more to the right Can't zoom deeper than 5 Cannot run [arriba] ! Categories Category name '%s' already in use. Category name '%s' not valid. Must be non-empty. Category: Center Click on it.

Hold down Ctrl while clicking events to select multiple. Clinical data generated during encounters under this health issue: Clinical data generated during encounters within this episode: Close Closed bill Color: Concepts Connecting to backend Contact Container: Could not find page '%s'. Create &Event... Create Event Create Timeline Create a new timeline Create event Created during encounter: %s (%s - %s)   [#%s] D: Scientific diagnosis Date && Time Day Dec Delete Delete event Description Device(%s): Direction Displayed period must be > 0. Distance between selected events Documents: Documents: %s Double click on an event. Double click somewhere on the timeline.

Hold down Ctrl while dragging the mouse to select a period. Drug Duplicate Event Duplicate... ERROR: unknown allergy state [%s] Edit Edit &Categories Edit Categories Edit Category Edit Container Edit Event Edit categories Edit event Edit... Encounters: %s (%s - %s): End must be > Start Ends today Episode %s%s%s   [#%s] Episodes: %s (most recent: %s%s%s) Error Error retrieving episodes for this health issue. Error running gnuplot. Cannot plot data. Error running pdflatex. Cannot turn LaTeX template into PDF. Error running pdftk. Cannot extract fields from PDF form template. Error running pdftk. Cannot fill in PDF form template. Error running pdftk. Cannot flatten filled in PDF form. Event Properties Events Events are overlapping or distance is 0 Events belonging to '%s' will no longer belong to a category. Events belonging to '%s' will now belong to '%s'. Exit the program Export to Image Export to SVG Family History Family History: %s Features Feb Field '%s' can't be empty. File '%s' does not exist. File '%s' exists. Overwrite? First select me and then drag the handles. First time using Timeline? Fit Century Fit Day Fit Decade Fit Millennium Fit Month Fit Year Font Color: Forward Frequency: Fri Fuzzy General Getting started tutorial Go back one page Go forward one page Go to Date Go to Time Go to home page Health Issue %s%s%s%s   [#%s] Health issue Help Help contents Hold down Ctrl while scrolling the mouse wheel.

Hold down Shift while dragging with the mouse. Hospitalizations: %s Hover me! Hovering events with a triangle shows the event description. Icon Image files Images will be scaled to fit inside a %ix%i box. Impedance: Implanted: Inactive billable item Information Intro Invalid date Jan Jul Jun Last worked on: %s
 Left click somewhere on the timeline and start dragging.

You can also use the mouse wheel.

You can also middle click with the mouse to center around that point. Locked Mar May Measurements and Results: Measurements and Results: %s Medical problems: %(problems)s
Total encounters: %(encounters)s
Total EMR entries: %(items)s
Active medications: %(active_drugs)s
Documents: %(documents)s
Test results: %(results)s
Hospitalizations: %(stays)s
Procedures: %(procedures)s
Vaccinations: %(vaccinations)s Medication history Medication list Mon Monday Month Name Name: New No [arriba] result found in [%s]. No encounters found for this health issue. No test results to format. No timeline opened. No timeline set Nov Number of duplicates: Occupations Oct Open &Recent Open Timeline Open an existing timeline Open bill Open item Open most recent timeline on startup Opening timeline instead of creating new. Organization: %s (%s) PDF output file cannot be opened. Page not found Parent: Period Preferences Preparation: %s
 Print Problem Procedures performed: %s Progress notes in most recent encounter: Question Questions and answers RFE Regimen / Advice Related pages Resize and move me! SVG files Sat Saving Scroll Search results for '%s' Select Icon Select event Select events to be deleted and press the Del key. Select region to zoom into Selecting events Sensing: Sep Show time Status Sticky Balloon Strength Substance Substance intake entry (%s, %s)   [#%s]                     
 Substance: %s   [#%s]
 Sun Sunday Tasks Test results Text Text: The lab request already exists but belongs to a different patient. The specified timeline already exists. There are no episodes for this health issue. This timeline is stored in memory and modifications to it will not be persisted between sessions.

Choose File/New/File Timeline to create a timeline that is saved on disk. Threshold Thu Timeline Timeline files Tue Unable to copy to clipboard. Unable to open timeline '%s'. Unable to read from file '%s'. Unable to read timeline data from '%s'. Unable to save timeline data to '%s'. File left unmodified. Unable to take backup to '%s'. Unable to write configuration file. Unable to write timeline data. Unattributed episodes Unknown format. Use inertial scrolling Vaccinations Vaccinations: Verify which patient this lab request really belongs to. Wed Week Week start on: Welcome Welcome to Timeline When: Where do the week numbers go if I start my weeks on Sunday? Where is the save button? Why is Timeline not available in my language? Will associated events be deleted when I delete a category? Year You can't change time when the Event is locked Zoom active active, needs check approved assumed active bilateral clinically relevant closed data day days definite episode     : %s error retrieving unreviewed lab results error with placeholder [%s] external patient source (name, gender, date of birth) finished generic error message generic info message generic warning message health issue: %s hour hours inactive lab [%s], request ID [%s], expected link with patient [%s], currently linked to patient [%s] last check: left long-term minute minutes modified entry need lab request when inserting lab result no known allergies none associated not available not clinically relevant ongoing ordered by brand original entry programmer forgot to specify error message programmer forgot to specify info message programmer forgot to specify warning read-only right short-term suspected to today unapproved units unknown allergy state unknown reaction unknown test results output format [%s] Project-Id-Version: gnumed
Report-Msgid-Bugs-To: 
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Generated-By: pygettext.py 1.5
 

Ju lutem konsultoni loget e gabimeve per detajet e plota! 
 ne faturen: %s 
Kategorite perdoren per te grupuar ngjarjet. Nje ngjarje mund te jete pjese e vetem nje katerogie. Te gjitha ngjarjet qe i perkasin nje kategorie afishohen me te njejten ngjyre ne sfond.
 
Kategorite mund te menaxhohen ne dialogun *Modifiko Kategorite* (*Linje kohore*>*Modifiko kategorite*). Per te modifikuar nje kategori egzistuese, klikoni dy here mbi te.

Shikueshmeria e kategorise mundet te modifikohet ne panelin anesor (*Shiko*>*Paneli anesor*).
 
Nese keni pyetje te tjere mbi Linjen Kohore apo nese doni te kontaktoni programuesit e Linjes Kohore, dergonie nje email tek: thetimelineproj-user@lists.sourceforge.net. (Ju lutem shkruani ne Anglisht).
 
Jo. Ngjarjet do te jene po aty por, nuk do ti perkasin me asnje kategorie.
 
Dialogu i *Krijo Ngjarje* mund te hapet ne keto menyra:

- Zgjidhni *Linje kohore* - *Krijo Ngjarje* nga menuja.
- Klikoni dy here me butonin *e majte* te mausit mbi linjen kohore.
- Shtypni butonin *Ctrl*, me pas mbani te shtypur butonin *e majte* te mausit mbi linjen kohore, zvarriteni mausin dhe me pas leshojeni.
 
Aktualisht objekti i dates se perdorur nuk i suporton numrat e javes per javet qe fillojne me te Djelen. Ne te ardhmen kemi ne plan te perdorim nje objekt tjeter per daten qe ti sportoje keto.
 
Nuk ka nje buton per ruajtjen. Linja kohore do ti ruaje automatikisht te dhenat atehere kur nevojitet.
 
Linja Kohore programohet dhe perkthehet nga vullnetare. Nese ju doni te kontriboni ne perkthim jeni shume te mirepritur te na kontaktoni.
 
Per te fshire nje ngjarje,zgjidheni ate dhe shtypni butonin *Del*. Disa ngjarje mund te fshihen njekohesisht.
 
Per te perzgjedhur nje ngjarje, klikoni mbi te. Per te perzgjedhur disa ngjarje, mbani shtypur butonin *Ctrl* dhe nderkohe klikoni mbi ngjarjet.
   koha juaj: %s - %s (@%s = %s%s)
  %s mjekime aktive
  %s dokumente
  %s takime nga %s ne %s
  %s hospitalizaime  %s probleme te njohura
  %s probleme te njohura, klinikalisht te qenesishme prandaj:
  %s te njesise: %s
  %s procedura te kryera  %s rezultate te testit
  (konfirmimi i fundit %s)  (afatgjate)  (ne vijim)  (afatshkurter)  ***** KONFIDENCIALE *****  ATC (substance): %s
  Keshille: %s
  Qellim: %s
  Shumefishimi i shumes: %s
  Dita e faturimit: %s  E mbyllur:%s
  E krijuar gjate takimit : %s (%s - %s) [#%s]  Detaje:%s
  I nderprere %s
  Episod: %s
  Reference nga jashte: %s
  Problem shendeti: %s
  ID e fatures:%s
  Fatura: %s
  Artikujt e faturuar: %s
  Me i fundit: %s - %s  E vene re ne moshen : %s  Pacienti: #%s
  Arsye: %s
  Pritesi: #%s
  Regjim: %s
  Filluar %s%s%s
  TVSH: e pa aplikueshme
  kontribuoi ne vdekjen e pacientit %d Ngjarje nuk u dyfishuan per shkak te mungeses se dates. %s më parë %s takim(e) (%s - %s): %s ngjarje te fshehura %s episode te tjera te modifikuara gjate ketij takimi: %s pjese %s: %s by %.8s (v%s)
%s %s: takime (%s) &Balloons pezull &Duplicate ngjarjen e perzgjedhur... &Edit &File &Help &Legend &Measure distanca midis dy ngjarjeve... &Navigate &Timeline &View *ka* alergji , me te fundit:
%s
 1 pjese 1-periudhe 10-periudhe 100-periudhe 1000-periudhe <%s>: klasifikim i panjohur per siguria e diagnozes <channel> nuk mund te jete bosh <issuer> nuk mund te jete bosh <name> nuk mund te jete bosh <number> nuk mund te jete bosh <street> nuk mund te jete bosh <urb> nuk mund te jete bosh <zip> nuk mund te jete bosh ?ne vazhdimesi ?afatshkurter A: Shenje AOE Produkt i faturueshem aktiv Shto kategori Shto kontenier Shtoni me shume ngjarje pas kesaj Shto të re Shto... Qellim Njoftim Alegjite dhe intolerancat
 Alergjite/intolerancat Detajet e alergjise: %s Sasia per doze: %s %s Prill Jeni i sigurte se done te fshini kategorine '%s'? Jeni i sigurte se doni ta fshini kete ngjarje? Gusht B: Grumbull shenjash Prapa Bateri: Produkt i faturuar Të dyja C: Diganoze sindrome Nuk mund te leviz nje ngjarje te bllokuar Nuk mund te zvarris me majtas Nuk mund te zvarris me djathtas Nuk mund te zmadhohet me shume se 5 Nuk mund te egzekutohet [arriba] ! Kategoritë Emri i kategorise '%s' eshte perdorur me pare. Emri i kategorise '%s' nuk eshte i vlefshem. Nuk duhet te jete bosh. Kategori: Qender Klikoni mbi te.
Mbani te shtypur butonin Ctrl ndersa klikoni ngjarjet per te zgjedhur disa syresh. Te dhena klinike te gjeneruara gjate takimeve per kete ceshtje shendeti: Te dhenat klinike te gjeneruara gjate brenda ketij episodi: Mbyll Fature e mbyllur Ngjyra: Koncepte Duke u lidhur me panelin e programit Kontakt Kontenier: Faqja  '%s' nuk mund te gjindet. Krijo &Event... Krijo Ngjarje Krijo Linje Kohore Krijo nje linje kohore te re Krijo nje ngjarje E krijuar gjate tekimit: %s (%s - %s) [#%s] D: Diagnoze shkencore Data && Koha Ditë Dhjetor Fshij Fshini ngjarje Përshkrim Pajisja(%s): Drejtim Periudha e afishuar duhet te jete  > 0. Distanca ndermjet ngjarjeve te perzgjedhura Dokumenta: Dokumente: %s Klikoni dy here mbi nje ngjarje. Klikoni dy here diku ne linjen kohore.
Mbani te shtypur butonin Ctrl ndersa zvarrisni mausin per te perzgjedhur nje periudhe. Ilac Ngjarje duplikate Dyfisho... GABIM: gjendja alergjike [%s] e panjohur Modifiko Modifiko &Categories Modifiko Kategoritë Modifiko kategori Modifiko Kontenierin Modifiko Ngjarjen Modifiko kategorite Modifiko nje ngjarje Modifiko... Takime: %s (%s - %s): Fundi duhet te jete > Fillimi Perfundon sot Episodi %s%s%s [#%s] Episodet: %s (me te fundit: %s%s%s) Gabim Gabim ne marrjen e episodeve per kete ceshtje shendeti. Gabim ne egzekutim me gnuplot. Nuk mund te afishoj te dhenat. Gabim me egzekutimin e pdflatex. Num mund te kthej templaten LaTeX ne PDF. Gabim ne egzekutimin e pdftk. Nuk mund te ekstraktoj fushat nga templata e PDF-se. Gabim ne egzekutimin e pdftk. Nuk mund te plotesohen forma e templates se PDF-se. Gabim ne egzekutimin e pdftk. Nuk mund te rrafshoj fushat e plotesuar te templates se PDF-se. Vecorite e ngjarjes Ngjarjet Ngjarjet janembivendosur ose distanca eshte 0 Ngjarjet qe i perkasin '%s' nuk do te jene me pjese e asnje kategorie. Ngjarjet qe i perkisnin '%s' tani do ti perkasin '%s'. Dil nga programi Eksporto ne Imazh Eksporto ne SVG Historiku Familjar Historia e familjes: %s Veçoritë Shkurt Fusha '%s' nuk mund te jete bosh. File '%s' nuk egziston. File '%s' ekziston. Ta ri-shkruaj? Ne fillim me zgjidh dhe me pas zvarrit mbajteset anesore. Hera e pare qe perdorni linjen kohore? Perputh Shekullin Perputh Diten Perputh Dekaden Perputh Mijevjecarin Perputh Muajin Perputh Vitin Ngjyra e fontit: Përpara Frekuenca: Premte Lemsh Të Përgjithshme Manuali i fillimit Shko nje faqe pas Shko nje faqe para Shko ne daten Shkoni ne kohen Shko ne faqen meme Ceshtja shendetit %s%s%s%s [#%s] Ceshtje shendeti Ndihmë Përmbajtja e ndihmës Mbani te shtypur butonin Ctrl ndersa rreshkisni me rroten e mausit.
Mbani te shtypur butonin Shift ndersa zvarrisni me maus. Hospitalizime: %s Me pezullo! Pezullimi i nje ngjarje me  trekëndësh tregon përshkrimin e ngjarjes. Ikone File imazhi Imazhet do te pershtaten per tu vendosur ne nje kuti %ix%i. Impedanca: Implantuar: Produkt i faturueshem jo-aktiv Informacion Hyrje Datë e pavlefshme Janar Korrik Qershor Punuar se fundmi me: %s
 Kliko me butonin e majte diku ne linjen kohore dhe fillo zvarritjen!
Mund te perdorni gjithashty rroten e mausit!
Mund te klikoni gjithashtu me butonin e mesit te mausit per tu qenderzuar rreth asaj pike! I bllokuar Mars Maj Matje dhe Rezultate: Matje dhe rezultate: %s Probleme mjekesore: %(problems)s
Totali takimeve: %(encounters)s
Totali i hyrjeve EMR: %(items)s
Mjekime aktive: %(active_drugs)s
Dokumente: %(documents)s
Rezultati i testit: %(results)s
Hospitalizime: %(stays)s
Procedura: %(procedures)s
Vaksinime: %(vaccinations)s Hostoriku i mjekimit Liste mjekimi Hënë E hënë Muaj Emri Emri: E re Asnje rezultat [arriba] nuk u gjet ne [%s]. Asnje takim nuk u gjet per kete ceshtje shendeti. Asnje rezultat testi per te formatuar. Asnje linje kohore e hapur. Asnje linje kohore e percaktuar Nëntor Numri i duplikatave: Profesionet Tetor Hap &Recent Hap Linjen Kohore Hap nje linje kohore egzistuese... Hap faturen Hap produkt Hap linjen kohore me te fundit ne fillim Duke hapur linjen kohore ne vend te krijimit te nje te reje. Organizate: %s (%s) File PDF dales nuk mund te hapet. Faqja nuk u gjet Meme: Periudhë Preferencat Pergatitje: %s
 Printo Problem Procedura te kryera: %s Shenimet rreth progresit ne takimin me te fundit: Pyetje Pyetje dhe pergjigje RFE Regjim / Keshille Faqe te nderlidhura Me ndrysho madhesine dhe me leviz! Filet SVG Shtunë Duke u ruajtur Rrëshkitje Kerkoni rezultatet per '%s' Zgjidh Ikonën Zgjidhni ngjarje Zgjidhni ngjarjet qe do fshihen dhe shtypni butonin Del. Zgjidhni zonen per ta zmadhuar ate Perzgjedhja e ngjarjeve Ndjeshmeria: Shtator Koha e shfaqjes Statusi Tollumbac ngjites Fuqia Substance Subsanca hyrese (%s, %s) [#%s]                     
 Substance: %s [#%s]
 Djelë E djelë Detyrat Resultatet e testit Tekst Tekst: Kerkesa e laboratorit egziston per i perket nje pacienti tjeter. Koha e specifikuar egzistin ne sistem. Nuk ka episode per kete ceshtje shendeti. Kjo linje kohore eshte e ruajtur ne memorje dhe ndrshimet ne te nuk do te qendrojne ndermjet sesioneve.
Zgjidh File/E re/File e linjes kohore  per te krijuar nje linje kohore qe ruhet ne disk. Prag Enjte Linje kohore Filet e linjes kohore. Martë E pamundur te kopjoj E pamundur per te hapur linjen kohore  '%s'. E pamundur per te lexuar nga file  '%s'. E pamundur per te lexuar te dhena nga linja kohore  '%s'. E pamundur per te ruajtur linjen kohore ne '%s'. File u la i pamodifkuar. E pamundur per te bere backupin e '%s'. E pamundur te shkruhet file e konfigurimit. E pamundur per te shkruajtur te dhenat e linjes kohore. Episode te pa atribuara Format i panjohur. Perdor zvaritje me inerci Vaksinimet Vaksinime: %s Verifiko kujt pacienti kjo kerkese laboratori i perket. Mërkurë Java Java fillon tek: Mirë se erdhët Mirë se erdhët ne linjen kohore. Kur: Ku jane numrat e javes nese une e fillj javen te djelen? Ku eshte butoni per ruajtjen? Perse Linja Kohore nuk eshte ne gjuhen time> A do te fshihen ngjarjet e perfshira kur fshihet nje kategori? Vit Ju nuk mund te ndrysoni kohen kur Ngjarja eshte e bllokuar Zmadho aktiv aktiv, nevojitet kontroll aprovuar prezumohet aktiv dypalesh klinikisht me vlere mbyllur të dhëna ditë ditë perfundimtar episodi : %s gabim gjate marrjes se rezultateve laboratorike te pashtjelluara Gabim me placeholder [%s] burimi i pacientit te jashtem (emri, gjinia, datelindja) përfundoi mesazh gabimi i pergjithshem mesazh informues i pergjithshem mesazh paralajmerimi i pergjithshem problem shendeti: %s orë orë inaktiv ID e kerkeses [%s], e laboratorit [%s], lidhje e pritshme me pacientin [%s], aktualisht e lidhur me paciantin [%s] kontrolli i fundit: majtas afatgjate minutë minuta e dhene e modifikuar eshte e nevojshme nje kerkese laboratorike kur shtoni rezultatet laboratorike asnje alergji te njohur asnje asocim jo në dispozicion klinikisht jo me vlere ne vijim rreshtuar ne baze te markes e dhene origjinale programuesi ka harruar te specifikoje nje numer per gabimin programuesi ka harruar te specifikoje nje mesazh informues programuesi ka harruar te specifikoje nje paralajmerim e pa modifikueshme djathtas afatshkurter dyshuar tek sot I paaprovuar njesi gjendje alergjike e panjohur rekasion i panjohur formati i rezultateve te testit, i panjohur [%s] 